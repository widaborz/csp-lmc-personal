CPS.LMC Common project
===========================
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-csp-lmc-common/badge/?version=latest)](https://ska-telescope-ska-csp-lmc-common.readthedocs.io/en/latest/?badge=latest)


## Table of contents
* [Introduction](#introduction)
* [Repository organization](#repository-organization)
* [Prerequisities](#prerequisities)
* [Description](#description)
* [Known bugs](#known-bugs)
* [Troubleshooting](#troubleshooting)
* [License](#license)

## Introduction

General requirements for the monitor and control functionality are the same for both the SKA MID and LOW telescopes. <br/>
In addition, two of three other CSP Sub-elements, namely the `Pulsar Search` and the `Pulsar Timing`, have the same functionality and use the same design in both telescopes.<br/>
Functionality common to `CSP_Low.LMC` and `CSP_Mid.LMC` includes: communication framework, logging, archiving, alarm generation, sub-
arraying, some of the functionality related to handling observing mode changes, `Pulsar Search` and
`Pulsar Timing`, and to some extent Very Long Baseline Interferometry (`VLBI`).<br/>
The difference between `CSP_Low.LMC` and `CSP_Mid.LMC` is mostly due to different receivers (dishes vs stations) and
different `CBF` functionality and design.<br/>
To maximize code reuse, the software common to `CSP_Low.LMC` and `CSP_Mid.LMC` is developed by the work
package `CSP_Common.LMC` and provided to work packages `CSP_Low.LMC` and `CSP_Mid.LMC`, to
be used as a base for telescope specific `CSP.LMC` software. <br/>
In the current repository the code for the package `ska_csp_lmc_common` is provided. 

## Repository organization

The repository has the following organization:

* project source (src): contains the specific project TANGO Device Class files
* pogo: contains the POGO files of the TANGO Device Classes of the project 
* tests: contains the test
* docker: containes the configuration files as well as 
the Makefile to generate the docker image and run the tests.
* docs: contains all the files to generate the documentation for the project.

To get a local copy of the repository:

```bash
git clone https://gitlab.com/ska-telescope/ska-csp-lmc-common.git
```
## Prerequisities

* A TANGO development environment properly configured, as described in [SKA developer portal](https://developer.skatelescope.org/en/latest/tools/tango-devenv-setup.html)

* [SKA Base classes](https://gitlab.com/ska-telescope/ska-tango-base)
* access to a K8s/minikube cluster.


## Description
General requirements for the monitor and control functionality are the same for both SKA Mid and Low 
telescopes.<br>
Two of three `CSP` Sub-elements, `PSS` and `PST`, have the same functionality and use the
same design in both telesopes.<br>
Functionality common to Low and Mid `CSP.LMC` includes:

* communication software
* logging
* archiving
* alarm generation
* sub-arraying
* some of the functionality related to handling observing mode
* Pulsar Search and Pulsar Timing.

The main differences between `Low` and `Mid` `CSP.LMC` are due to the different receivers and `CBF`
functionality and design.<br>
The `CSP.LMC Common Software` package provides the base software to develop the Low and Mid CSP.LMC.<br>
The `CSP.LMC` software is based on the `TANGO Control System`,the `SKA Control System Guidelines` and the `SKA CSP.LMC Base Classes`.

## Known bugs


## Troubleshooting


## License
See the LICENSE file for details.

