#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the csp-lmc project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
"""Contain the tests for the CspMaster."""

# Standard imports
import numpy as np
import pytest
import tango
from tango import DevState
from tango.test_context import DeviceTestContext

#Local imports
from ska_csp_lmc_common import CspMaster
from ska.base.control_model import AdminMode, HealthState

device_to_load = {
    "path": "docker/config/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_common",
    "device": "commaster",
}
# Device test case

@pytest.mark.mock_device_proxy
class TestCspMaster:

    def test_State(self, device_under_test):
        """Test for State after initialization 
           NOTE: the CspMaster device state is ALARM because
           this device relies on several forwarded attributes.
        """
        # reinitalize Csp Master and CbfMaster devices
        csp_state = device_under_test.State()
        print(csp_state)
        assert csp_state not in [DevState.FAULT, DevState.DISABLE]

    def test_adminMode_offline(self, device_under_test):
        """ Test the adminMode attribute w/r"""
        device_under_test.adminMode = AdminMode.OFFLINE
        assert device_under_test.adminMode.value == AdminMode.OFFLINE

    def test_adminMode_online(self, device_under_test):
        """ Test the adminMode attribute w/r"""
        device_under_test.adminMode = AdminMode.ONLINE
        assert device_under_test.adminMode.value == AdminMode.ONLINE

    def test_write_invalid_admin_mode(self, device_under_test):

        with pytest.raises(tango.DevFailed) as df:
            device_under_test.adminMode = 7
        assert "Set value for attribute adminMode is negative or above the maximun authorized" in str(df.value.args[0].desc)

    def test_cspHealthState(self, device_under_test):
        """ Test the CSP initial healthState.
            NOTE: to build the CSP healthState, the sub-elements
            not ONLINE or MAINTENACE are not taken into account.
        """
        assert device_under_test.healthState == HealthState.OK

    def test_cbfAdminMode(self, device_under_test):
        """ Test the CBF adminMode attribute w/r"""
        assert device_under_test.cspCbfAdminMode == AdminMode.NOT_FITTED

    def test_cbfHealthState(self, device_under_test):
        """ Test the CBF initial healthState          """
        device_under_test.cspCbfHealthState == HealthState.OK

    def test_pssState(self, device_under_test):
        """ Test the PSS initial State          """
        assert device_under_test.cspPssState == DevState.DISABLE

    def test_pssHealthState(self, device_under_test):
        """ Test the PSS initial healthState          """
        assert device_under_test.cspPssHealthState == HealthState.UNKNOWN

    def test_pssAdminMode(self, device_under_test):
        """ Test the PSS initial adminMode          """
        pss_admin = device_under_test.cspPssAdminMode
        assert pss_admin == AdminMode.NOT_FITTED

    def test_pstState(self, device_under_test):
        """ Test the PST initial State          """
        assert device_under_test.cspPstState == DevState.DISABLE

    def test_pstHealthState(self, device_under_test):
        """ Test the PST initial healthState          """
        assert device_under_test.cspPstHealthState == HealthState.UNKNOWN

    def test_pstAdminMode(self, device_under_test):
        """ Test the PST initial adminMode          """
        assert device_under_test.cspPstAdminMode == AdminMode.NOT_FITTED

    def test_subelement_cbf_address(self, device_under_test, device_info):
        """Test the cbfMasterAdress value"""
        cbf_addr = device_under_test.cbfMasterAddress
        master_property = device_info['properties']
        cbf_addr_from_property = master_property['CspCbf']
        assert cbf_addr == cbf_addr_from_property[0]

    def test_subelement_pss_address(self, device_under_test, device_info):
        """Test the pssMasterAdress value"""
        pss_addr = device_under_test.pssMasterAddress
        master_property = device_info['properties']
        pss_addr_from_property = master_property['CspPss']
        assert pss_addr == pss_addr_from_property[0]

    def test_subelement_pst_address(self, device_under_test, device_info):
        """Test the pstMasterAdress value"""
        pst_addr = device_under_test.pstMasterAddress
        master_property = device_info['properties']
        pst_addr_from_property = master_property['CspPst']
        assert pst_addr == pst_addr_from_property[0]

    def test_configure_On_command_duration_time(self, device_under_test):
        device_under_test.onCmdDurationExpected = 3
        assert device_under_test.onCmdDurationExpected == 3

    def test_configure_Off_command_duration_time(self, device_under_test):
        device_under_test.offCmdDurationExpected = 3
        assert device_under_test.offCmdDurationExpected == 3

    def test_configure_Standby_command_duration_time(self, device_under_test):
        device_under_test.standbyCmdDurationExpected = 3
        assert device_under_test.standbyCmdDurationExpected == 3
