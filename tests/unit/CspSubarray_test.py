#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the csp-lmc project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
"""Contain the tests for the CspMaster."""

# Standard imports
import pytest
# Tango imports
import tango
from tango import DevState
from tango.test_context import DeviceTestContext
#Local imports
from ska_csp_lmc_common import CspSubarray
from ska.base.control_model import AdminMode, ObsState, HealthState

# Device test case
device_to_load = {
    "path": "docker/config/csplmc_dsconfig.json",
    "package": "ska_csp_lmc_common",
    "device": "comsub1",
}

@pytest.mark.mock_device_proxy
class TestCspSubarray:
    def test_init_State(self, device_under_test):
        """Test for State at initialization """
        state = device_under_test.State()
        print("state:{}".format(state))
        assert state not in [DevState.FAULT]

    def test_cbfsubarray_state(self, device_under_test):
        assert device_under_test.cbfSubarrayState == DevState.DISABLE

    def test_cbfsubarray_adminMode(self, device_under_test):
        assert device_under_test.cbfSubarrayAdminMode == AdminMode.NOT_FITTED

    def test_cbfsubarray_healthState(self, device_under_test):
        assert device_under_test.cbfSubarrayHealthState == HealthState.UNKNOWN

    def test_psssubarray_state(self, device_under_test):
        assert device_under_test.pssSubarrayState == DevState.DISABLE

    def test_psssubarray_adminMode(self, device_under_test):
        assert device_under_test.pssSubarrayAdminMode == AdminMode.NOT_FITTED

    def test_psssubarray_healthState(self, device_under_test):
        assert device_under_test.pssSubarrayHealthState == HealthState.UNKNOWN

    def test_psssubarray_obsState(self, device_under_test):
        assert device_under_test.pssSubarrayObsState == ObsState.IDLE

    def test_init_obsState(self, device_under_test):
        """Test the obsState value at initialization """
        obs_state = device_under_test.obsState
        assert obs_state == ObsState.EMPTY

    def test_commands_progress(self, device_under_test):
        """Test xxCmdProgress attributes """
        assert device_under_test.scanCmdProgress == 0
        assert device_under_test.endScanCmdProgress == 0
        assert device_under_test.goToIdleCmdProgress == 0
    '''
    def test_configure_invalid_state(self, device_under_test):
        """ Test Configure command execution when subarray
            State is not ON
        """
        with pytest.raises(tango.DevFailed) as df:
            device_under_test.Configure("")
        assert "Command Configure not allowed" in df.value.args[0].desc

    def test_subarray_off(self, device_under_test, csp_master):
        """ Test subarray state after power-on
            NOTE: this is a wrong behaviour of CbfSubarray that
            starts in DISABLE state with adminMode = ONLINE and
            it moves to OFF only after a power-on.
            The CbfSubarray has to start in OFF State when it is
            ONLINE and there is no need to power-on it.
            This behavior has to be changed
        """
        csp_master.On(["mid_csp_cbf/sub_elt/master",])
        time.sleep(4)
        csp_state = csp_master.State()
        state = device_under_test.cbfSubarrayState
        assert state == DevState.OFF
    '''

    #def test_remove_timing_beams_while_add_is_running_state(self, device_under_test):
    #    """ Test the execution of the RemovingTimingBeams" while
    #        the AddTimingBeams is already running
    #    """
    #    device_under_test.AddTimingBeams([1,2,3])
    #    with pytest.raises(tango.DevFailed) as df:
    #        device_under_test.RemoveTimingBeams([1,2])
    #    assert "Can't execute command" in df.value.args[0].desc
