import json
from ska_csp_lmc_common.csp_manage_json import JsonConfiguration
import logging
import pytest

LOGGER = logging.getLogger(__name__)

json_string_test_ADR_22 = """
{
    "interface": "https://schema.skatelescope.org/ska-csp-configure/1.0",
    "common": {
        "id": "sbi-mvp01-20200325-00001-science_A",     
        "frequencyBand": "1"     
    },
    "cbf": {
        "fsp": [       
            {         
                "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 0,         
                "outputLinkMap": [[0,0], [200,1]]       
            },       
            {         
                "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 744,         
                "outputLinkMap": [[0,4], [200,5]]       
            }     
        ] 
    }
}
"""
json_string_test_ADR_18 = """
{
    "common": {

        "id": "sbi-mvp01-20200325-00001-science_A",     
        "frequencyBand": "1"     
    },
    "cbf": {
        "fsp": [       
            {         
                "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 0,         
                "outputLinkMap": [[0,0], [200,1]]       
            },       
            {         
                "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 744,         
                "outputLinkMap": [[0,4], [200,5]]       
            }     
        ] 
    }
}
"""
#function mode missing 
json_string_test_ADR_18_invalid = """
{
    "common": {

        "id": "sbi-mvp01-20200325-00001-science_A",     
        "frequencyBand": "1" 
    },
    "cbf": {
        "fsp": [       
            {         
                "fspID": 1, "frequencySliceID": 1,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 0,         
                "outputLinkMap": [[0,0], [200,1]]       
            },       
            {         
                "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,         
                "integrationTime": 1400, "corrBandwidth": 0,         
                "channelAveragingMap": [[0,2], [744,0]],         
                "fspChannelOffset": 744,         
                "outputLinkMap": [[0,4], [200,5]]       
            }     
        ] 
    }
}
"""
json_string_test_pre_ADR_18 = """
{
    "id": "sbi-mvp01-20200325-00001-science_A",     
    "frequencyBand": "1",     
    "fsp": [       
        {         
            "fspID": 1, "functionMode": "CORR", "frequencySliceID": 1,         
            "integrationTime": 1400, "corrBandwidth": 0,         
            "channelAveragingMap": [[0,2], [744,0]],         
            "fspChannelOffset": 0,         
            "outputLinkMap": [[0,0], [200,1]]       
        },       
        {         
            "fspID": 2, "functionMode": "CORR", "frequencySliceID": 2,         
            "integrationTime": 1400, "corrBandwidth": 0,         
            "channelAveragingMap": [[0,2], [744,0]],         
            "fspChannelOffset": 744,         
            "outputLinkMap": [[0,4], [200,5]]       
        }     
    ] 
}
"""

json_string_invalid_URI = '{"interface": "invaliduri.com/0.1"}'

class TestJsonConfiguration:

    def init_config(self):
        self.json_config_ADR22 = JsonConfiguration(json_string_test_ADR_22, LOGGER)
        self.json_config_ADR18 = JsonConfiguration(json_string_test_ADR_18, LOGGER)
        self.json_config_pre_ADR18 = JsonConfiguration(json_string_test_pre_ADR_18, LOGGER)


    def test_detect_version(self):
        self.init_config()
        self.json_config_ADR22.detect_version()
        assert self.json_config_ADR22.major_version == 1  
        assert self.json_config_ADR22.minor_version == 0

        self.json_config_ADR18.detect_version()
        assert self.json_config_ADR18.major_version == 1  
        assert self.json_config_ADR18.minor_version == 0

        self.json_config_pre_ADR18.detect_version()
        assert self.json_config_pre_ADR18.major_version == 0  
        assert self.json_config_pre_ADR18.minor_version == 1

    def test_detect_version_with_invalid_uri(self):
        with pytest.raises(ValueError, match= r'Wrong json schema URI!'):
            JsonConfiguration(json_string_invalid_URI, LOGGER).detect_version()
            
    def test_get_section_when_section_is_present(self):
        self.init_config()
        assert isinstance(self.json_config_ADR18.get_section('cbf'), dict)
        assert 'fsp' in self.json_config_ADR18.get_section('cbf').keys()

        assert isinstance(self.json_config_pre_ADR18.get_section('fsp'), list)
    
    def test_get_section_when_section_is_not_present(self):
        self.init_config()
        with pytest.raises(ValueError):
            self.json_config_pre_ADR18.get_section('cbf')
        
        with pytest.raises(ValueError):
            self.json_config_ADR18.get_section('fsp')

    def test_conversion_10_01(self):
        self.init_config()
        self.json_config_ADR18.conversion_10_01()
        assert self.json_config_ADR18.config == self.json_config_pre_ADR18.config

    def test_conversion_10_01_invalid_schema(self):
        with pytest.raises(ValueError):
            JsonConfiguration(json_string_test_ADR_18_invalid, LOGGER).conversion_10_01()

    def test_build_json_cbf_version(self):
        self.init_config()
        assert self.json_config_ADR18.build_json_cbf() == self.json_config_pre_ADR18.build_json_cbf() 
        assert self.json_config_ADR22.build_json_cbf() == self.json_config_pre_ADR18.build_json_cbf()

    def test_build_json_cbf_version_not_supported(self):
        self.init_config()
        # version in interface is changed to 2.0 to force the failing of the test
        self.json_config_ADR22.config['interface'] = "https://schema.skatelescope.org/ska-csp-configure/2.0"
        with pytest.raises(ValueError, match=r'version.*is not supported'):
            self.json_config_ADR22.build_json_cbf()

    def test_get_id(self):
        self.init_config()
        assert self.json_config_pre_ADR18.get_id() == "sbi-mvp01-20200325-00001-science_A"
        assert self.json_config_ADR18.get_id() == "sbi-mvp01-20200325-00001-science_A"

    def test_get_id_NOT_PRESENT(self):
        self.init_config()
        self.json_config_pre_ADR18.config.pop('id')
        json_config_with_no_id = json.dumps(self.json_config_pre_ADR18.config)
        json_config_with_no_id = JsonConfiguration(json_config_with_no_id, LOGGER)
        with pytest.raises(ValueError):
            json_config_with_no_id.get_id()
