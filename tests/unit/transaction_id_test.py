from ska_csp_lmc_common.utils.decorators import transaction_id
import json


class class_test:
    """ A test class for the transaction id. 
    It has the few essential attributes
    and the decorated method called in tests. 
    """
    def __init__(self):
        self.name = 'name_test'
        self.logger = None
    @transaction_id
    def test_func(self, argin):
        """ A method to be tested with the transaction id decorator.
        it returns the input "argin" modified by the decorator
        """
        return argin

def test_transaction_id_with_positional_arguments():
    """ A test that shows that the transaction id is added to the argin
    when a positional arguments is passed to the decorated function
    """
    argin = '{"id":"test_id"}'
    c = class_test()
    argin = c.test_func(argin)
    assert 'transaction_id' in argin

def test_transaction_id_with_keyword_arguments():
    """ The transaction id is added to the argin
    when a keyword argument is passed to the decorated function.
    The key of the argument is irrelevant
    """
    argin = '{"id":"test_id"}'
    c = class_test()
    argin = c.test_func(argin1=argin)
    assert 'transaction_id' in argin

    argin = c.test_func(argin2=argin)
    assert 'transaction_id' in argin

def test_transaction_id_if_present():
    """ If the transaction id is already present in argin
    it is not changed by the decorator
    """
    argin = '{"transaction_id":"test_id"}'
    c = class_test()
    out = c.test_func(argin)
    out = json.loads(out)
    assert out['transaction_id'] == 'test_id'


