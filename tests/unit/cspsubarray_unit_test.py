import contextlib
import importlib
import sys
import mock
import pytest
import tango
import logging
import re
from mock import Mock, MagicMock
from ska.base.control_model import HealthState, ObsState, LoggingLevel
from ska.base.commands import  ResultCode
from tango.test_context import DeviceTestContext
from tango import DevState, DevFailed
from ska_csp_lmc_common import CspSubarray
from ska_csp_lmc_common.utils.test_utils import Probe, Poller

def test_cspsubarray_state_and_obstate_value_after_initialization():
    """
    Test the State and obsState values for the CpSubarray at the
    end of the initialization process.
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    pst_subarray_fqdn = 'mid_csp_pst/sub_elt/subarray_01'

    state_attr = 'State'
    dut_properties = {
            'CspMaster': 'mid_csp/elt/master',
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'PstSubarray': pst_subarray_fqdn
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    pst_subarray_device_proxy_mock = Mock()

    event_subscription_map_cbf= {}
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map_cbf.update({attr_name: callback}))

    event_subscription_map_pss = {}
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
        lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map_pss.update(
            {attr_name: callback}))

    event_subscription_map_pst = {}
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
        lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map_pst.update(
            {attr_name: callback}))

    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
        pst_subarray_fqdn: pst_subarray_device_proxy_mock
    }
    # CASE A: CBF is OFF
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        assert tango_context.device.State() == DevState.OFF
        assert tango_context.device.obsState == ObsState.EMPTY

def test_cspsbarray_state_after_On_WITH_exception_raised_by_subelement_subarray():
    """
    Test the behavior of the CspSubarray when one of the sub-element subarray
    raises a DevFailed exception.
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = raise_devfailed_exception 
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, "State", DevState.OFF, f"State is not OFF")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        assert tango_context.device.State() == DevState.FAULT
        assert tango_context.device.obsState == ObsState.EMPTY

def test_cspsbarray_state_after_On_WITH_command_failed_code_returned_by_subelement_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,

    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_failed
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, "State", DevState.OFF, f"State is not OFF")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        assert tango_context.device.State() == DevState.FAULT
        assert tango_context.device.obsState == ObsState.EMPTY

def test_cspsbarray_state_after_On_WITH_command_failed_code_returned_by_pss_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_failed
        prober_state = Probe(tango_context.device, "State", DevState.OFF, f"State is not OFF")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        assert tango_context.device.State() == DevState.ON
        assert tango_context.device.obsState == ObsState.EMPTY
        assert tango_context.device.healthState == HealthState.DEGRADED

def test_cspsbarray_state_after_On_forwarded_to_subelement_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, "State", DevState.OFF, f"State is not OFF")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        assert tango_context.device.State() == DevState.ON
        assert tango_context.device.obsState == ObsState.EMPTY

def test_cspsubarray_transaction_id_in_log(capsys):
    """
    Test that when transaction_id decorator is applied to the Configure
    and Assign Resources command, both transaction id are captured in log
    """
    device_under_test = CspSubarray
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CspMaster':'mid_csp/elt/master',
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': 'mid_csp_pss/sub_elt/subarray_01',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
    }
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, "State", DevState.OFF, f"State is not OFF")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        assert tango_context.device.State() == DevState.ON
        #tango_context.device.AssignResources('{"subarrayID":1,"dish":{"receptorIDList":["0001","0002"]}}')
        tango_context.device.AssignResources('{"example": ["BAND1"]}')
        tango_context.device.obsState == ObsState.IDLE
        #TODO: find a way to test Configure (ObsState is needed to be IDLE)
        #tango_context.device.Configure('{"id":"sbi-400-scienceA"}')
        # a prober is needed since the duration of the Configure command is variable.
        #prober_obs_state = Probe(tango_context.device, "obsState", ObsState.IDLE, f"Configure command out of time")
        #Poller(10, 0.1).check(prober_obs_state)
        #assert_that_log_contains('ConfigureCommand',capsys)
        assert_that_log_contains('AssignResourcesCommand', capsys)

def assert_that_log_contains(name:str,capsys):
    patterns = [f'|Transaction.*(?<=Enter\[{name}\])',f'|Transaction.*(?<=Exit\[{name}\])']
    for pattern in patterns:
        found = False
        out, err = capsys.readouterr()
        if re.match(pattern,out):
            found = True
            break
        if not found:
            raise AssertionError(f'pattern ({pattern}) not found in expected log messages')

def return_ok():
    """
    Return a FAILED code in the execution of a device method.
    """
    message = "CBF Subarray Oncommand OK"
    return (ResultCode.OK, message)

def return_failed():
    """
    Return a FAILED code in the execution of a device method.
    """
    print("return failed")
    return (ResultCode.FAILED, "On Command failed")

def raise_devfailed_exception():
    """
    Raise an exception to test the failure of a device command
    """
    tango.Except.throw_exception("Commandfailed", "This is error message for devfailed",
                                 " ", tango.ErrSeverity.ERR)
def mock_event_dev_name(device_name):
    return device_name

def create_dummy_event(cbf_subarray_fqdn, event_value):
    """
    Create a mocked event object to test the event callback method
    associate to the attribute at subscription.
    param: cbf_subarray_fqdn the CBF Subarray FQDN
           event_value the expected value
    return: the fake event
    """
    fake_event = Mock()
    fake_event.err = False
    fake_event.attr_name = f"{cbf_subarray_fqdn}/state"
    fake_event.attr_value.value = event_value
    fake_event.attr_value.name = 'State'
    fake_event.device.name = cbf_subarray_fqdn
    fake_event.device.dev_name.side_effect=(lambda *args, **kwargs: mock_event_dev_name(cbf_subarray_fqdn))
    return fake_event

def create_dummy_obs_event(cbf_subarray_fqdn, event_value):
    """
    Create a mocked event object to test the event callback method
    associate to the attribute at subscription.
    param: cbf_subarray_fqdn the CBF Subarray FQDN
           event_value the expected value
    return: the fake event
    """
    fake_event = Mock()
    fake_event.err = False
    fake_event.attr_name = f"{cbf_subarray_fqdn}/obsstate"
    fake_event.attr_value.value = event_value
    fake_event.attr_value.name = 'obsState'
    fake_event.device.name = cbf_subarray_fqdn
    fake_event.device.dev_name.side_effect=(lambda *args, **kwargs: mock_event_dev_name(cbf_subarray_fqdn))
    return fake_event
@contextlib.contextmanager
def fake_tango_system(device_under_test, initial_dut_properties={}, proxies_to_mock={},
                      device_proxy_import_path='tango.DeviceProxy'):

    with mock.patch(device_proxy_import_path) as patched_constructor:
        patched_constructor.side_effect = lambda device_fqdn: proxies_to_mock.get(device_fqdn, Mock())
        patched_module = importlib.reload(sys.modules[device_under_test.__module__])
        print("patched_module:", patched_module)

    print("device_under_test.__name__:", device_under_test.__name__)
    device_under_test = getattr(patched_module, device_under_test.__name__)

    device_test_context = DeviceTestContext(device_under_test, properties=initial_dut_properties)
    device_test_context.start()
    yield device_test_context
    device_test_context.stop()
