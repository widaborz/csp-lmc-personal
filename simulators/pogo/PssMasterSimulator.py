# -*- coding: utf-8 -*-
#
# This file is part of the PssMasterSimulator project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" PssMaster Class simulator

"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from DeviceTestMaster import DeviceTestMaster
# Additional import
# PROTECTED REGION ID(PssMasterSimulator.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  PssMasterSimulator.additionnal_import

__all__ = ["PssMasterSimulator", "main"]


class PssMasterSimulator(DeviceTestMaster):
    """

    **Properties:**

    - Device Property
    
    
    
    
    
    
    
    
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(PssMasterSimulator.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  PssMasterSimulator.class_variable

    # -----------------
    # Device Properties
    # -----------------









    # ----------
    # Attributes
    # ----------























    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the PssMasterSimulator."""
        DeviceTestMaster.init_device(self)
        # PROTECTED REGION ID(PssMasterSimulator.init_device) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(PssMasterSimulator.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(PssMasterSimulator.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(PssMasterSimulator.main) ENABLED START #
    return run((PssMasterSimulator,), args=args, **kwargs)
    # PROTECTED REGION END #    //  PssMasterSimulator.main

if __name__ == '__main__':
    main()
