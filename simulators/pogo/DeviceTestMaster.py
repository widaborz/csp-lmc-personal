# -*- coding: utf-8 -*-
#
# This file is part of the DeviceTestMaster project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC subelement Test Master Tango device prototype

Test TANGO device class to test connection with the CSPMaster prototype.
It simulates the CbfMaster sub-element.
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from SKAMaster import SKAMaster
# Additional import
# PROTECTED REGION ID(DeviceTestMaster.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  DeviceTestMaster.additionnal_import

__all__ = ["DeviceTestMaster", "main"]


class DeviceTestMaster(SKAMaster):
    """
    Test TANGO device class to test connection with the CSPMaster prototype.
    It simulates the CbfMaster sub-element.

    **Properties:**

    - Device Property
    
    
    
    
    
    
    
    
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(DeviceTestMaster.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  DeviceTestMaster.class_variable

    # -----------------
    # Device Properties
    # -----------------









    # ----------
    # Attributes
    # ----------















    onCommandProgress = attribute(
        dtype='DevUShort',
        label="Command progress percentage",
        polling_period=1000,
        abs_change=5,
        max_value=100,
        min_value=0,
        doc="Percentage progress implemented for commands that  result in state/mode transitions for a large \nnumber of components and/or are executed in stages (e.g power up, power down)",
    )

    offCommandProgress = attribute(
        dtype='DevDouble',
        polling_period=1000,
        abs_change=5,
    )

    standbyCommandProgress = attribute(
        dtype='DevDouble',
        polling_period=1000,
        abs_change=5,
    )

    onDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    offDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    standbyDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )



    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the DeviceTestMaster."""
        SKAMaster.init_device(self)
        # PROTECTED REGION ID(DeviceTestMaster.init_device) ENABLED START #
        # PROTECTED REGION END #    //  DeviceTestMaster.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(DeviceTestMaster.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  DeviceTestMaster.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(DeviceTestMaster.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  DeviceTestMaster.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_onCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.onCommandProgress_read) ENABLED START #
        """Return the onCommandProgress attribute."""
        return 0
        # PROTECTED REGION END #    //  DeviceTestMaster.onCommandProgress_read

    def read_offCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.offCommandProgress_read) ENABLED START #
        """Return the offCommandProgress attribute."""
        return 0.0
        # PROTECTED REGION END #    //  DeviceTestMaster.offCommandProgress_read

    def read_standbyCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.standbyCommandProgress_read) ENABLED START #
        """Return the standbyCommandProgress attribute."""
        return 0.0
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyCommandProgress_read

    def read_onDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.onDurationExpected_read) ENABLED START #
        """Return the onDurationExpected attribute."""
        return 0
        # PROTECTED REGION END #    //  DeviceTestMaster.onDurationExpected_read

    def write_onDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.onDurationExpected_write) ENABLED START #
        """Set the onDurationExpected attribute."""
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.onDurationExpected_write

    def read_offDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.offDurationExpected_read) ENABLED START #
        """Return the offDurationExpected attribute."""
        return 0
        # PROTECTED REGION END #    //  DeviceTestMaster.offDurationExpected_read

    def write_offDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.offDurationExpected_write) ENABLED START #
        """Set the offDurationExpected attribute."""
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.offDurationExpected_write

    def read_standbyDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.standbyDurationExpected_read) ENABLED START #
        """Return the standbyDurationExpected attribute."""
        return 0
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyDurationExpected_read

    def write_standbyDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.standbyDurationExpected_write) ENABLED START #
        """Set the standbyDurationExpected attribute."""
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyDurationExpected_write


    # --------
    # Commands
    # --------

    @command(
        dtype_in='DevVarStringArray',
        doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\nIf the array length is > 1, each array element specifies the FQDN of the\nCSP SubElement to switch ON.",
    )
    @DebugIt()
    def On(self, argin):
        # PROTECTED REGION ID(DeviceTestMaster.On) ENABLED START #
        """
        Transit CSP or one or more CSP SubElements fromSTANDBY to ON

        :param argin: 'DevVarStringArray'
        
            If the array length is0, the command apllies to the whole
            CSP Element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement to switch ON.

        :return:None
        """
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.On

    @command(
        dtype_in='DevVarStringArray',
        doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\nIf the array length is > 1, each array element specifies the FQDN of the\nCSP SubElement to switch OFF.",
    )
    @DebugIt()
    def Off(self, argin):
        # PROTECTED REGION ID(DeviceTestMaster.Off) ENABLED START #
        """
        Transit CSP or one or more CSP SubElements from ON to OFF.

        :param argin: 'DevVarStringArray'
        
            If the array length is0, the command apllies to the whole
            CSP Element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement to switch OFF.

        :return:None
        """
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.Off

    @command(
        dtype_in='DevVarStringArray',
        doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\nIf the array length is > 1, each array element specifies the FQDN of the\nCSP SubElement to put in STANDBY mode.",
    )
    @DebugIt()
    def Standby(self, argin):
        # PROTECTED REGION ID(DeviceTestMaster.Standby) ENABLED START #
        """
        
            Transit CSP or one or more CSP SubElements from ON/OFF to 
            STANDBY.

        :param argin: 'DevVarStringArray'
        
            If the array length is0, the command apllies to the whole
            CSP Element.
            If the array length is > 1, each array element specifies the FQDN of the
            CSP SubElement to put in STANDBY mode.

        :return:None
        """
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.Standby

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(DeviceTestMaster.main) ENABLED START #
    return run((DeviceTestMaster,), args=args, **kwargs)
    # PROTECTED REGION END #    //  DeviceTestMaster.main

if __name__ == '__main__':
    main()
