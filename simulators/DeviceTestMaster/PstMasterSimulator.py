# -*- coding: utf-8 -*-
#
# This file is part of the PssMasterSimulator project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" PssMaster Class simulator

"""

from __future__ import absolute_import
import sys
from collections import defaultdict
import os
import time

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
module_path = os.path.abspath(os.path.join(file_path, os.pardir)) + "/../utils"
print(module_path)
sys.path.insert(0, module_path)

# Tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(DeviceTestMaster.additionnal_import) ENABLED START #
from future.utils import with_metaclass
import threading
from ska_csp_lmc_common.utils import HealthState, AdminMode
from DeviceTestMaster import DeviceTestMaster
# PROTECTED REGION END #    //  DeviceTestMaster.additionnal_import

__all__ = ["PstMasterSimulator", "main"]


class PssMasterSimulator(with_metaclass(DeviceMeta,DeviceTestMaster)):
    """

    **Properties:**

    - Device Property
    """
    # PROTECTED REGION ID(PssMasterSimulator.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  PssMasterSimulator.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------


    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the PssMasterSimulator."""
        DeviceTestMaster.init_device(self)
        # PROTECTED REGION ID(PssMasterSimulator.init_device) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(PssMasterSimulator.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(PssMasterSimulator.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  PssMasterSimulator.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(PssMasterSimulator.main) ENABLED START #
    return run((PssMasterSimulator,), args=args, **kwargs)
    # PROTECTED REGION END #    //  PssMasterSimulator.main

if __name__ == '__main__':
    main()
