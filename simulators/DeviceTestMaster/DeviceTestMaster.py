# -*- coding: utf-8 -*-
#
# This file is part of the DeviceTestMaster project
#
#
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC subelement Test Master Tango device prototype

Test TANGO device class to test connection with the CSPMaster prototype.
It simulates the CbfMaster sub-element.
"""
import sys
from collections import defaultdict
import os
import time
import threading

# Tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(DeviceTestMaster.additionnal_import) ENABLED START #
from ska.base.control_model import HealthState, AdminMode
from ska.base import SKAMaster
# PROTECTED REGION END #    //  DeviceTestMaster.additionnal_import

__all__ = ["DeviceTestMaster", "main"]


class DeviceTestMaster(SKAMaster):
    """
    DeviceTestMaster TANGO device class to test connection with the CSPMaster prototype
    """
    # PROTECTED REGION ID(DeviceTestMaster.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  DeviceTestMaster.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    onCommandProgress = attribute(
        dtype='DevUShort',
        label="Command progress percentage",
        polling_period=1000,
        abs_change=5,
        max_value=100,
        min_value=0,
        doc="Percentage progress implemented for commands that  result in state/mode transitions for a large \nnumber of components and/or are executed in stages (e.g power up, power down)",
    )

    offCommandProgress = attribute(
        dtype='DevDouble',
        polling_period=1000,
        abs_change=5,
    )

    standbyCommandProgress = attribute(
        dtype='DevDouble',
        polling_period=1000,
        abs_change=5,
    )

    onDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    offDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )

    standbyDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        memorized=True,
    )


    # ---------------
    # General methods
    # ---------------

    def init_subelement(self):
        """
        Simulate the sub-element device initialization
        """
        time.sleep(3)
        self.set_state(tango.DevState.STANDBY)

    def on_subelement(self):
        """
        Simulate the sub-element transition from STANDBY to ON 
        """
        print("Executing the On command...wait")
        while True:
            elapsed_time = time.time() - self._start_time
            if  elapsed_time >  self._duration_expected['on']:
                break
            if elapsed_time  > 1:
                self._cmd_progress['on'] = elapsed_time* 100/self._duration_expected['on']
                print(self._cmd_progress['on'])
                self.push_change_event("onCommandProgress", int(self._cmd_progress['on']))
            time.sleep(1)
        self.set_state(tango.DevState.ON)
        self._health_state = HealthState.DEGRADED
        self.push_change_event("onCommandProgress", 100)
        print("Final state:", self.get_state())
        print("End On command...wait")

    def standby_subelement(self):
        """
        Simulate the sub-element transition from ON to STANDBY
        """
        print("Executing the Standby command...wait")
        while True:
            elapsed_time = time.time() - self._start_time
            if  elapsed_time > self._duration_expected['standby']:
                break
            if elapsed_time > 1:
                self._cmd_progress['standby'] = elapsed_time* 100/self._duration_expected['standby']
                print(self._cmd_progress['standby'])
                self.push_change_event("standbyCommandProgress", int(self._cmd_progress['standby']))
            time.sleep(1)
        self.set_state(tango.DevState.STANDBY)
        self.push_change_event("standbyCommandProgress", 100)
        print("End Standby command...wait")

    def off_subelement(self):
        """
        Simulate the sub-element transition from STANDBY to OFF
        """
        while True:
            elapsed_time = time.time() - self._start_time
            if  elapsed_time > self._duration_expected['off']:
                break
            if  elapsed_time >  1:
                self._cmd_progress['off'] = elapsed_time* 100/self._duration_expected['ff']
                self.push_change_event("offCommandProgress", int(self._cmd_progress['off']))
            time.sleep(1)
        self._health_state = HealthState.UNKNOWN
        self.set_state(tango.DevState.OFF)
        self.push_change_event("offCommandProgress", 100)

    def init_device(self):
        SKAMaster.init_device(self)
        # PROTECTED REGION ID(DeviceTestMaster.init_device) ENABLED START #
        
        self.set_state(tango.DevState.INIT)
        self._health_state = HealthState.UNKNOWN
        self._admin_mode = AdminMode.NOT_FITTED
        self._duration_expected = defaultdict(lambda:10)
        self._cmd_progress = defaultdict(lambda:0)
        
        csp_tango_db = tango.Database()
        # read the CSP memorized attributes from the TANGO DB. 
        # Note: a memorized attribute has defined the attribute
        # property '__value'
        attribute_properties = csp_tango_db.get_device_attribute_property(self.get_name(),
                                                                          {'adminMode': ['__value'],
                                                                           'on': ['__value'],
                                                                           'elementLoggingLevel': ['__value'],
                                                                           'centralLoggingLevel': ['__value'],})

        self._admin_mode = int(attribute_properties['adminMode']['__value'][0])
        # start a timer to simulate device intialization
        thread = threading.Timer(1, self.init_subelement)
        thread.start()

        # PROTECTED REGION END #    //  DeviceTestMaster.init_device

    def always_executed_hook(self):
        # PROTECTED REGION ID(DeviceTestMaster.always_executed_hook) ENABLED START #
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.always_executed_hook

    def delete_device(self):
        # PROTECTED REGION ID(DeviceTestMaster.delete_device) ENABLED START #
        pass
        # PROTECTED REGION END #    //  DeviceTestMaster.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_onCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.onCommandProgress_read) ENABLED START #
        """Return the onCommandProgress attribute."""
        return int(self._cmd_progress['on'])
        # PROTECTED REGION END #    //  DeviceTestMaster.onCommandProgress_read

    def read_offCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.offCommandProgress_read) ENABLED START #
        """Return the offCommandProgress attribute."""
        return int(self._cmd_progress['off'])
        # PROTECTED REGION END #    //  DeviceTestMaster.offCommandProgress_read

    def read_standbyCommandProgress(self):
        # PROTECTED REGION ID(DeviceTestMaster.standbyCommandProgress_read) ENABLED START #
        """Return the standbyCommandProgress attribute."""
        return int(self._cmd_progress['standby'])
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyCommandProgress_read

    def read_onDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.onDurationExpected_read) ENABLED START #
        """Return the onDurationExpected attribute."""
        return self._duration_expected['on']
        # PROTECTED REGION END #    //  DeviceTestMaster.onDurationExpected_read

    def write_onDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.onDurationExpected_write) ENABLED START #
        """Set the onDurationExpected attribute."""
        self._duration_expected['on'] = value
        # PROTECTED REGION END #    //  DeviceTestMaster.onDurationExpected_write

    def read_offDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.offDurationExpected_read) ENABLED START #
        """Return the offDurationExpected attribute."""
        return self._duration_expected['off']
        # PROTECTED REGION END #    //  DeviceTestMaster.offDurationExpected_read

    def write_offDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.offDurationExpected_write) ENABLED START #
        """Set the offDurationExpected attribute."""
        self._duration_expected['off'] = value
        # PROTECTED REGION END #    //  DeviceTestMaster.offDurationExpected_write

    def read_standbyDurationExpected(self):
        # PROTECTED REGION ID(DeviceTestMaster.standbyDurationExpected_read) ENABLED START #
        """Return the standbyDurationExpected attribute."""
        return self._duration_expected['standby']
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyDurationExpected_read

    def write_standbyDurationExpected(self, value):
        # PROTECTED REGION ID(DeviceTestMaster.standbyDurationExpected_write) ENABLED START #
        """Set the standbyDurationExpected attribute."""
        elf._duration_expected['standby'] = value
        # PROTECTED REGION END #    //  DeviceTestMaster.standbyDurationExpected_write

    # --------
    # Commands
    # --------

    @command(
    doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\nIf the array length is > 1, each array element specifies the FQDN of the\nCSP SubElement to switch ON.", 
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(DeviceTestMaster.On) ENABLED START #
        print("Processing On command")
        self._cmd_progress['on'] = 0
        self._start_time = time.time()
        thread = threading.Timer(2, self.on_subelement)
        thread.start()
        # PROTECTED REGION END #    //  DeviceTestMaster.On

    @command(
    dtype_in=('str',), 
    doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\nIf the array length is > 1, each array element specifies the FQDN of the\nCSP SubElement to switch OFF.", 
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(DeviceTestMaster.Off) ENABLED START #
        self._cmd_progress['off'] = 0
        self._start_time = time.time()
        thread = threading.Timer(1, self.off_subelement)
        thread.start()
        # PROTECTED REGION END #    //  DeviceTestMaster.Off

    @command(
        doc_in="If the array length is0, the command apllies to the whole\nCSP Element.\n\
                If the array length is > 1, each array element specifies the FQDN of the\n\
                CSP SubElement to switch OFF.",
    )

    @DebugIt()
    def Standby(self):
        # PROTECTED REGION ID(DeviceTestMaster.Standby) ENABLED START #
        self._cmd_progress['standby'] = 0
        self._start_time = time.time()
        thread = threading.Timer(2, self.standby_subelement)
        thread.start()
        # PROTECTED REGION END #    //  DeviceTestMaster.Standby

    def is_Off_allowed(self):
        # PROTECTED REGION ID(DeviceTestMaster.is_Off_allowed) ENABLED START #
        if self.get_state() not in [DevState.STANDBY]:
            return False
        return True
        # PROTECTED REGION END #    //  DeviceTestMaster.is_Off_allowed.

    def is_Standby_allowed(self):
        # PROTECTED REGION ID(DeviceTestMaster.is_Standby_allowed) ENABLED START #
        if self.get_state() not in [DevState.ON, DevState.OFF]:
            return False
        return True
        # PROTECTED REGION END #    //  DeviceTestMaster.is_Standby_allowed

    def is_On_allowed(self):
        # PROTECTED REGION ID(DeviceTestMaster.is_On_allowed) ENABLED START #
        if self.get_state() not in [DevState.STANDBY]:
            return False
        return True
        # PROTECTED REGION END #    //  DeviceTestMaster.is_On_allowed

# ----------
# Run server
# ----------

def main(args=None, **kwargs):
    # PROTECTED REGION ID(DeviceTestMaster.main) ENABLED START #
    return run((DeviceTestMaster,), args=args, **kwargs)
    # PROTECTED REGION END #    //  DeviceTestMaster.main

if __name__ == '__main__':
    main()
