.. Documentation

.. |br| raw:: html

   <br />

######################
CSP.LMC Common Package
######################

General requirements for the monitor and control functionality are the same in both telescopes.
In addition two of three other CSP Sub-elements, namely PSS and PST, have the same functionality and use the same
design for both the telescopes. |br|
Functionality common to Low and Mid CSP.LMC includes: communication framework, logging, archiving, alarm generation,
subarraying, some of the functionality realated to handling observing mode changes, Pulsar Search and
Pulsar Timing, and to some extent Very Long Baseline Interferometry (VLBI). |br|
The difference between LOW and MID CSP.LMC is mostly due to the different receivers (dishes vs stations) and different
CBF functionality and design. More than the 50% of the CSP.LMC functionality is common for both
telescopes. |br|
The CSP.LMC Common Package comprises all the software components and functionality common to LOW and MID CSP.LMC
and is used as a base for development of the Low CSP.LMC and Mid CSP.LMC software. |br|
The *CSP.LMC Common Package* is delivered as a part of each CSP.LMC release, via a Python package that can be used as 
required for maintenance and upgrade. |br|
CSP.LMC implements a high level interface (API) that Telescope Manager (TM), or other authorized
client, can use to monitor and control CSP as a single instrument. |br|
At the same time, CSP.LMC provides high level commands that the TM can use to sub-divide the array
into up to 16 sub-arrays, i.e. to assign station/receptors to sub-arrays, and to operate each 
sub-array independently and conccurently with all othe sub-arrays. |br|
The top-level software components provided by CSP.LMC API are:

* :ref:`Csp Master <CSP-Master>`
* :ref:`Csp Subarray <CSP-Subarray>`
* CSP Alarm Handler
* CSP Logger
* CSP TANGO Facility Database
* Input processor Capability (receptors/stations)
* :ref:`Search Beam Capability <CSP-Capability>`
* :ref:`Timing Beam Capability <CSP-Capability>`
* :ref:`VLBI Beam Capability <CSP-Capability>`

Components listed above are implemented as TANGO devices, i.e. classes that implement standard TANGO API.
The CSP.LMC TANGO devices are based on the standard SKA1 TANGO Element Devices provided via the *SKA Base Classes package*.

.. _CSP-Master:

**************
CSP.LMC Master
**************

.. include:: CspMaster.inc


.. _CSP-Subarray:

****************
CSP.LMC Subarray
****************

.. include:: CspSubarray.inc

.. _CSP-Capability:

********************
CSP.LMC Capabilities
********************

.. include:: CspCapability.inc


