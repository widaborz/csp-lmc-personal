Capabilities represent the CSP schedulable resources and provide API that can be used to
configure, monitor and control resources that implement signal processing functionality.
During normal operations, TM uses the sub-array API to assign capabilities to the sub-array, configure
sub-array Processing Mode, start and stop scan. |br|
The CSP.LMC Common Package implements the capabilities that are shared between LOW and MID instances. |br|
These are:

* :ref:`CSP Search Beam Capability <CSP-SearchBeam-Capability>`
* :ref:`CSP Timing Beam Capability <CSP-TimingBeam-Capability>`
* :ref:`CSP VLBI Beam Capability <CSP-VlbiBeam-Capability>`

.. _CSP-SearchBeam-Capability:

CSP.LMC Search Beam Capability 
==============================
The Search Beam Capability exposes the attributes and commands to monitor and control beamforming and PSS
processing in a single beam. |br|
The mapping between an instance of the CSP Search Beam and the internal CSP Sub-element components performing
beamforming and search is established at initialization and is permanent.

CSP.LMC SearchBeamCapability API Documentation
-----------------------------------------------

.. autoclass:: ska_csp_lmc_common.CspSearchBeamCapability.CspSearchBeamCapability
   :members:
   :member-order:
   :private-members:
   :show-inheritance:
   :undoc-members:

.. _CSP-TimingBeam-Capability:

CSP.LMC Timing Beam Capability 
==============================
The Timing Beam Capability exposes the attributes and commands to monitor and control beamforming and PST
processing in a single beam. |br|
The mapping between an instance of the CSP Search Beam and the internal CSP Sub-element components performing
beamforming and search is established at initialization and is permanent.


CSP.LMC TimingBeamCapability API Documentation
----------------------------------------------
.. autoclass:: ska_csp_lmc_common.CspTimingBeamCapability.CspTimingBeamCapability
   :members:
   :member-order:
   :noindex:
   :private-members:
   :show-inheritance:
   :undoc-members:

.. _CSP-VlbiBeam-Capability:

CSP.LMC VLBI Beam Capability 
============================
The VLBI Beam Capability exposes the attributes and commands to monitor and control beamforming and VLBI
processing in a single beam. |br|

CSP.LMC VlbiBeamCapability API Documentation
--------------------------------------------
.. autoclass:: ska_csp_lmc_common.CspVlbiBeamCapability.CspVlbiBeamCapability
   :members:
   :private-members:
   :special-members:
   :show-inheritance:
   :undoc-members:
   :noindex:

.. _CSP-CapabilityMonitor:

CSP.LMC CapabilityMonitor
=========================
This device...

CSP.LMC CapabilityMonitor API Documentation
-------------------------------------------
.. autoclass:: ska_csp_lmc_common.CspCapabilityMonitor.CspCapabilityMonitor
   :members:
   :private-members:
   :special-members:
   :show-inheritance:
   :undoc-members:
   :noindex:


