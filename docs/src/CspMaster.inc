The CSP Master provides API for monitor and control the CSP Element. CSP Master implements CSP Element-level
status indicators, configuration parameters, housekeeping commands. |br|
CSP Master is the primary point of access for CSP Monitor and Control. |br|
CSP Master maintains the pool of schedulable resources, and it can relies on the 
CSP CapabilityMonitor device, as needed.

CspMaster API  Documentation 
============================

.. autoclass:: ska_csp_lmc_common.CspMaster.CspMaster
   :members:
   :noindex:
   :undoc-members:
   :member-order:
   :show-inheritance:

