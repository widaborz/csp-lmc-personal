# -*- coding: utf-8 -*-
#
# This file is part of the CspCapabilityMonitor project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC Common  Class

CSP.LMC Common Class designed to monitoring the SCM
attributes of the CSP Capabilities: SearchBeams, TimingBeams,
VlbiBeams, Receptors/Stations.
"""
# PROTECTED REGION ID (CspMaster.standardlibray_import) ENABLED START #
# Python standard library
from __future__ import absolute_import
import sys
import os
from collections import defaultdict
# PROTECTED REGION END# //CspMaster.standardlibray_import
# tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, EventType
# Additional import
# PROTECTED REGION ID(CspCapabilityMonitor.additionnal_import) ENABLED START #
from ska.base import SKABaseDevice
from ska.base import utils
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
# PROTECTED REGION END #    //  CspCapabilityMonitor.additionnal_import

__all__ = ["CspCapabilityMonitor", "main"]

class CspCapabilityMonitor(SKABaseDevice):
    """
    CSP.LMC Common Class designed to monitoring the SCM
    attributes of the CSP Capabilities: SearchBeams, TimingBeams,
    VlbiBeams, Receptors/Stations.

    **Properties:**

    - Device Property
    
    CapabilityDevices
        - The list of the CSP Capability devices FQDNs monitored by the instance.
        - Type:'DevVarStringArray'

    
    """
    # PROTECTED REGION ID(CspCapabilityMonitor.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspCapabilityMonitor.class_variable

    # -----------------
    # Device Properties
    # -----------------
        
    CapabilityDevices = device_property(
        dtype='DevVarStringArray',
    )

    # ----------
    # Attributes
    # ----------

    numOfUnassignedIDs = attribute(
        dtype='DevUShort',
    )

    capabilityState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Capability State",
        doc="Report the State of the capabilities.",
    )

    capabilityHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
    )

    capabilityAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=1500,
    )

    capabilityObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
    )

    unassignedIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
    )

    capabilityAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=1500,
        label="CSP.LMC Capability devices FQDNs",
        doc="The FQDNs of the CSP.LMC Capability devices: SearchBeams, TimingBeams, VlbiBeams,\nReceptors (MID instance), Stations (LOW instance)",
    )
    
    capabilityMembership = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="Capability sub-array affiliation",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspCapabilityMonitor."""
        SKABaseDevice.init_device(self)
        # PROTECTED REGION ID(CspCapabilityMonitor.init_device) ENABLED START #
        self.set_state(tango.DevState.INIT)

        # _capability_fqdn
        # Python list of the FQDNs of the Capability devices 
        self._capability_fqdn = []

        # _capability_proxies
        # Python dictionary  with the proxies the Capability devices 
        # keys: capability fqdn
        # values: tango proxy
        self._capability_proxies = {}

        # _event_id
        # Python dictionary  with the event ids of the subscribed attributes 
        # keys: capability fqdn
        # values: event id
        self._event_id = defaultdict(lambda: defaultdict(lambda: 0))


        # _capability_state
        # Python dictionary  with the State of each capability
        # keys: capability fqdn
        # values: capability device State
        self._capability_state = {}

        # _capability_health_state
        # Python dictionary  with the healthState of each capability
        # keys: capability fqdn
        # values: capability healthState
        self._capability_health_state = {}

        # _capability_admin_mode
        # Python dictionary  with the adminMode of each capability
        # keys: capability fqdn
        # values: capability adminMode
        self._capability_admin_mode = {}

        # _capability_obs_state
        # Python dictionary  with the obsSate of each capability
        # keys: capability fqdn
        # values: capability obsState
        self._capability_obs_state = {} 

        # _capability_membership
        # Python dictionary with the subarray affiliation (subarrayID
        # value) of each capability
        # keys: capability fqdn
        # values: capability affiliation (= subarrayID, 0 if not assigned to
        #         any subarray)
        self._capability_membership = {}

        # self.CapabilityDevices type is <class 'tango._tango.StdStringVector'>
        # copy to a Python list
        for fqdn in self.CapabilityDevices: 
            self._capability_fqdn.append(fqdn)
        
        # initialization
        self._unassigned_ids = [0]* len(self._capability_fqdn)
        self._capability_state = {fqdn:tango.DevState.DISABLE for fqdn in self._capability_fqdn}
        self._capability_health_state = {fqdn:HealthState.UNKNOWN for fqdn in self._capability_fqdn}
        self._capability_admin_mode = {fqdn:AdminMode.NOT_FITTED for fqdn in self._capability_fqdn}
        self._capability_obs_state = {fqdn:ObsState.IDLE for fqdn in self._capability_fqdn} 
        self._capability_membership = {fqdn:0 for fqdn in self._capability_fqdn}

        # establish connection with the CSP.LMC TANGO DB
        self._csp_tango_db = 0
        self._csp_tango_db = tango.Database()
        attribute_properties = self._csp_tango_db.get_device_attribute_property(self.get_name(),
                                                                          {'adminMode': ['__value']})
        try:
            admin_mode_memorized = attribute_properties['adminMode']['__value']
            self._admin_mode = int(admin_mode_memorized[0])
        except KeyError as key_err:
            self.logger.info("No {} key found".format(str(key_err)))
        self._connect_to_capability()
        self._healthState = HealthState.OK
        self.set_state(tango.DevState.ON)
        
    def _connect_to_capability(self):
        # connect to DB and read the adminMode of each capability
        
        for fqdn in self.CapabilityDevices:
            self._event_id[fqdn] = []
            attribute_properties = self._csp_tango_db.get_device_attribute_property(fqdn, {'adminMode': ['__value'],})
            try:
                admin_mode_memorized = attribute_properties['adminMode']['__value']
                self._capability_admin_mode[fqdn] = int(admin_mode_memorized[0])
            except KeyError as key_err:
                self.logger.warning("_connect_to_capability: No key {} found.".format(str(key_err)))
            try:
                device_proxy = tango.DeviceProxy(fqdn)
                self._capability_proxies[fqdn] = device_proxy
                ev_id = device_proxy.subscribe_event("adminMode",
                                                     EventType.CHANGE_EVENT,
                                                     self._attributes_change_event_cb,
                                                     stateless=True)
                self._event_id[fqdn].append(ev_id)
                
                ev_id = device_proxy.subscribe_event("State",
                                                     EventType.CHANGE_EVENT,
                                                     self._attributes_change_event_cb,
                                                     stateless=True)
                self._event_id[fqdn].append(ev_id)

                ev_id = device_proxy.subscribe_event("healthState",
                                                     EventType.CHANGE_EVENT,
                                                     self._attributes_change_event_cb,
                                                     stateless=True)
                self._event_id[fqdn].append(ev_id)
                
                ev_id = device_proxy.subscribe_event("obsState",
                                                     EventType.CHANGE_EVENT,
                                                     self._attributes_change_event_cb,
                                                     stateless=True)
                self._event_id[fqdn].append(ev_id)
                
                ev_id = device_proxy.subscribe_event("subarrayMemebership",
                                                     EventType.CHANGE_EVENT,
                                                     self._attributes_change_event_cb,
                                                     stateless=True)
                self._event_id[fqdn].append(ev_id)
            except tango.DevFailed as tango_err:
                self.logger.warning("_connect_to_capability: {}.".format(tango_err.args[0].desc))
                
    def _attributes_change_event_cb(self, evt):
        dev_name = evt.device.dev_name()
        if not evt.err:
            try:
                if dev_name in self.CapabilityDevices:
                    if evt.attr_value.name.lower() == "state":
                        self._capability_state[dev_name] = evt.attr_value.value
                        
                    elif evt.attr_value.name.lower() == "healthstate":
                        self._capability_health_state[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "adminmode":
                        self._capability_admin_mode[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "obsstate":
                        self._capability_obsstate[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "subarrayMembershp":
                        self._capability_membership[dev_name] = evt.attr_value.value   
                    else:
                        self.logger.info("Attribute {} not still "
                                         "handled".format(evt.attr_name))
                else:
                    self.logger.info("Unexpected change event for"
                                     " attribute: {}".format(str(evt.attr_name)))
                    return
                log_msg = "New value for {} is {}".format(str(evt.attr_name),
                                                          str(evt.attr_value.value))
            except tango.DevFailed as df:
                self.logger.error(str(df.args[0].desc))
            except Exception as except_occurred:
                self.logger.error(str(except_occurred))
        else:
            for item in evt.errors:
                # API_EventTimeout: if sub-element device not reachable it transitions
                # to UNKNOWN state.
                if item.reason == "API_EventTimeout":
                    self._capability_state[dev_name] = tango.DevState.DISABLED
                    self._capability_health_state[dev_name] = HealthState.UNKNOWN
                    self._capability_admin_mode[dev_name] = AdminMode.NOT_FITTED
                    self._capability_health_state[dev_name] = ObsState.IDLE
                log_msg = item.reason + ": on attribute " + str(evt.attr_name)
                self.logger.warning(log_msg)
                
    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspCapabilityMonitor.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspCapabilityMonitor.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspCapabilityMonitor.delete_device) ENABLED START #
        for fqdn in self._capability_fqdn:
            self.logger.info("delete: {}".format(fqdn))
            event_to_remove = []
            try:
                for ev_id in self._event_id[fqdn]:
                    self.logger.info("ev_id:{}".format(ev_id))
                    try: 
                        self._capability_proxies[fqdn].unsubscribe_event(ev_id)
                        event_to_remove.append(ev_id)
                    except KeyError as key_err:
                        msg = ("Failure unsubscribing event {} "
                               "on device {}. Reason: {}".format(ev_id,
                                                             fqdn,
                                                             key_err))
                        self.logger.error(msg)
                self.logger.info(event_to_remove)
                # remove the events id from the list
                for k in event_to_remove:
                    self._event_id[fqdn].remove(k)
                if self._event_id[fqdn]:
                    msg = "Still subscribed events: {}".format(self._event_id)
                    self.logger.warning(msg)
                else:
                    # remove the dictionary element
                    self._event_id.pop(fqdn)
                    self.logger.info(self._event_id)
            except KeyError as key_err:
                msg = " Can't retrieve the information of key {}".format(key_err)
                self.logger.error(msg)
            
        self.logger.info("1")
        self.logger.info("self._capability_fqdn:{}".format(self._capability_fqdn))
        self.logger.info("self._capability_fqdn:{}".format(type(self._capability_fqdn)))
        #self._capability_fqdn.clear()
        self.logger.info("2")
        self._capability_state.clear()
        self.logger.info("3")
        self._capability_health_state.clear()
        self.logger.info("4")
        self._capability_admin_mode.clear()
        self.logger.info("5")
        self._capability_obs_state.clear() 
        self.logger.info("6")
        self._capability_membership.clear()
        self.logger.info("7")
        # PROTECTED REGION END #    //  CspCapabilityMonitor.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_numOfUnassignedIDs(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.numOfUnassignedIDs_read) ENABLED START #
        """Return the numOfUnassignedIDs attribute."""
        return len(self._unassigned_ids)
        # PROTECTED REGION END #    //  CspCapabilityMonitor.numOfUnassignedIDs_read

    def read_capabilityState(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityState_read) ENABLED START #
        """Return the capabilityState attribute."""
        return list(self._capability_state.values())
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityState_read

    def read_capabilityHealthState(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityHealthState_read) ENABLED START #
        """Return the capabilityHealthState attribute."""
        return list(self._capability_health_state.values())
       
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityHealthState_read

    def read_capabilityAdminMode(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityAdminMode_read) ENABLED START #
        """Return the capabilityAdminMode attribute."""
        return list(self._capability_admin_mode.values())
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityAdminMode_read

    def write_capabilityAdminMode(self, value):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityAdminMode_write) ENABLED START #
        """Set the capabilityAdminMode attribute."""
        pass
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityAdminMode_write

    def read_capabilityObsState(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityObsState_read) ENABLED START #
        """Return the capabilityObsState attribute."""
        return list(self._capability_obs_state.values())
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityObsState_read

    def read_unassignedIDs(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.unassignedIDs_read) ENABLED START #
        """Return the unassignedIDs attribute."""
        return self._unassigned_ids
        # PROTECTED REGION END #    //  CspCapabilityMonitor.unassignedIDs_read

    def read_capabilityAddresses(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.capabilityAddresses_read) ENABLED START #
        return self.CapabilityDevices
        # PROTECTED REGION END #    //  CspCapabilityMonitor.capabilityAddresses_read
    
    def read_capabilityMembership(self):
        # PROTECTED REGION ID(CspCapabilityMonitor.membership_read) ENABLED START #
        """Return the membership attribute."""
        return list(self._capability_membership.values())
        # PROTECTED REGION END #    //  CspCapabilityMonitor.membership_read
    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------

def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspCapabilityMonitor.main) ENABLED START #
    return run((CspCapabilityMonitor,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspCapabilityMonitor.main

if __name__ == '__main__':
    main()
