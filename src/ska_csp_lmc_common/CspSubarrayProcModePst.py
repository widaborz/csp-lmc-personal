# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarrayProcModePst project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Subarray PST Capability

The class exposes parameters and commands required
for monitor and control of the Procesing Mode PST in a
single subarray.
It is used as base for developement of LOW and MID
specific capabilities.
"""
# PROTECTED REGION ID (CspSubarrayProcModePst.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os
# PROTECTED REGION END# //CspSubarrayIProcModePst.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSubarrayProcModePst.additionnal_import) ENABLED START #
from .CspSubarrayInherentCapability import CspSubarrayInherentCapability
# PROTECTED REGION END #    //  CspSubarrayProcModePst.additionnal_import

__all__ = ["CspSubarrayProcModePst", "main"]


class CspSubarrayProcModePst(CspSubarrayInherentCapability):
    """
    The class exposes parameters and commands required
    for monitor and control of the Procesing Mode PST in a
    single subarray.
    It is used as base for developement of LOW and MID
    specific capabilities.

    **Properties:**

    - Device Property
   """
    # PROTECTED REGION ID(CspSubarrayProcModePst.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarrayProcModePst.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------

















    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSubarrayProcModePst."""
        CspSubarrayInherentCapability.init_device(self)
        # PROTECTED REGION ID(CspSubarrayProcModePst.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModePst.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarrayProcModePst.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModePst.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarrayProcModePst.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModePst.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarrayProcModePst.main) ENABLED START #
    return run((CspSubarrayProcModePst,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarrayProcModePst.main

if __name__ == '__main__':
    main()
