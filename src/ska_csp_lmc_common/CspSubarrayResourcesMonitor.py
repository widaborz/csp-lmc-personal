# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarrayResourcesMonitor project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Subarray Resource Monitor

The class implementes the monitoring of all the resources
allocated to the CSP Subarray.
"""

# tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSubarrayResourcesMonitor.additionnal_import) ENABLED START #
from ska.base import SKABaseDevice
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
# PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.additionnal_import

__all__ = ["CspSubarrayResourcesMonitor", "main"]


class CspSubarrayResourcesMonitor(SKABaseDevice):
    """
    The class implementes the monitoring of all the resources
    allocated to the CSP Subarray.

    **Properties:**

    - Device Property
    
    
    
    
    """
    # PROTECTED REGION ID(CspSubarrayResourcesMonitor.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.class_variable

    # -----------------
    # Device Properties
    # -----------------





    # ----------
    # Attributes
    # ----------










    assignedSearchBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Assigned SearchBeams State",
        polling_period=3000,
        doc="State of the assigned SearchBeams",
    )

    assignedTimingBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="Assigned TimingBeams State",
        polling_period=3000,
        doc="State of the assigned TimingBeams",
    )

    assignedVlbiBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="Assigned VlbiBeams State",
        polling_period=3000,
        doc="State of the assigned VlbiBeams",
    )

    assignedSearchBeamsHealthState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Assigned SearchBeams HealthState",
        polling_period=3000,
        doc="HealthState of the assigned SearchBeams",
    )

    assignedTimingBeamsHealthState = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="Assigned TimingBeams HealthState",
        polling_period=3000,
        doc="HealthState of the assigned TimingBeams",
    )

    assignedVlbiBeamsHealthState = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="Assigned VlbiBeams HealthState",
        polling_period=3000,
        doc="HealthState of the assigned VlbiBeams",
    )

    assignedSearchBeamsObsState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Assigned SearchBeams ObsState",
        polling_period=3000,
        doc="ObsState of the assigned SearchBeams",
    )

    assignedTimingBeamsObsState = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="Assigned TimingBeams ObsState",
        polling_period=3000,
        doc="ObsState of the assigned TimingBeams",
    )

    assignedVlbiBeamsObsState = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="Assigned VlbiBeams ObsState",
        polling_period=3000,
        doc="ObsState of the assigned VlbiBeams",
    )

    assignedSearchBeamsAdminMode = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Assigned SearchBeams AdminMode",
        polling_period=3000,
        doc="AdminMode of the assigned SearchBeams",
    )

    assignedTimingBeamsAdminMode = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="Assigned TimingBeams AdminMode",
        polling_period=3000,
        doc="AdminMode of the assigned TimingBeams",
    )

    assignedVlbiBeamsAdminMode = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="Assigned VlbiBeams AdminMode",
        polling_period=3000,
        doc="AdminMode of the assigned VlbiBeams",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSubarrayResourcesMonitor."""
        SKABaseDevice.init_device(self)
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_assignedSearchBeamsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedSearchBeamsState_read) ENABLED START #
        """Return the assignedSearchBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedSearchBeamsState_read

    def read_assignedTimingBeamsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedTimingBeamsState_read) ENABLED START #
        """Return the assignedTimingBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedTimingBeamsState_read

    def read_assignedVlbiBeamsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedVlbiBeamsState_read) ENABLED START #
        """Return the assignedVlbiBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedVlbiBeamsState_read

    def read_assignedSearchBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedSearchBeamsHealthState_read) ENABLED START #
        """Return the assignedSearchBeamsHealthState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedSearchBeamsHealthState_read

    def read_assignedTimingBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedTimingBeamsHealthState_read) ENABLED START #
        """Return the assignedTimingBeamsHealthState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedTimingBeamsHealthState_read

    def read_assignedVlbiBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedVlbiBeamsHealthState_read) ENABLED START #
        """Return the assignedVlbiBeamsHealthState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedVlbiBeamsHealthState_read

    def read_assignedSearchBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedSearchBeamsObsState_read) ENABLED START #
        """Return the assignedSearchBeamsObsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedSearchBeamsObsState_read

    def read_assignedTimingBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedTimingBeamsObsState_read) ENABLED START #
        """Return the assignedTimingBeamsObsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedTimingBeamsObsState_read

    def read_assignedVlbiBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedVlbiBeamsObsState_read) ENABLED START #
        """Return the assignedVlbiBeamsObsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedVlbiBeamsObsState_read

    def read_assignedSearchBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedSearchBeamsAdminMode_read) ENABLED START #
        """Return the assignedSearchBeamsAdminMode attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedSearchBeamsAdminMode_read

    def read_assignedTimingBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedTimingBeamsAdminMode_read) ENABLED START #
        """Return the assignedTimingBeamsAdminMode attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedTimingBeamsAdminMode_read

    def read_assignedVlbiBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarrayResourcesMonitor.assignedVlbiBeamsAdminMode_read) ENABLED START #
        """Return the assignedVlbiBeamsAdminMode attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.assignedVlbiBeamsAdminMode_read


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarrayResourcesMonitor.main) ENABLED START #
    return run((CspSubarrayResourcesMonitor,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarrayResourcesMonitor.main

if __name__ == '__main__':
    main()
