# -*- coding: utf-8 -*-
#
# This file is part of the CspVlbiBeamCapability project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP  TimingBeam Capability

The class models the Capability VlbiBeam exsposing
the attributes and commands used to monitor and control beamforming
and VLBI processing in a single beam.
Used for development of LOW and MID specific devices.
"""
# PROTECTED REGION ID (CspVlbiBeamCapability.standardlibray_import) ENABLED START #
# Python standard library
# PROTECTED REGION END# //CspVlbiBeamCapability.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspVlbiBeamCapability.additionnal_import) ENABLED START #
from ska.base import SKACapability
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
# PROTECTED REGION END #    //  CspVlbiBeamCapability.additionnal_import

__all__ = ["CspVlbiBeamCapability", "main"]


class CspVlbiBeamCapability(SKACapability):
    """
    The class models the Capability VlbiBeam exsposing
    the attributes and commands used to monitor and control beamforming
    and VLBI processing in a single beam.
    Used for development of LOW and MID specific devices.

    **Properties:**

    - Device Property
    """

    # PROTECTED REGION ID(CspVlbiBeamCapability.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspVlbiBeamCapability.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------















    subarrayMembership = attribute(
        dtype='DevUShort',
        label="The Beam Capability subarray affiliation.",
        doc="The subarray ID the CSP Beam Capability belongs to.",
    )



    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspVlbiBeamCapability."""
        SKACapability.init_device(self)
        # PROTECTED REGION ID(CspVlbiBeamCapability.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspVlbiBeamCapability.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspVlbiBeamCapability.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspVlbiBeamCapability.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspVlbiBeamCapability.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspVlbiBeamCapability.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_subarrayMembership(self):
        # PROTECTED REGION ID(CspVlbiBeamCapability.subarrayMembership_read) ENABLED START #
        """Return the subarrayMembership attribute."""
        return 0
        # PROTECTED REGION END #    //  CspVlbiBeamCapability.subarrayMembership_read

    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the CspVlbiBeamCapability module."""
    # PROTECTED REGION ID(CspVlbiBeamCapability.main) ENABLED START #
    return run((CspVlbiBeamCapability,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspVlbiBeamCapability.main


if __name__ == '__main__':
    main()
