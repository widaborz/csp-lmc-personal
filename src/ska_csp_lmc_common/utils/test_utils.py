import time
import logging
import numpy
from tango import DeviceProxy, DevState

LOGGER = logging.getLogger(__name__)


class Timeout:
    def __init__(self, duration):
        self.endtime = time.time() + duration

    def has_expired(self):
        return time.time() > self.endtime


class Probe:
    def __init__(self, proxy, attr_name, expected_state, message):
        """
        """
        self.proxy = proxy
        self.attr_name = attr_name
        self.expected_state = expected_state
        self.message = message
        self.current_state = DevState.DISABLE
 
    def get_attribute(self):
        return self.attr_name

    def sample(self):
        """
        extract the state of client and store it
        """
        device_attr = self.proxy.read_attribute(self.attr_name)
        self.current_state = device_attr.value
        #LOGGER.info("attr_name: {} current_state:{}".format(self.attr_name, self.current_state))
 
    def is_satisfied(self):
        """
        Check if the state satisfies this test condition
        """

        if isinstance(self.current_state, numpy.ndarray):
            return (self.expected_state == self.current_state).all()
        return self.expected_state == self.current_state


class Poller:
    def __init__(self, timeout, interval):
        self.timeout = timeout
        self.interval = interval
 
    def check(self, probe: Probe):
        """
        Repeatedly check if the probe is satisfied.
        Assert false when the timeout expires.
        """
        timer = Timeout(self.timeout)
        probe.sample()
        while not probe.is_satisfied():
            if timer.has_expired():
                LOGGER.debug("Check Timeout on:{}".format(probe.get_attribute()))
                assert False, probe.message
            time.sleep(self.interval)
            probe.sample()
        LOGGER.debug("Check success on: {}".format(probe.get_attribute()))
        assert True
