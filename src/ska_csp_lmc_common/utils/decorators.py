import sys
import os
import tango
import functools
import json
from ska.log_transactions import transaction
from ska.base.control_model import AdminMode,ObsState
from .cspcommons import CmdExecState

def transaction_id(func):
    """
    Add a transaction id to the input of the decorated method. 
    The input of the decorated method is a json string.
    
    """
    @functools.wraps(func)
    def wrap(*args, **kwargs):
        
        # Argument of the decorated method can be either positional or keyword. 
        # Following code manage both cases.
        # A keyword argument occurs when applied to "do" method of SKA Base Command Classes.
        # argument must be single.
        # note that args[0] is the "self" object of the class of the decorated method.
        if kwargs:
            keys=list(kwargs.keys())
            argin = kwargs[keys[0]]    
        elif len(args)>1:
            argin = args[1]
        else:
            raise Exception('No argument are passed to the transaction ID decorator')
        parameters =json.loads(argin)
        obj = args[0]

        #note: obj.name is the Command Class Name.
        with transaction(obj.name, parameters,logger=obj.logger) as transaction_id:    

            parameters['transaction_id'] = transaction_id

        argin = json.dumps(parameters)
        return func(obj,argin)
    return wrap

class AdminModeCheck(object):
    """
    Class designed to be a decorator for the CspMaster methods.
    It checks the adminMode attribute value: if not ONLINE or 
    MAINTENANCE it throws an exception.
    """

    def __init__(self, cmd_to_execute):
        self._cmd_to_execute = cmd_to_execute

    def __call__(self, f):
        @functools.wraps(f)
        def admin_mode_check(*args, **kwargs):
            # get the  device instance
            dev_instance = args[0]
            dev_instance.logger.debug(
                "Command to execute: {}".format(self._cmd_to_execute))
            # Check the AdminMode value: the command is callable only if the
            # the administration mode is ONLINE or MAINTENACE

            if dev_instance._admin_mode not in [AdminMode.ONLINE, AdminMode.MAINTENANCE]:
                # NOTE: when adminMode is not ONLINE/MAINTENANCE the device State has to be
                # DISABLE
                # Add only a check on State value to log a warning message if it
                # is different from DISABLE
                msg_args = (self._cmd_to_execute, dev_instance.get_state(
                ), AdminMode(dev_instance._admin_mode).name)
                if dev_instance.get_state() != tango.DevState.DISABLE:
                    dev_instance.logger.warn("Command {}: incoherent device State {} "
                                             " with adminMode {}".format(*msg_args))
                err_msg = ("{} command can't be issued when State is {} and"
                           " adminMode is {} ".format(*msg_args))

                tango.Except.throw_exception("Invalid adminMode value",
                                             err_msg,
                                             "AdminModeCheck decorator",
                                             tango.ErrSeverity.ERR)
            return f(*args, **kwargs)
        return admin_mode_check

class CmdInputArgsCheck(object):
    """
    Class designed to be a decorator for the CspMaster methods.
    The *decorator function* performs a check on the function input argument,
    detecting if the corresponding servers are running and the 
    administrative mode of the devices has the right value to execute a command (i.e
    ONLINE/MAINTENACE)
    The *decorator function* accepts some parameters as input to customize
    its functionality
    """

    def __init__(self, *args, **kwargs):
        # store the decorators parameters:
        # args: the list of sub-element attributes to subscribe, to track the
        #       sub-element command progress and detect a command timeout
        # kwargs: a dictionary: key ="cmd_name",
        #                       value = command to execute ('on', 'off'...)
        #
        self._args = args
        self._kwargs = kwargs

    def __call__(self, f):
        @functools.wraps(f)
        def input_args_check(*args, **kwargs):
            # the CspMaster device instance
            dev_instance = args[0]
            # get the information passed as arguments to the decorator
            attrs_to_subscribe = self._args
            try:
                cmd_to_exec = self._kwargs['cmd_name']
            except KeyError as key_err:
                msg = ("No key found for {}".format(str(key_err)))
                dev_instance.logger.warn(msg)
                tango.Except.throw_exception("Command failure",
                                             "Error in input decorator args",
                                             "CmdInputArgsCheck decorator",
                                             tango.ErrSeverity.ERR)
            input_arg = args[1]
            device_list = input_arg
            # Note: device list is a reference to args[1]: changing
            # device_list content, args[1] changes accordingly!
            num_of_devices = len(input_arg)
            if num_of_devices == 0:
                # no input argument -> switch on all sub-elements
                num_of_devices = len(dev_instance._se_fqdn)
                # !!Note!!:
                # need to copy inside device_list to maintain the reference to args[1]
                # if we do
                # device_list = dev_instance._se_fqdn
                # we get a new variable address and we lose the reference to args[1]
                for fqdn in dev_instance._se_fqdn:
                    device_list.append(fqdn)
            else:
                if num_of_devices > len(dev_instance._se_fqdn):
                    # too many devices specified-> log the warning but go on
                    # with command execution
                    dev_instance.logger.warn("Too many input parameters")
            dev_instance.logger.debug(
                "Devices {} to check:".format(device_list))
            # If a sub-element device is already executing a power command, an exception is
            # thrown only when the requested command is different from the one
            # already running (power commands have to be executed sequentially).
            # TODO:
            # What to do if the required device is performing a software upgrade or is changing its
            # adminMode? How can CSP.LMC detect this condition?
            dev_instance.logger.info("Command to execute:{}".format(cmd_to_exec))
            dev_instance.logger.info("_cmd_execution_state: {}".format(dev_instance._cmd_execution_state.items()))
            list_of_running_cmd = [cmd_name for cmd_name, cmd_state in dev_instance._cmd_execution_state.items()
                                   if cmd_state == CmdExecState.RUNNING]
            dev_instance.logger.info("list of running commands: {}".format(list_of_running_cmd))
            if list_of_running_cmd:
                # if a command is running, check if its the requested one
                if len(list_of_running_cmd) > 1:
                    # should not happen!! throw an exception
                    err_msg = "{} power commands are running. Something strange!".format(
                        len(list_of_running_cmd))
                    tango.Except.throw_exception("Command failure",
                                                 err_msg,
                                                 "CmdInputArgsCheck decorator",
                                                 tango.ErrSeverity.ERR)
                if list_of_running_cmd[0] == cmd_to_exec:
                    # the requested command is already running
                    # return with no exception (see SKA Guidelines)
                    msg = ("Command {} is already running".format(cmd_to_exec))
                    dev_instance.logger.info(msg)
                    return
                else:
                    # another power command is already running
                    err_msg = ("Can't execute command {}:"
                               " power command {} is already running".format(cmd_to_exec, list_of_running_cmd[0]))
                    tango.Except.throw_exception("Command failure",
                                                 err_msg,
                                                 "CmdInputArgsCheck decorator",
                                                 tango.ErrSeverity.ERR)
            # check for devices that are not ONLINE/MAINTENANCE
            device_to_remove = []
            for device in device_list:
                dev_instance.logger.debug(
                    "CmdInputArgsCheack-processing device:{}".format(device))
                # if the sub-element device server is not running or the adminMode
                # attribute is not ONLINE or MAINTENANCE:
                # - schedule the device for removing from the input arg list
                # - skip to next device and
                if (not dev_instance._is_device_running(device, dev_instance._se_proxies) or
                    dev_instance._se_admin_mode[device] not in [AdminMode.ONLINE,
                                                                AdminMode.MAINTENANCE]):
                    if device == dev_instance.CspCbf:
                        msg = ("Can't  execute command {} with "
                               "device {} {}".format(cmd_to_exec,
                                                     device,
                                                     AdminMode(dev_instance._se_admin_mode[device]).name))
                        tango.Except.throw_exception("Command failure",
                                                     msg,
                                                     "CmdInputArgsCheck decorator",
                                                     tango.ErrSeverity.ERR)
                    device_to_remove.append(device)
                    dev_instance.logger.debug(
                        "Devices to remove from the list:{}".format(device_to_remove))
                    continue
            for device in device_to_remove:
                device_list.remove(device)
            if not device_list:
                dev_instance.logger.info("No device to power on")
                return
            command_timeout = 0
            # build the sub-element attribute name storing the command duration
            # expected value.
            cmd_time_attr_name = cmd_to_exec + "DurationExpected"
            for device in device_list:
                dev_instance.logger.debug("Processing device: {}".format(device))
                # set to QUEUED. This state can be useful in case of command abort.
                dev_instance._se_cmd_execution_state[device][cmd_to_exec] = CmdExecState.QUEUED
                # reset the timeout and failure attribute content
                dev_instance._timeout_expired[cmd_to_exec] = False
                # TODO: how check if the failure condition has been reset by AlarmHandler?
                dev_instance._failure_raised[cmd_to_exec] = False
                device_proxy = dev_instance._se_proxies[device]
                try:
                    # get the sub-element value for the onCommandDurationExpected attribute.
                    # If this attribute is not implemented at sub-element level,
                    # the default value is used.
                    # Note: if onCmdDurationExpected attribute is not implemented, the
                    # call device_proxy.onCmdDurationExpected throws an AttributeError exception
                    # (not tango.DevFailed)
                    #dev_instance._se_cmd_duration_expected[device][cmd_to_exec] = device_proxy.onCmdDurationExpected
                    # read_Attribute returns a DeviceAttribute object
                    device_attr = device_proxy.read_attribute(
                        cmd_time_attr_name)
                    dev_instance._se_cmd_duration_expected[device][cmd_to_exec] = device_attr.value
                except tango.DevFailed as tango_err:
                    # we get here if the attribute is not implemented
                    dev_instance.logger.info(tango_err.args[0].desc)
                for attr in attrs_to_subscribe:
                    try:
                        if dev_instance._se_event_id[device][attr.lower()] == 0:
                            evt_id = device_proxy.subscribe_event(attr, tango.EventType.CHANGE_EVENT,
                                                                  dev_instance._attributes_change_evt_cb, stateless=False)
                            dev_instance._se_event_id[device][attr.lower(
                            )] = evt_id
                    except tango.DevFailed as tango_err:
                        dev_instance.logger.info(tango_err.args[0].desc)
                # evaluate the total timeout value to execute the whole command
                # note: if the xxxDurationExpected attribute read fails, it is used the default value
                # (xxxDurationExpected  initialized as defaultdict)
                command_timeout += dev_instance._se_cmd_duration_expected[device][cmd_to_exec]
            # device loop end
            # use the greatest value for the onCommand duration expected.
            if command_timeout > dev_instance._cmd_duration_expected[cmd_to_exec]:
                dev_instance._cmd_duration_expected[cmd_to_exec] = command_timeout
                dev_instance.logger.info(
                    "Modified the {} command Duration Expected value!!".format(cmd_to_exec))
            return f(*args, **kwargs)
        return input_args_check


class SubarrayRejectCmd(object):
    """
    Class designed to be a decorator for the CspSubarray methods.
    The *decorator function* controls if the execution of a sub-array
    command to assign/remove/configure resources conflicts with a command already
    running.
    The *decorator function* accepts some parameters as input to customize
    its functionality.
    """

    def __init__(self, *args, **kwargs):
        # store the decorators parameters:
        # args: the list of command to reject
        #
        self._args = args
        self._kwargs = kwargs

    def __call__(self, f):
        @functools.wraps(f)
        def input_args_check(*args, **kwargs):
            # the CspSubarray device instance
            dev_instance = args[0]
            cmd_to_exec = f.__name__
            commands_to_reject = self._args
            dev_instance.logger.info(
                    "Command to execute: {}".format(f.__name__))
            dev_instance.logger.info(
                    "Device state : {} obs_state: {}".format(dev_instance.get_state(), dev_instance._obs_state))
            # If a sub-array is executing an AddXXX (RemoveXX) command, an exception is
            # thown only when the requested command is RemoveXXX (AddXXX)remove/add resources or ConfigureScan.
            # If the requested command is already running, the decorator returns.

            list_of_running_cmd = [cmd_name for cmd_name, cmd_state in dev_instance._cmd_execution_state.items()
                                   if cmd_state == CmdExecState.RUNNING]
            dev_instance.logger.info("list of running commands: {}".format(list_of_running_cmd))
            if list_of_running_cmd:
                dev_instance.logger.info("Commands {} are running".format(list_of_running_cmd))
            if cmd_to_exec.lower() in list_of_running_cmd:
                # the requested command is already running
                # return with no exception (see SKA Guidelines)
                msg = ("Command {} is already running".format(cmd_to_exec))
                dev_instance.logger.info(msg)
                return
            # Change the name of commands to reject in lower-case letters to match
            # the name of the running commands (if any)
            commands_to_reject = map(str.lower, commands_to_reject)
            if any(cmd_name in commands_to_reject for cmd_name in list_of_running_cmd):
                err_msg = ("Can't execute command {}:"
                           " command {} is already running".format(cmd_to_exec, list_of_running_cmd))
                dev_instance.logger.error(err_msg)
                tango.Except.throw_exception("Command failure",
                                             err_msg,
                                             "SubarrayRejectCmd decorator",
                                             tango.ErrSeverity.ERR)
            # reset failure/timeout condition
            if dev_instance._failure_raised:
                dev_instance.logger.info(
                    "A previous failure condition is present")
                dev_instance._failure_raised = False
            if dev_instance._timeout_expired:
                dev_instance.logger.info(
                    "A previous timeout condition is present")
                dev_instance._timeout_expired = False
            # TODO: how check if the failure condition has been reset by AlarmHandler?
            return f(*args, **kwargs)
        return input_args_check
