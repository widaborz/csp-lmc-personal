# -*- coding: utf-8 -*-
#
# This file is part of the CspSearchBeamCapability project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP  SearchBeam Capability

The class models the Capability Search Beam exsposing
 the attributes and commands used to monitor and control beamforming
and PSS pulsar search in a single beam.
In particular it maps components used for beamforming with those
that perform PSS processing (pulsar search and transients).
Used for development of LOW and MID specific devices.
"""
# PROTECTED REGION ID (CspSearchBeamCapability.standardlibray_import) ENABLED START #
# Python standard library
# PROTECTED REGION END# //CspSearchBeamCapability.standardlibray_import
# tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSearchBeamCapability.additionnal_import) ENABLED START #
from ska.base import SKACapability
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
# PROTECTED REGION END #    //  CspSearchBeamCapability.additionnal_import

__all__ = ["CspSearchBeamCapability", "main"]


class CspSearchBeamCapability(SKACapability):
    """
    The class models the Capability Search Beam exsposing
    the attributes and commands used to monitor and control beamforming
    and PSS pulsar search in a single beam.
    In particular it maps components used for beamforming with those
    that perform PSS processing (pulsar search and transients).
    Used for development of LOW and MID specific devices.

    **Properties:**

    - Device Property
    
    """
    # PROTECTED REGION ID(CspSearchBeamCapability.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSearchBeamCapability.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    pssPipelineTangoAddr = attribute(
        dtype='DevString',
        label="The PSS pipeline name",
        doc="The PssPipeline TANGO Device FQDN.",
    )

    pssPipelineIPAddr = attribute(
        dtype='DevString',
        label="PSS Node IP address",
        doc="The IP address of the PSS node hosting the PssPipeline TANGO Device and running the\ndata reduction pipeline.",
    )

    pssPipelineIPPort = attribute(
        dtype='DevUShort',
        label="The PSS data pipeline IP port",
        doc="The IP port the data reduction processing pipeline is listening to receive commands and\ninput parameters.",
    )

    pssPipelineState = attribute(
        dtype='DevState',
        label="The PssPipeline State",
        doc="The STate of the PssPipeline TANGO Device. It represents the operational state of the\nPSS data reduction pipeline.",
    )

    pssPipelineAdminMode = attribute(
        dtype='DevEnum',
        access=AttrWriteType.READ_WRITE,
        label="The PssPipeline adminMode",
        doc="The adminMode of the PssPipeline TANGO Device.",
    )

    pssPipelineObsState = attribute(
        dtype='DevEnum',
        label="The PssPipeline obsState",
        doc="The obsState of the PssPipeline TANGO Device.",
    )

    pssPipelineObsMode = attribute(
        dtype='DevEnum',
        label="The PssPipeline obsMode",
        doc="The obsMode of the PSsPipeline TANGO Device.",
    )

    subarrayMembership = attribute(
        dtype='DevUShort',
        label="The Beam Capability subarray affiliation.",
        doc="The subarray ID the CSP Beam Capability belongs to.",
    )



    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSearchBeamCapability."""
        SKACapability.init_device(self)
        # PROTECTED REGION ID(CspSearchBeamCapability.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSearchBeamCapability.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSearchBeamCapability.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSearchBeamCapability.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSearchBeamCapability.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSearchBeamCapability.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_pssPipelineTangoAddr(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineTangoAddr_read) ENABLED START #
        """Return the pssPipelineTangoAddr attribute."""
        return ''
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineTangoAddr_read

    def read_pssPipelineIPAddr(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineIPAddr_read) ENABLED START #
        """Return the pssPipelineIPAddr attribute."""
        return ''
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineIPAddr_read

    def read_pssPipelineIPPort(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineIPPort_read) ENABLED START #
        """Return the pssPipelineIPPort attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineIPPort_read

    def read_pssPipelineState(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineState_read) ENABLED START #
        """Return the pssPipelineState attribute."""
        return tango.DevState.UNKNOWN
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineState_read

    def read_pssPipelineAdminMode(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineAdminMode_read) ENABLED START #
        """Return the pssPipelineAdminMode attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineAdminMode_read

    def write_pssPipelineAdminMode(self, value):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineAdminMode_write) ENABLED START #
        """Set the pssPipelineAdminMode attribute."""
        pass
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineAdminMode_write

    def read_pssPipelineObsState(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineObsState_read) ENABLED START #
        """Return the pssPipelineObsState attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineObsState_read

    def read_pssPipelineObsMode(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.pssPipelineObsMode_read) ENABLED START #
        """Return the pssPipelineObsMode attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSearchBeamCapability.pssPipelineObsMode_read

    def read_subarrayMembership(self):
        # PROTECTED REGION ID(CspSearchBeamCapability.subarrayMembership_read) ENABLED START #
        """Return the subarrayMembership attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSearchBeamCapability.subarrayMembership_read

    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the CspSearchBeamCapability module."""
    # PROTECTED REGION ID(CspSearchBeamCapability.main) ENABLED START #
    return run((CspSearchBeamCapability,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSearchBeamCapability.main


if __name__ == '__main__':
    main()
