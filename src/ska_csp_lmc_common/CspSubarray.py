# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC Common CspSubarray

CSP subarray functionality is modeled via a TANGCSP.LMC Common Class for the CSPSubarray TANGO Device.
"""

# PROTECTED REGION ID (CspSubarray.standardlibray_import) ENABLED START #
# Python standard library
from __future__ import absolute_import
import sys
import os
from collections import defaultdict
from enum import IntEnum, unique
import threading
import time
import json
# PROTECTED REGION END# //CspSubarray.standardlibray_import
# tango imports
import tango
from tango import DebugIt, EnsureOmniThread
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import class_property, device_property
from tango import AttrQuality, EventType, DevState
from tango import AttrWriteType, DeviceProxy
# Additional import
# PROTECTED REGION ID(CspSubarray.additionnal_import) ENABLED START #
from ska.base import SKASubarray, SKASubarrayStateModel
#from ska.base import utils
from ska.base.commands import ActionCommand, ResultCode
from ska.base.faults import CapabilityValidationError, CommandError,StateModelError
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
from .utils.cspcommons import CmdExecState
from .utils.decorators import transaction_id
from . import release
from .csp_manage_json import JsonConfiguration
# PROTECTED REGION END #    //  CspSubarray.additionnal_import

__all__ = ["CspSubarray", "main"]

class CspSubarrayStateModel(SKASubarrayStateModel):
    """
    Implements the state model for the CSP SKASubarray,
    This new State Model use automatic transitions of the ObservationStateMachine.
    In addition to any transitions added explicitly, a to_«state»() method 
    is created automatically whenever a state is added to a Machine instance. 
    This method transitions to the target state no matter which state the machine is currently in.
    Automatic transitions are used at the server re-initialization to
    align the CSP Subarray with the current state of its
    sub-elements.
    """

    def __init__(
        self,
        logger,
        op_state_callback=None,
        admin_mode_callback=None, 
        obs_state_callback=None,
    ):
        """
        Initialises the model. Note that this does not imply moving to
        INIT state. The INIT state is managed by the model itself.

        :param logger: the logger to be used by this state model.
        :type logger: a logger that implements the standard library
            logger interface
        :param op_state_callback: A callback to be called when a
            transition implies a change to op state
        :type op_state_callback: callable
        :param admin_mode_callback: A callback to be called when a
            transition causes a change to device admin_mode
        :type admin_mode_callback: callable
        :param obs_state_callback: A callback to be called when a
            transition causes a change to device obs_state
        :type obs_state_callback: callable
        """
        super().__init__(
            logger,
            op_state_callback=op_state_callback,
            admin_mode_callback=admin_mode_callback,
            obs_state_callback=obs_state_callback,
        )
        # add direct transition from EMPTY to another observing state.
        self._action_breakdown["force_to_idle"] = ("force_to_idle", None)
        self._action_breakdown["force_to_empty"] = ("force_to_empty", None)
        self._action_breakdown["force_to_ready"] = ("force_to_ready", None)
        self._action_breakdown["force_to_aborted"] = ("force_to_aborted", None)
        self._action_breakdown["force_to_scanning"] = ("force_to_scanning", None)
        # add transtions to the ObservationStateMachine
        self._observation_state_machine.add_transition(trigger='force_to_empty', source='*', dest='EMPTY')
        self._observation_state_machine.add_transition(trigger='force_to_idle', source='*', dest='IDLE')
        self._observation_state_machine.add_transition(trigger='force_to_ready', source='*', dest='READY')
        self._observation_state_machine.add_transition(trigger='force_to_scanning', source='*', dest='SCANNING')
        self._observation_state_machine.add_transition(trigger='force_to_aborted', source='*', dest='ABORTED')

class CspSubarray(SKASubarray):
    """
    CSP subarray functionality is modeled via a TANGCSP.LMC Common Class for the CSPSubarray TANGO Device.

    **Properties:**

    - Class Property
        PSTBeams
            - PST sub-element PSTBeams TANGO devices FQDNs
            - Type:'DevVarStringArray'

    - Device Property 
        CspMaster
            - The TANGO address of the CspMaster.
            - Type:'DevString'

        CbfSubarray 
            - CBF sub-element sub-array TANGO device FQDN
            - Type:'DevString'

        PssSubarray 
            - PST sub-element sub-array TANGO device FQDN.
            - Type:'DevString'

        SubarrayProcModeCorrelation 
            - CSP Subarray *Correlation Inherent Capability* TANGO device FQDN
            - Type:'DevString'

        SubarrayProcModePss 
            - CSP Subarray *Pss Inherent Capability* TANGO device FQDN
            - Type:'DevString'

        SubarrayProcModePst 
            - CSP Subarray *PST Inherent Capability* TANGO device FQDN
            - Type:'DevString'

        SubarrayProcModeVlbi 
            - CSP Subarray *VLBI Inherent Capability* TANGO device FQDN
            - Type:'DevString'

    """

    class ForceObsStateTransitionCommand(ActionCommand):
        """
        Class to handle the re-initialization of the Device server.
        """

        def __init__(self, target, state_model, logger=None):
            """
            Constructor for ForceObsStateTransition.
            It's not a TANGO command. 
            This command in invoked at CSP.LMC Subarray re-initialization.

            :param target: the object that this command acts upon; for
                example, the SKASubarray device for which this class
                implements the command
            :type target: object
            :param state_model: the state model that this command uses
                 to check that it is allowed to run, and that it drives
                 with actions.
            :type state_model: :py:class:`SKASubarrayStateModel`
            :param logger: the logger to be used by this Command. If not
                provided, then a default module logger will be used.
            :type logger: a logger that implements the standard library
                logger interface
            """
            super().__init__(
                target, state_model, "force", start_action=False, logger=logger
            )
            self.action = None

        def __call__(self, argin=None):
            """
            Override the __call__ method to set the action to execute when the succeeded method
            is called.
            """
            self.action = argin
            super().__call__(argin)

        def check_allowed(self):
            """
            check_allowed is invoked before the do() in the command __call__ method.
            The basic behavior is to check for a transition named 'force_succeeded'.
            At re-initialization there are several 'succeeded' transitions, depending on 
            the expected observing state of the CSP Subarray, so it's necessary to modify 
            the succeeded method name with the one stored in the action attribute.
            """
            self.logger.info("check for transition trigger {}".format(self.action))
            self._succeeded_hook = self.action
            return super().check_allowed()

        def do(self, argin):
            self.action = argin
            return (ResultCode.OK, f"Executed {argin}")

        def succeeded(self):
            """
            Action to take on successful completion of device server
            re-initialization.
            """
            self.logger.info("Execute succeeded with arg {}".format(self.action))
            if not self.action:
                self.logger.info("Action not specified!!")
            self.state_model.perform_action(self.action)

        def failed(self):
            self.state_model.perform_action("fatal_error")

    class InitCommand(SKASubarray.InitCommand):
        """
        A class for the SKASubarray's init_device() "command".
        """
        def do(self):
            """
            Stateless hook for device initialisation.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            (result_code, message) = super().do()

            device = self.target
            self.logger.info("CspSubarray INIT COMMAND STARTED!!")
            self.logger.info("CspSubarray obs_state: {}".format(device._obs_state))
            device._build_state = '{}, {}, {}'.format(release.name, release.version, release.description)
            device._version_id = release.version
            # connect to CSP.LMC TANGO DB
            # _config_delay_expected: inherited from the SKAObsDevice base class
            # Note: the _config_delay_expected could be set equal the max among the sub-elements
            # sub-arrays expected times for the command
            # For tests purpose we set it  = 10
            device._config_delay_expected = 10
            device._scan_id = 0
            device._configuration_id = ''
            # flag to signal if we have a recofniguraiton
            device._reconfiguring = False

            # connect to TANGO DB
            
            # use defaultdict to initialize the sub-element State,healthState
            # and adminMode. The dictionary uses as keys the sub-element
            # fqdn, for example
            # device._sc_subarray_state[device.CspCbf]
            # return the State value of the Mid Cbf sub-element.
            device._sc_subarray_state         = defaultdict(lambda: tango.DevState.DISABLE)
            device._sc_subarray_health_state  = defaultdict(lambda: HealthState.UNKNOWN)
            device._sc_subarray_admin_mode    = defaultdict(lambda: AdminMode.NOT_FITTED)
            device._sc_subarray_obs_state     = defaultdict(lambda: ObsState.IDLE)
            device._sc_subarray_obs_mode      = defaultdict(lambda: ObsMode.IDLE)
            device._mutex_obs_state = threading.Lock()
            #device._csp_tango_db = tango.Database()
            # read the CSP memorized attributes from the TANGO DB. 
            # Note: a memorized attribute has defined the attribute
            # property '__value'
            #attribute_properties = device._csp_tango_db.get_device_attribute_property(device.get_name(),
            #                                                                  {'adminMode': ['__value']})
            # build a dictionary with the (attr_name, value) of the memorized attributes
            # use memorized atrtibute if present, otherwise the default one
            #memorized_attr_dict = {attr_name : int(value[0]) for attr_name, db_key in attribute_properties.items() 
            #                                                 for key, value in db_key.items() 
            #                                                 if key == '__value'}
            #try:
            #    device._admin_mode = memorized_attr_dict['adminMode']
            #    if device._admin_mode not in [AdminMode.ONLINE, AdminMode.MAINTENANCE]:
            #        device._health_state = HealthState.UNKNOWN
            #        device.set_state(tango.DevState.DISABLE)
                    
            #except KeyError as key_err:
            #    device.logger.info("Key {} not found".format(key_err))

            # list of sub-array sub-component FQDNs
            device._sc_subarray_fqdn = []
            
            # list of sub-component FQDNs assigned to the sub-array
            device._sc_subarray_assigned_fqdn = []
            
            # _sc_subarray_proxies: the sub-element sub-array proxies
            # implementes as dictionary:
            # keys: sub-element sub-arrayFQDN
            # values: device proxy
            device._sc_subarray_proxies = {}
            
            # Nested default dictionary with list of event ids/CSP sub-array sub-component. Need to
            # store the event ids for each CSP sub-array component and attribute to un-subscribe
            # them at disconnection.
            # keys: sub-component FQDN
            # values: dictionary (keys: attribute name, values: event id)
            device._sc_subarray_event_id = defaultdict(lambda: defaultdict(lambda: 0))
            
            # _sc_subarray_cmd_exec_state: implement the execution state of a long-running
            # command for each sub-array sub-component.
            # implemented as a nested default dictionary:
            # keys: sub-element FQDN
            # values: defaut dictionary (keys: command name, values: command state)
            device._sc_subarray_cmd_exec_state = defaultdict(lambda: defaultdict(lambda: CmdExecState.IDLE))
            
            # _sc_subarray_cmd_starting_time: for each sub-element report the long-running command 
            # starting time
            # Implemented as dictionary:
            # keys: the sub-element sub-array FQDN
            # values: starting time
            device._sc_subarray_cmd_starting_time = defaultdict(lambda: 0.0)
            
            # _command_thread: thread for the command execution
            # keys: the command name('on, 'off'...)
            # values: thread instance
            device._command_thread = {}
            device._stop_thread = defaultdict(lambda: False)
            
            # _end_scan_event: thread event to signal EndScan
            device._end_scan_event = threading.Event()
            
            # _abort_obs_event: thread event to signal Abort request
            device._abort_obs_event = threading.Event()
            
            # of a long-running command
            # implemented as a default nested dictionary:
            # keys: sub-element sub-array FQDN
            # values: default dictionary (keys: command name, values: the execution percentage)
            device._sc_subarray_cmd_progress = defaultdict(lambda: defaultdict(lambda: 0))
            
            # _sc_subarray_cmd_duration_expected: for each sub-element, store the duration (in sec.) 
            # configured for a long-running command 
            # Implemented as a nested default dictionary
            # keys: FQDN
            # values: default dictionary (keys: command name, values: the duration (in sec))
            device._sc_subarray_cmd_duration_expected = defaultdict(lambda: defaultdict(lambda: 10))
            
            # _sc_subarray_scan_configuration: report the scan configuration
            # for each sub-array sub-component (CBF, PSS subarray, PSTBeams) 
            # Implemented as default dictionary
            # keys: FQDN
            # values: the scan confiration as JSON formatted string
            
            device._sc_subarray_scan_configuration = defaultdict(lambda:'')
            
            # _cmd_execution_state: implement the execution state of a long-running
            # command for the whole CSP sub-array  Setting this attribute prevent the execution
            # of the same command while it is already running.
            # implemented as a default dictionary:
            # keys: command name
            # values:command state
            device._cmd_execution_state = defaultdict(lambda: CmdExecState.IDLE)
            
            # _cmd_progress: report the execution progress of a long-running command
            # implemented as a dictionary:
            # keys: command name ('addbeams', 'removebeams', 'configure')
            # values: the percentage
            device._cmd_progress = defaultdict(lambda: 0)
            
            # _cmd_duration_expected: store the duration (in sec.) configured for
            # a long-running command 
            # Implemented asdefault dictionary
            # keys: command name ('addsearchbeams','configurescan'....)
            # values: the duration (in sec)
            device._cmd_duration_expected = defaultdict(lambda: 5)
            
            # _cmd_duration_measured: report the measured duration (in sec.) for
            # a long-running command 
            # Implemented as default dictionary
            # keys: command name ('on', 'off',..)
            # values: the duration (in sec)
            device._cmd_duration_measured = defaultdict(lambda: 0)
            
            # _timeout_expired: report the timeout flag 
            device._timeout_expired = False
            
            # _failure_raised: report the failure flag 
            device._failure_raised = False
            
            # _failure_message: report the failure message
            # Implemented as a dictionary
            # keys: command name ('addserachbeams', 'configurescan'..)
            # values: the message
            device._failure_message = defaultdict(lambda: '')
            
            # _list_dev_completed_task: for each long-running command report the list of subordinate
            # components that completed the task
            # Implemented as a dictionary
            # keys: the command name ('on', 'off',...)
            # values: the list of components
            device._list_dev_completed_task = defaultdict(lambda: [])
            # the last executed command 
            device._last_executed_command = ""
             
            # _num_dev_completed_task: for each long-running command report the number
            #  of subordinate components that completed the task
            # Implemented as a dictionary
            # keys: the command name ('addbeams, 'configurescan',...)
            # values: the number of components
            device._num_dev_completed_task = defaultdict(lambda:0)
            
            # _valid_scan_configuration: the last programmed scan configuration
            device._valid_scan_configuration = ''
            
            # unassigned CSP SearchBeam, TimingBeam, VlbiBeam Capability IDs
            device._assigned_search_beams = []
            #device._unassigned_search_beam_num =  0
            device._reserved_search_beams =  []
            #device._reserved_search_beam_num =  0
            device._assigned_timing_beams= []
            device._assigned_vlbi_beams = []      
            device._command_thread['init'] = threading.Thread(target=self.initialize_thread,
                                                              name="Thread-Initialization ObsState Alignment",
                                                              args=())
            device._command_thread['init'].start()
            # to use the push model in command_inout_asynch (the one with the callback parameter),
            # change the global TANGO model to PUSH_CALLBACK.
            apiutil = tango.ApiUtil.instance()
            apiutil.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)
            return (ResultCode.STARTED, "CSP Subarray Init STARTED")

        def initialize_thread(self):
            try:
                with EnsureOmniThread():
                    self.logger.info("Init thread started")
                    device = self.target
                    args = (device, device.state_model, self.logger)
                    device.force_cmd_obj = device.ForceObsStateTransitionCommand(*args)
                    on_handler = device.OnCommand(*args)

                    # Try connection with the CBF sub-array
                    device.connect_to_subarray_subcomponent(device.CbfSubarray)
                    # TODO: add connection to CSPMaster to get information
                    #       on subarrayMembership for PSS, PST and VLBI beams.
                    # Try connection with the PSS sub-array
                    device.connect_to_subarray_subcomponent(device.PssSubarray)
                    # put the device to OFF/EMPTY: no transition is allowed from INIT state
                    self.succeeded()
                    subarray_state = device._sc_subarray_state[device.CbfSubarray]
                    if subarray_state is not DevState.ON:
                        return
                    # put the device to ON/EMPTY
                    on_handler.succeeded()

                    # CASE B: CSP is ON
                    target_obs_state = device.obs_state_evaluator()
                    if target_obs_state in ['RESOURCING', 'ABORTING', 'CONFIGURING', 'RESETTING','RESTARTING']:
                        self.logger.info("Still to implement transitional state different from SCANNINNG")
                        return
                        #self.monitor_running_command(target_obs_state)
                    self.logger.info('CSP is already ON. Aligning to Sub-elements...')
                    device.set_csp_obs_state(target_obs_state)
                    if target_obs_state == 'SCANNING':
                        return self.monitor_running_command(target_obs_state)
            except Exception as msg:
                self.logger.info(f'error in thread: {msg}')

        def monitor_running_command(self, csp_obs_state):
            """
            Helper method to monitor the CSP Subarray observing state at re-initialization if
            the observing state is in a transitional state.
            NOTE: Currently only the SCANNING obsState is handled.

            :param csp_obs_state: the CSP.LMC Subarray observing state.
            :type csp_obs_state: string
            """
            device = self.target
            if csp_obs_state == 'SCANNING':
                args = (device, device.state_model, self.logger)
                handler = device.ScanCommand(*args)
                device._command_thread['scan'] = threading.Thread(target=handler.monitor_scan_execution,
                                                   name="Thread-Scan",
                                                   args=(device._sc_subarray_fqdn,))
                device._cmd_execution_state['scan'] = CmdExecState.RUNNING
                self.logger.info("Start scan thread")
                device._command_thread['scan'].start()

    def obs_state_evaluator(self):
        """
        Helper method to evaluate the CSP Subarray observing state starting from the 
        SCM values of its components.
        Criteria: 
        - If any one of the componts is in a transitional state, the CSP subarray
        assumes that value.
        - If components are in a steady observing state, the values must be equal
        otherwise the CSP Subarray is set in FAULT.
        - A component not ONLINE/MAINTENANCE does not contribute to the observing 
        state value.
        
        :return: The observing state.
        :rtype: string with the observing state name.
        """
        target_obs_state = 'FAULT'
        obs_states_list = []
        allowed_coupled = {'RESOURCING': 'IDLE',
                  'RESOURCING': 'EMPTY',
                  'CONFIGURING': 'READY',
                  'SCANNING': 'READY',
                  'ABORTING': 'ABORTED',
                  'RESETTING': 'IDLE',
                  'RESTARTING': 'EMPTY'
                   }
        transitional_states = allowed_coupled.keys()
        for fqdn in self._sc_subarray_fqdn:
            self.logger.info("fqdn {} admin mode: {}".format(fqdn, self._sc_subarray_admin_mode[fqdn]))
            if self._sc_subarray_admin_mode[fqdn] not in [AdminMode.ONLINE,
                                                      AdminMode.MAINTENANCE
                                                     ]:
                continue
            obs_state_name =  ObsState(self._sc_subarray_obs_state[fqdn]).name
            obs_states_list.append(obs_state_name)
        self.logger.info("obs_state_evaluator: {}".format(obs_states_list))
        # first creates an intersection list
        transitional_present = set(transitional_states) & set(obs_states_list)
        self.logger.info("transitional_present: {}".format(transitional_present))
        # CASE 1: Transitional states NOT present
        if not transitional_present:
            # CASE 1.1: All obsStates are EQUAL
            if len(set(obs_states_list)) == 1:
                target_obs_state = obs_states_list[0]
        else:
            state =list(transitional_present)[0]
            for sub_elt_obs_state in obs_states_list:
                if sub_elt_obs_state is not (state or allowed_coupled[state]):
                    break
                # CASE 2.2: Other obs_states are ALLOWED
                else:
                    target_obs_state = state
        self.logger.info("Evaluated CSP Subarray obsState: {}".format(target_obs_state))
        # CASE 1: Transitional states NOT present
        return target_obs_state

    def set_csp_obs_state(self, state):
        """
        Set the Subarray observing state to the specified value.

        :param state: The target observing state value
        :type state: string
        :return: None
        """
        try:
            if state == 'FAULT':
                self.force_cmd_obj.failed()
            self.force_cmd_obj(f"force_to_{state.lower()}")
        except StateModelError as state_error:
            self.logger.warning(state_error)

    class OnCommand(SKASubarray.OnCommand):
        def do(self):
            super().do()
            device = self.target
            device_in_error = 0
            self.logger.info("Call On Command")
            for fqdn in device._sc_subarray_fqdn:
                try:
                    # check if the sub-element subarray is already in the
                    # requested state
                    if device._sc_subarray_state[fqdn] == tango.DevState.ON:
                        continue
                    (result_code, message) = device._sc_subarray_proxies[fqdn].On()
                    if result_code == ResultCode.FAILED:
                        self.logger.error("On command failed on device {}".format(fqdn))
                        if fqdn != device.CbfSubarray:
                            device._health_state = HealthState.DEGRADED
                            continue
                        else:
                            device_in_error += 1
                except tango.DevFailed as tango_err:
                    message = str(tango_err.args[0].desc)
                    device_in_error += 1
            if device_in_error:
                return (ResultCode.FAILED, "Command On failed")
            message = "On command completed OK"
            self.logger.info(message)
            return (ResultCode.OK, message)

    class OffCommand(SKASubarray.OffCommand):
        def do(self):
            self.logger.info("Call Off Command")
            device = self.target
            subelement_thr = {}
            device_to_turn_off = []
            self.empty_succeeded = {}
            off_failed = False
            # First loop that start the sub-element threads to get the ObsState.EMPTY
            for fqdn in device._sc_subarray_fqdn:
                # check if the sub-element subarray is already in the
                # requested state
                if device._sc_subarray_state[fqdn] == tango.DevState.OFF:
                    continue
                elif device._sc_subarray_obs_state[fqdn] != ObsState.EMPTY:
                    subelement_thr[fqdn] = threading.Thread(target=self.off_sequence,
                                                           name="Thread-OffSequence",
                                                           args=(fqdn,))
                    subelement_thr[fqdn].start()
                    device_to_turn_off.append(fqdn)
                else:
                    self.logger.info(f'ObsState of device {fqdn} is already EMPTY. ObsState:{device._sc_subarray_obs_state[fqdn]}')
                    device_to_turn_off.append(fqdn)
                    self.empty_succeeded[fqdn] = True

            # Second loop check if the thread is succeeded and send the Off command to sub-element
            for fqdn in subelement_thr.keys():
                subelement_thr[fqdn].join()

            if all(self.empty_succeeded[fqdn] for fqdn in device_to_turn_off) and device_to_turn_off:
                
                result_code = ResultCode.FAILED

                for fqdn in device_to_turn_off:
                    try:
                        self.logger.info('Sending Off Command...')
                        (result_code, message) = device._sc_subarray_proxies[fqdn].Off()
                    except tango.DevFailed as tango_err:
                        message = str(tango_err.args[0].desc)

                if result_code == ResultCode.FAILED:
                    self.logger.error("Off command failed on device {}".format(fqdn))
                    self.logger.info(message)
                    off_failed = True          

            if off_failed:
                return ResultCode.FAILED, 'Off command Failed'
            
            message = "Off command completed OK"
            self.logger.info(message)
            return ResultCode.OK, message

        def off_sequence(self, fqdn):
            #Off command send the device in the state:OFF, ObsState:EMPTY
            self.logger.info('off sequence started!')
            self.logger.info(f'FQDN: {fqdn}')
            device = self.target
            starting_time = time.time()
            timeout = device._sc_subarray_cmd_duration_expected[fqdn]['off']
            while (time.time() - starting_time) < timeout and (device._sc_subarray_obs_state[fqdn] != ObsState.EMPTY):
                # Try to send Abort Command.
                # Device goes to ABORTED in case ObsState is: IDLE, READY, CONFIGURING, SCANNING, RESETTING
                try:
                    self.logger.info('Try to execute Abort command')
                    self.logger.info(f'ObState: {device._sc_subarray_obs_state[fqdn]}')
                    device._sc_subarray_proxies[fqdn].Abort()
                    while (time.time() - starting_time) < timeout and (device._sc_subarray_obs_state[fqdn] != ObsState.ABORTED):
                        if device._sc_subarray_obs_state[fqdn] == ObsState.FAULT:
                            break
                        time.sleep(0.1)
                except Exception as msg:
                    self.logger.info(msg)
                # Try to send Restart Command.
                # Device goes to EMPTY if ObsState is: FAULT, ABORTED
                # ObsState could be aborted from above try section
                try:
                    self.logger.info('Try to execute Restart command')
                    device._sc_subarray_proxies[fqdn].Restart()
                except Exception as msg:
                    self.logger.info(msg)
                time.sleep(0.1)
                # if ObsState is: RESOURCING, ABORTING start again to wait end of the transition

            if device._sc_subarray_obs_state[fqdn] == ObsState.EMPTY:
                self.empty_succeeded[fqdn] = True
            else:
                self.empty_succeeded[fqdn] = False

    class AssignResourcesCommand(SKASubarray.AssignResourcesCommand):
        @transaction_id
        def do(self, argin):
            #TODO: Edit the logger
            self.logger.info('Assign resource command ... ')
            return (ResultCode.OK, "Assign Resources command completed OK")

    class ConfigureCommand(SKASubarray.ConfigureCommand):
        @transaction_id
        def do(self, argin):
            # checks on State, adminMode and obsState values are performed inside the 
            # python decorators
           
            # the dictionary with the scan configuration

            self.logger.info(f"ConfigureCommand at {time.time()}")
            target_device = self.target
            try:
                # if the stored configuration attribute is not empty, check
                # for the received configuration content
                if target_device._valid_scan_configuration:
                    try:
                        stored_configuration = json.dumps(target_device._valid_scan_configuration, sort_keys=True)
                        received_configuration = json.dumps(argin, sort_keys=True)
                        # if the received configuration is equal to the last valid configuration and the
                        # state of the subarray is READY than subarray is not re-configured.
                        if (stored_configuration == received_configuration) and (target_device.state_model._obs_state == ObsState.READY):
                            self.logger.info("Subarray is going to use the same configuration")
                            return (ResultCode.OK, "Configure command OK")
                    except Exception as e:
                        self.logger.warning(str(e))
                # go ahead and parse the received configuration
                self.validate_scan_configuration(argin)
            except tango.DevFailed as tango_err:
                # validation failure
                msg = "Failure during validation of configuration:{}".format(tango_err.args[0].desc)
                self.logger.error(msg)
                tango.Except.throw_exception("Command failed",
                                             msg,
                                             "Configure",
                                             tango.ErrSeverity.ERR)
            target_device._reconfiguring = False
            # Forward the Configure command to the sub-elements
            # components (subarrays, pst beams)
            for device in target_device._sc_subarray_assigned_fqdn:
                # reset the command progress counter for each
                # sub-array component
                self.logger.info(f"ConfigureCommand exec_state {target_device._sc_subarray_cmd_exec_state[device]['configurescan']}")
                target_device._sc_subarray_cmd_exec_state[device]['configurescan'] = CmdExecState.IDLE
                if target_device._sc_subarray_obs_state[device] == ObsState.READY:
                    target_device._reconfiguring = True
                target_device._sc_subarray_cmd_progress[device]['configurescan'] = 0
                target_device._failure_message['configurescan'] = ''
                # NOTE: what happens if a sub-element subarray/PSt beam TANGO
                # device is not running? The next calls to proxy should fail
                # with a tango.DevFailed exception.
                if not target_device._is_sc_subarray_running(device):
                    msg = "Device {} not running".format(device)
                    if device == target_device.CbfSubarray:
                        return (ResultCode.FAILED, msg)
                    target_device._failure_message['configurescan'] += msg
                    target_device._sc_subarray_cmd_exec_state[device]['configurescan'] = CmdExecState.FAILED
                    #skip to next device
                    continue
                proxy = target_device._sc_subarray_proxies[device]
                # subscribe sub-elements attributes to track command execution and
                # timeout on subarray sub-components
                attributes = ['configureScanCmdProgress', 'timeoutExpiredFlag']
                for attr in attributes:
                    try:
                        if target_device._sc_subarray_event_id[attr.lower()] == 0:
                            event_id = proxy.subscribe_event(attr,
                                                             tango.EventType.CHANGE_EVENT,
                                                             target_device._attributes_change_evt_cb,
                                                             stateless=False)
                            target_device._sc_subarray_event_id[device][attr] = event_id
                    except tango.DevFailed as df:
                        self.logger.info(df.args[0].desc)              
                # NOTE: CBF/PSS sub-array checks for the validity of its
                # configuration. Failure in configuration throws an exception that is
                # caught via the _cmd_ended_cb callback
                try:
                    # read the timeout configured for the operation on the device
                    target_device._sc_subarray_cmd_duration_expected[device]['configurescan'] = target_device._get_expected_delay(proxy, "configureDelayExpected")
                except tango.DevFailed as tango_err:
                    self.logger.info("No attribute {} on device {}. Use default value {}".format(tango_err.args[0].desc, device,
                                                      target_device._sc_subarray_cmd_duration_expected[device]['configurescan']))
                if target_device._sc_subarray_cmd_duration_expected[device]['configurescan'] >  target_device._config_delay_expected:
                    target_device._config_delay_expected = target_device._sc_subarray_cmd_duration_expected[device]['configurescan'] 
                try:
                    target_device._timeout_expired = False
                    target_device._failure_raised = False
                    proxy.command_inout_asynch("ConfigureScan",
                                               target_device._sc_subarray_scan_configuration[device],
                                               target_device._cmd_ended_cb)
                    self.logger.info("Exec state command flag is {}".format(target_device._sc_subarray_cmd_exec_state[device]['configurescan']))
                    # Note: check if callaback executed...
                    if target_device._sc_subarray_cmd_exec_state[device]['configurescan'] != CmdExecState.FAILED:
                        target_device._sc_subarray_cmd_exec_state[device]['configurescan'] = CmdExecState.RUNNING
                except tango.DevFailed as tango_err:
                    if device == target_device.CbfSubarray:
                        msg = "Failure in configuring CBF: {}".format(tango_err.args[0].desc)
                        self.logger.error(msg)
                        # throw the exception, configuration can't proceed
                        return (ResultCode.FAILED, msg)

                    target_device._failure_message['configurescan'] += tango_err.args[0].desc
                    target_device._sc_subarray_cmd_exec_state[device]['configurescan'] = CmdExecState.FAILED

                # register the starting time for the command
                target_device._sc_subarray_cmd_starting_time[device] = time.time()
                self.logger.debug("configure starting time: {}".format(target_device._sc_subarray_cmd_starting_time[device]))
            # end for loop on devices

            self.logger.debug("_config_delay_expected :{}".format(target_device._config_delay_expected))
            # invoke the constructor for the command thread
            thread_args = [target_device._sc_subarray_assigned_fqdn, argin]
            target_device._command_thread['configurescan'] = threading.Thread(target=self.configure_scan,
                                                               name="Thread-Configure",
                                                               args=(thread_args,))
            target_device._abort_obs_event.clear()
            target_device._cmd_execution_state['configurescan'] = CmdExecState.RUNNING
            target_device._cmd_duration_measured['configurescan'] = 0
            target_device._command_thread['configurescan'].start()
            message = "Configure command started"
            self.logger.info(message)
            return (ResultCode.STARTED, message)

        def configure_scan(self, input_arg, **args_dict):
            """
            Thread target function invoked from Configure method.
            It monitors the obsState value of each sub-array sub-component
            configuring the scan, looking for timeout or failure conditions.
            
            :param: input_arg: a list with two elements: 
                - the FQDNs of the sub-array sub-components
                - the configuration string 
            
            If the CSP.LMC Subarray device is properly configured, the received
            string passed as input argument of the command is stored the 
            class atribute _valid_scan_configuration.
            
            :return: None
            """
            target_device = self.target
            cmd_name = 'configurescan'
            dev_successful_state = ObsState.READY
            # tango_cmd_name: is the TANGO command name with the capital letter
            # In the dictionary keys, is generally used the command name in lower letters
            command_start_time = time.time()
            target_device._num_dev_completed_task[cmd_name] = 0
            target_device._list_dev_completed_task[cmd_name] = []
            target_device._cmd_progress[cmd_name] = 0
            target_device._cmd_duration_measured[cmd_name] = 0
            # flag to signal when configuration ends on a sub-array sub-component
            device_done = defaultdict(lambda:False)
            # inside the end-less lop check the obsState of each sub-component
            device_list = input_arg[0]
            self.logger.info("Trhead started at {}".format(time.time()))
            while True:
                command_progress = 0
                for device in device_list:
                    self.logger.info("Current device {} obsState is {}".format(device,
                                                                     ObsState(target_device._sc_subarray_obs_state[device]).name))
                    if device_done[device] == True:
                        continue
                    # if the sub-component execution flag is no more RUNNING, the command has
                    # ended with or without success. Go to check next device state.
                    self.logger.info("Wait for final obsState = {}".format(ObsState(dev_successful_state).name))
                    self.logger.info("current device {} state:{} reconfiguring: {}".format(device, 
                                              ObsState(target_device._sc_subarray_obs_state[device]).name,
                                              target_device._reconfiguring))
                    if target_device._sc_subarray_obs_state[device] == dev_successful_state:
                        if not target_device._reconfiguring:
                            self.logger.info("Command {} ended with success on device {} at {}.".format(cmd_name,
                                                                                                      device,
                                                                                                      time.time()))
                            # update the list and number of device that completed the task
                            target_device._num_dev_completed_task[cmd_name]  += 1
                            target_device._list_dev_completed_task[cmd_name].append(device)
                            # reset the value of the attribute reporting the execution state of
                            # the command
                            target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                            target_device._sc_subarray_cmd_progress[device][cmd_name] = 100
                            # command success: step to next device
                            device_done[device] = True

                    # check for timeout event. A timeout event can be detected in two ways:
                    # 1- the sub-element implements the 'timeoutExpiredFlag' attribute configured
                    #    for change event
                    # 2- the CspSubarray periodically checks the time elapsed from the start
                    #    of the command: if the value is greater than the sub-element expected time
                    #    for command execution, the sub-element command execution state is set
                    #    to TIMEOUT
                    # Note: the second check, can be useful if the timeout event is not received
                    # (for example for a temporary connection timeout)
                    elapsed_time = time.time() - target_device._sc_subarray_cmd_starting_time[device]
                    if (elapsed_time > target_device._sc_subarray_cmd_duration_expected[device][cmd_name] or
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.TIMEOUT):
                        msg = ("Timeout executing {} command  on device {}".format(cmd_name, device))
                        self.logger.warning(msg)
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.TIMEOUT
                        device_done[device] = True 
                    # check if sub-element command ended throwing an exception: in this case the
                    # 'cmd_ended_cb' callback is invoked.
                    if target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED or \
                       target_device._sc_subarray_obs_state[device] == ObsState.FAULT:
                       target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.FAILED
                       # execution ended for this sub-element, skip to the next one
                       device_done[device] = True
                    # update the progress counter inside the loop taking into account the number of devices
                    # executing the command
                    command_progress += target_device._sc_subarray_cmd_progress[device][cmd_name]/len(device_list)
                    self.logger.debug("Command {} on device {} exec_state {}:".format(cmd_name,device,
                                                                       target_device._sc_subarray_cmd_exec_state[device][cmd_name]))
                target_device._cmd_progress[cmd_name] = command_progress
                if any(target_device._sc_subarray_obs_state[device] == ObsState.CONFIGURING for device in device_list):
                    #target_device._reconfiguring = False
                    self.logger.info("device {} is in {}: reconfiguring is:{}".format(device, 
                                                                                      ObsState(target_device._sc_subarray_obs_state[device]).name,
                                                                                      target_device._reconfiguring))
                # check if the abort flag is set
                if target_device._abort_obs_event.is_set():
                    self.logger.info("Received an ABORT request during configuration {}".format(time.time()))
                    return
                if all(value == True for value in device_done.values()):
                    self.logger.info("All devices have been handled at time {}!".format(time.time()))
                    break
                # check for global timeout expiration
                # may be this check is not necessary 
                #if target_device._cmd_duration_expected[cmd_name] < (time.time() - command_start_time):
                #    target_device._timeout_expired = True
                #    self.logger.warning("Device went in timeout during configuration")
                #    break
                time.sleep(0.1)
            # end of the while loop
            # acquire the mutex during the check of configuration success/failure. We don't want
            # to receive an abort during this phase otherwise could happen strange situation
            self.logger.info("Going to lock TangoMonitor at {}".format(time.time()))
            with tango.AutoTangoMonitor(self.target):
                # check for timeout/failure conditions on each sub-component
                if any(target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.TIMEOUT for device in device_list):
                    target_device._timeout_expired = True
                if any(target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED for device in device_list):
                    target_device._failure_raised = True
                # reset the sub-element command execution flag
                self.logger.info(target_device._sc_subarray_cmd_exec_state.keys())
                for device in  target_device._sc_subarray_cmd_exec_state.keys():
                    for cmd_name in  target_device._sc_subarray_cmd_exec_state[device].keys():
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                self.logger.info("CspSubarray failure flag:{}".format(target_device._failure_raised))
                self.logger.info("CspSubarray timeout flag:{}".format(target_device._timeout_expired))
                if target_device._abort_obs_event.is_set():
                    return
                try:
                    if all(target_device._sc_subarray_obs_state[fqdn] == ObsState.READY for fqdn in device_list):
                        target_device._valid_scan_configuration = input_arg[1]
                        target_device._cmd_duration_measured[cmd_name] = time.time() - command_start_time
                        target_device._cmd_progress[cmd_name] = 100
                        target_device._last_executed_command = cmd_name
                        self.logger.info("1 Configure ends with success!! {}".format(time.time()))
                        return self.succeeded()
                except Exception as e:
                    self.logger.error(e)
                self.logger.info("Configure ends with failure!! {}".format(time.time()))
                return self.failed()

        def validate_scan_configuration(self, argin):
            """
            This method is overwritten in each CspSubarray instance.
            
            :param argin: the JSON string with scan configuration
            :return: None
            :raises: tango.DevFailed exception
            """

            target_device = self.target
            try:
                json_config = JsonConfiguration(argin, self.logger)
                target_device._configuration_id = json_config.get_id()
                self.logger.info("Processing configuration {}".format(target_device._configuration_id))

            except Exception as e:  # argument not a valid JSON object
                # this is a fatal error
                if isinstance(e, json.JSONDecodeError):
                    msg = ("Scan configuration object is not a valid JSON object."
                           "Aborting configuration:{}".format(str(e)))
                elif isinstance(e, ValueError):
                    msg = "Command failed: No configuration ID in JSON object"
                else:
                    msg = 'Unknown Error'
                self.logger.error(msg)
                tango.Except.throw_exception("Command failed",
                                             msg,
                                             "ConfigureScan execution",
                                             tango.ErrSeverity.ERR)

    class ReleaseResourcesCommand(SKASubarray.ReleaseResourcesCommand):
        
        @transaction_id
        def do(self,argin):
            #TODO: Log message
            self.logger.info('Release Resource command...')
            return (ResultCode.OK, "Assign Resources command completed OK")


    class ScanCommand(SKASubarray.ScanCommand):
        def do(self, argin):
            target_device = self.target
            try:
                target_device._scan_id = int(argin)
            except (ValueError, Exception) as err:
                msg = "Scan command invalid argument:{}".format(str(err))
                self.logger.error(msg)
                return (ResultCode.FAILED, msg) 
            # invoke the constructor for the command thread
            self.logger.info("Received Scan command with id:{}".format(target_device._scan_id))
            for device in target_device._sc_subarray_assigned_fqdn:
                try:
                    proxy = target_device._sc_subarray_proxies[device]
                    if not target_device._sc_subarray_event_id[device]['scancmdprogress']:
                        evt_id = proxy.subscribe_event("scanCmdProgress",
                                                       tango.EventType.CHANGE_EVENT,
                                                       target_device._attributes_change_evt_cb,
                                                       stateless=False)
                        target_device._sc_subarray_event_id[device]['scancmdprogress'] = evt_id
                except KeyError as key_err:
                    self.logger.warning("No key {} found".format(key_err)) 
                except tango.DevFailed as tango_err:
                    self.logger.info(tango_err.args[0].desc)
                try:
                    target_device._timeout_expired = False
                    target_device._failure_raised = False
                    proxy.command_inout_asynch("Scan", target_device._scan_id, target_device._cmd_ended_cb)
                except tango.DevFailed as tango_err:
                    self.logger.info(tango_err.args[0].desc)
                    # TODO: add check on the failed device. If CBF
                    # throw an exception.
            # invoke the constructor for the command thread
            target_device._command_thread['scan'] = threading.Thread(target=self.monitor_scan_execution,
                                                               name="Thread-Scan",
                                                               args=(target_device._sc_subarray_assigned_fqdn,))
            self.logger.info("Thread scan: {}".format(target_device._command_thread['scan']))
            target_device._cmd_execution_state['scan'] = CmdExecState.RUNNING
            target_device._command_thread['scan'].start()
            return (ResultCode.STARTED, "Scan command started") 

        def monitor_scan_execution(self, device_list):
            self.logger.info("Starting scan thread")
            cmd_name = 'scan'
            target_device = self.target
            target_device._end_scan_event.clear()
            target_device._abort_obs_event.clear()
            dev_successful_state = ObsState.READY
            target_device._num_dev_completed_task[cmd_name] = 0
            target_device._list_dev_completed_task[cmd_name] = []
            target_device._cmd_progress[cmd_name] = 0
            for device in device_list:
                target_device._failure_message[cmd_name] = ''
                target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.RUNNING
                self.logger.info("Device {} State {} expected value {}".format(device,
                                                                               target_device._sc_subarray_obs_state[device], 
                                                                               dev_successful_state))
            # flag to signal when configuration ends on a sub-array sub-component
            device_done = defaultdict(lambda:False)
            elapsed_time = 0
            starting_time = time.time()
            stop_scan = False
            # inside the end-less loop check the obsState of each sub-component
            while True:
                if target_device._stop_thread[cmd_name]:
                    target_device._stop_thread[cmd_name] = False
                    self.logger.info("STOPPING THE THREAD!!!")
                    return
                if target_device._end_scan_event.is_set() or target_device._abort_obs_event.is_set(): 
                    if not stop_scan:
                        stop_scan = True
                        starting_time = time.time()
                        cmd_name = 'abort'
                        if target_device._end_scan_event.is_set():
                            cmd_name = 'endscan'
                    elapsed_time = time.time() - starting_time
                    self.logger.info("elapsed_time:{}".format(elapsed_time))
                    for device in device_list:
                        if target_device._sc_subarray_obs_state[device] != ObsState.SCANNING:
                            if target_device._sc_subarray_obs_state[device] == ObsState.FAULT:
                                target_device._failure_raised = True
                            device_done[device] = True
                        if elapsed_time > target_device._sc_subarray_cmd_duration_expected[device][cmd_name]:
                             target_device._cmd_execution_state[cmd_name] = CmdExecState.IDLE
                             target_device.timeout_expired = True
                             device_done[device] = True
                    if any(device_done.values()) and all(value == True for value in device_done.values()):
                        break
                time.sleep(0.1)
                           
            target_device._cmd_execution_state[cmd_name] = CmdExecState.IDLE
            target_device._cmd_progress[cmd_name] = 100
            elapsed_time = time.time() - starting_time 
            self.logger.info("Scan elapsed time:{}".format(elapsed_time))
            if target_device._end_scan_event.is_set():
                return
            if target_device._abort_obs_event.is_set():
                if target_device._failure_raised or target_device._timeout_expired:
                    return target_device.abort_cmd_obj.failed()
                self.logger.info("Abort of scan command ends with success!")
                return target_device.abort_cmd_obj.succeeded()

    class EndScanCommand(SKASubarray.EndScanCommand):

        def do(self):
            target_device = self.target
            device_list = target_device._sc_subarray_assigned_fqdn
            if not any(target_device._sc_subarray_assigned_fqdn):
                # need to add a check also on PSTBeams belonging to subarray
                device_list = target_device._sc_subarray_fqdn
            try:
                sc_group = tango.Group("EndScanGroup")
                for device in device_list:
                    sc_group.add(device)
            except Exception:
                self.logger.error("TANGO Group command failed")
                return (ResultCode.FAILED, "EndScan Command FAILED")
            answers = sc_group.command_inout("EndScan")
            target_device._end_scan_event.set()
            for reply in answers: 
                if reply.has_failed(): 
                    for err in reply.get_err_stack(): 
                        self.logger.error("device {}: {}-{}".format(reply.dev_name(), err.desc, err.reason))
                else:
                    (result_code,msg) = reply.get_data()
                    self.logger.info("device {}: {}".format(reply.dev_name(), msg))
            if any(target_device._sc_subarray_obs_state[device]== ObsState.FAULT for device in  device_list):
                return (ResultCode.FAILED, "EndScan Command FAILED")
            return (ResultCode.OK, "EndScan command executed OK")


    class ObsResetCommand(SKASubarray.ObsResetCommand):
        def do(self):
            self.logger.info("Call ObsReset")
            target_device = self.target
            devices_to_reset = []
            device_list = target_device._sc_subarray_assigned_fqdn
            if not any(target_device._sc_subarray_assigned_fqdn):
                # need to add a check also on PSTBeams belonging to subarray
                device_list = target_device._sc_subarray_fqdn
            for device in device_list:
                if target_device._sc_subarray_obs_state[device] in [ObsState.FAULT, ObsState.ABORTED]:
                    devices_to_reset.append(device)
            self.logger.info("devices_to_reset:{}".format(devices_to_reset))
            if not any(devices_to_reset):
                result_code = ResultCode.OK
                msg = "ObsReset command OK"
                for device in device_list:
                    if target_device._sc_subarray_obs_state[device] == ObsState.IDLE:
                        continue
                    if target_device._sc_subarray_obs_state[device] == ObsState.READY:
                        (result_code, msg) = target_device.gotoidle_cmd_obj.do()
                return (result_code, msg)
            for device in devices_to_reset:
                try:
                    proxy = target_device._sc_subarray_proxies[device]
                    target_device._timeout_expired = False
                    target_device._failure_raised = False
                    proxy.command_inout_asynch("ObsReset", target_device._cmd_ended_cb)
                except KeyError as key_err:
                    self.logger.warning("No key {} found".format(key_err))
                except tango.DevFailed as tango_err:
                    self.logger.warning(tango_err.args[0].desc)
                    # TODO: address this case!
                    # signal the failure raising the failure flag?
            target_device._command_thread['obsreset'] = threading.Thread(target=self.monitor_obsreset,
                                                               name="Thread-ObsReset",
                                                               args=(devices_to_reset,))
            target_device._command_thread['obsreset'].start()
            return (ResultCode.STARTED, "ObsReset command executed STARTED")

        def monitor_obsreset(self, device_list):
            cmd_name = 'obsreset'
            target_device = self.target
            dev_successful_state = ObsState.IDLE
            target_device._cmd_progress[cmd_name] = 0
            device_done = defaultdict(lambda:False)
            # inside the end-less loop check the obsState of each sub-component
            while True:
                if target_device._stop_thread[cmd_name]:
                    target_device._stop_thread[cmd_name] = False
                    self.logger.info("STOPPING THE THREAD!!!")
                    return
                time.sleep(0.1)
                for device in device_list:
                    if device_done[device] == True:
                        continue
                    # if the sub-component execution flag is no more RUNNING, the command has
                    # ended with or without success. Go to check next device state.
                    if target_device._sc_subarray_obs_state[device] == dev_successful_state: 
                       self.logger.info("Command {} ended with success on device {}.".format(cmd_name,
                                                                                               device))
                       target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                       target_device._sc_subarray_cmd_progress[device][cmd_name] = 100
                       # command success: step to next device
                       device_done[device] = True
                        # check if sub-element command ended throwing an exception: in this case the
                        # 'cmd_ended_cb' callback is invoked.
                    if target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED or\
                            target_device._sc_subarray_obs_state[device] == ObsState.FAULT:
                        # execution ended for this sub-element, skip to the next one
                        target_device._failure_raised = True
                        device_done[device] = True
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                
                if any(device_done.values()) and all(value == True for value in device_done.values()):
                    self.logger.info("All devices have been handled!")
                    break
                           
            # end of the while loop
            # check for timeout/failure conditions on each sub-component
            if target_device._failure_raised or target_device._timeout_expired:
                return self.failed()

            if all(target_device._sc_subarray_obs_state[fqdn] == dev_successful_state for fqdn in device_list):
                target_device._cmd_progress[cmd_name] = 100
                target_device._last_executed_command = cmd_name
                self.logger.info("ObsReset ends with success")
                return self.succeeded()

    class AbortCommand(SKASubarray.AbortCommand):

        def do(self):
            device = self.target
            device_list = device._sc_subarray_assigned_fqdn
            if not any(device._sc_subarray_assigned_fqdn):
                # need to add a check also on PSTBeams belonging to subarray
                device_list = device._sc_subarray_fqdn
            self.logger.info("ABORT do at time {}".format(time.time()))
            try:
                abort_group = tango.Group("AbortGroup")
                for fqdn in device_list:
                    abort_group.add(fqdn)
                answer_id = abort_group.command_inout_asynch("Abort")
                device._abort_obs_event.set() 
                self.logger.info("abort is set? {}".format(device._abort_obs_event.is_set()))
                group_reply = abort_group.command_inout_reply(answer_id)
                '''
                if all ((reply.get_data())[0] == ResultCode.FAILED for reply in group_reply):
                    return (ResultCode.FAILED, "Abort FAILED")
                '''    
            except (TypeError, Exception) as ex:
                self.logger.info(str(ex))
                self.logger.error("TANGO Group command failed")
                return (ResultCode.FAILED, "Abort command failed")
            message = "Abort command completed STARTED"
            self.logger.info(message)
            #if all(device._command_thread[cmd_name].is_alive() == False for cmd_name in device._command_thread.keys()):
            device._command_thread['abort'] = threading.Thread(target=self.abort_monitoring,
                                                               name="Thread-Abort",
                                                               args=(device_list,))
            device._command_thread['abort'].start()
            return (ResultCode.STARTED, message)

        def abort_monitoring(self, device_list):
            cmd_name = 'abort'
            target_device = self.target
            device_done = defaultdict(lambda:False)
            elapsed_time = 0
            starting_time = time.time()
            while True:
                for device in device_list:
                    if device_done[device] == True:
                        continue
                    self.logger.info("device {} obs_state:{}".format(device, target_device._sc_subarray_obs_state[device]))
                    if target_device._sc_subarray_obs_state[device] == ObsState.ABORTED:
                       self.logger.info("Command {} ended with success on device {}.".format(cmd_name,
                                                                                               device))
                       target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                       target_device._sc_subarray_cmd_progress[device][cmd_name] = 100
                       # command success: step to next device
                       device_done[device] = True
                        # check if sub-element command ended throwing an exception: in this case the
                        # 'cmd_ended_cb' callback is invoked.
                    if target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED or\
                            target_device._sc_subarray_obs_state[device] == ObsState.FAULT:
                        # execution ended for this sub-element, skip to the next one
                        target_device._failure_raised = True
                        device_done[device] = True
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                    elapsed_time = time.time() - starting_time
                    if elapsed_time > target_device._sc_subarray_cmd_duration_expected[device][cmd_name]:
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.TIMEOUT
                        target_device._timeout_expired = True
                        device_done[device] = True
                
                if any(device_done.values()) and all(value == True for value in device_done.values()):
                    self.logger.info("All devices have been handled!")
                    break
                self.logger.info("Going to sleep")
                time.sleep(0.1)
                       
            # end of the while loop
            # check for timeout/failure conditions on each sub-component
            if target_device._failure_raised or target_device._timeout_expired:
                return self.failed()

            if all(target_device._sc_subarray_obs_state[fqdn] == ObsState.ABORTED for fqdn in device_list):
                target_device._cmd_progress[cmd_name] = 100
                target_device._last_executed_command = cmd_name
                self.logger.info("Abort ends with success")
                return self.succeeded()

    class RestartCommand(SKASubarray.RestartCommand):

        def do(self):
            self.logger.info("Call To Restart")
            device = self.target
            device_list = device._sc_subarray_assigned_fqdn
            if not any(device._sc_subarray_assigned_fqdn):
                # need to add a check also on PSTBeams belonging to subarray
                device_list = device._sc_subarray_fqdn
            # set all READY devices in IDLE
            for fqdn in device_list:
                proxy = device._sc_subarray_proxies[fqdn]
                if device._sc_subarray_obs_state[fqdn] == ObsState.EMPTY:
                    continue
                if device._sc_subarray_obs_state[fqdn] == ObsState.READY:
                    proxy.GoToIdle()
            for fqdn in device_list:
                proxy = device._sc_subarray_proxies[fqdn]
                self.logger.info("device {} obstate is {}".format(fqdn, ObsState(device._sc_subarray_obs_state[fqdn]).name))
                self.logger.info("lne(device) {}".format(len(device)))
                #if device._sc_subarray_obs_state[fqdn] == ObsState.IDLE or len(device):
                # Note: the RemoveAllREceptors can be invoked on the cbf subarray only if it is idle.
                # if it is in ABORTED or FAULT state, it's CBF subarray responsability to remove the
                # receptors before executing the Restart 
                if device._sc_subarray_obs_state[fqdn] == ObsState.IDLE:
                    if fqdn == device.CbfSubarray:
                        self.logger.info("Issue command RemoveAllReceptors")
                        proxy.command_inout_asynch("RemoveAllReceptors", device._cmd_ended_cb)
                if device._sc_subarray_obs_state[fqdn] in [ObsState.FAULT, ObsState.ABORTED]:
                    proxy.command_inout_asynch("Restart", device._cmd_ended_cb)
            device._command_thread['restart'] = threading.Thread(target=self.restart_monitoring,
                                                           name="Thread-Restart",
                                                           args=(device_list,))
            device._command_thread['restart'].start()
            return (ResultCode.STARTED, "Restart command STARTED")

        def restart_monitoring(self, device_list):
            cmd_name = 'restart'
            target_device = self.target
            device_done = defaultdict(lambda:False)
            elapsed_time = 0
            starting_time = time.time()
            while True:
                if target_device._stop_thread[cmd_name]:
                    target_device._stop_thread[cmd_name] = False
                    self.logger.info("STOPPING THE THREAD!!!")
                    return
                for device in device_list:
                    if device_done[device] == True:
                        continue
                    self.logger.info("device {} obs_state:{}".format(device, target_device._sc_subarray_obs_state[device]))
                    if target_device._sc_subarray_obs_state[device] == ObsState.EMPTY:
                       self.logger.info("Command {} ended with success on device {}.".format(cmd_name,
                                                                                               device))
                       target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                       target_device._sc_subarray_cmd_progress[device][cmd_name] = 100
                       # command success: step to next device
                       device_done[device] = True
                        # check if sub-element command ended throwing an exception: in this case the
                        # 'cmd_ended_cb' callback is invoked.
                    if target_device._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED or\
                            target_device._sc_subarray_obs_state[device] == ObsState.FAULT:
                        # execution ended for this sub-element, skip to the next one
                        target_device._failure_raised = True
                        device_done[device] = True
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
                    elapsed_time = time.time() - starting_time
                    if elapsed_time > target_device._sc_subarray_cmd_duration_expected[device][cmd_name]:
                        target_device._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.TIMEOUT
                        target_device._timeout_expired = True
                        device_done[device] = True
                
                if any(device_done.values()) and all(value == True for value in device_done.values()):
                    self.logger.info("All devices have been handled!")
                    break
                self.logger.info("Going to sleep")
                time.sleep(0.1)
                       
            # end of the while loop
            # check for timeout/failure conditions on each sub-component
            if target_device._failure_raised or target_device._timeout_expired:
                return self.failed()

            if all(target_device._sc_subarray_obs_state[fqdn] == ObsState.EMPTY for fqdn in device_list):
                target_device._cmd_progress[cmd_name] = 100
                target_device._last_executed_command = cmd_name
                self.logger.info("Restart ends with success")
                return self.succeeded()

    class GoToIdleCommand(SKASubarray.EndCommand):
        def do(self):
            target_device = self.target
            target_device._failure_raised = False
            device_list = target_device._sc_subarray_assigned_fqdn
            if not any(target_device._sc_subarray_assigned_fqdn):
                # need to add a check also on PSTBeams belonging to subarray
                device_list = target_device._sc_subarray_fqdn
            try:
                sc_group = tango.Group("GoToIdleGroup")
                for device in device_list:
                    sc_group.add(device)
            except Exception:
                self.logger.error("TANGO Group command failed")
                return (ResultCode.FAILED, "GoToIDle Command FAILED")
            self.logger.info("Issue GoToIdle")
            answers = sc_group.command_inout("GoToIdle")
            for reply in answers:
                if reply.has_failed():
                    target_device._failure_raised = True
                    for err in reply.get_err_stack():
                        self.logger.error("device {}: {}-{}".format(reply.dev_name(), err.desc, err.reason))
                else:
                    (result_code,msg) = reply.get_data()
                    self.logger.info("device {}: {}".format(reply.dev_name(), msg))
            if any(target_device._sc_subarray_obs_state[device] == ObsState.FAULT for device in  device_list) or target_device._failure_raised:
                return (ResultCode.FAILED, "GoToIdle Command FAILED")
            return (ResultCode.OK, "GoToIdle command executed OK")

    class ResetCommand(SKASubarray.ResetCommand):
        def do(self):
            target_device = self.target
            device_list = target_device._sc_subarray_fqdn
            try:
                self.logger.info("Creating group for Reset {}".format(device_list))
                sc_group = tango.Group("ResetGroup")
                for device in device_list:
                    if target_device._sc_subarray_state[device] != tango.DevState.FAULT:
                        continue
                    sc_group.add(device)
            except Exception:
                self.logger.error("TANGO Group command failed")
                return (ResultCode.FAILED, "Reset Command FAILED")
            self.logger.info("Issue Reset")
            answers = sc_group.command_inout("Reset")
            for reply in answers:
                if reply.has_failed():
                    for err in reply.get_err_stack():
                        self.logger.error("device {}: {}-{}".format(reply.dev_name(), err.desc, err.reason))
                else:
                    (result_code,msg) = reply.get_data()
                    self.logger.error("device {}: {}".format(reply.dev_name(), msg))
            if any(target_device._sc_subarray_state[device]== DevState.FAULT for device in  device_list):
                return (ResultCode.FAILED, "Reset Command FAILED")
            return (ResultCode.OK, "Reset command executed OK")

    # PROTECTED REGION ID(CspSubarray.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarray.class_variable
    # !! NOTE !!: 
    # In methods and attributes of the class:
    # 'sc' prefix stands for 'sub-component'
    # 'cb' suffix stands for 'callback'
    #----------------
    # Event Callback functions
    # ---------------
    def _sc_scm_change_event_cb(self, evt):
        """
        Class protected callback function.
        Retrieve the values of the sub-array sub-component SCM attributes subscribed
        at device connection.
        Sub-array sub-components are:
        - the CBF sub-array (assigned at initialization!)
        - the PSS sub-array if SearchBeams are assigned to the sub-array
        - the PSTBeams if TimingBeams are assigned to the sub-array
        These values are used to report the whole Csp Subarray State and healthState.

        :param evt: The event data

        :return: None
        """
        dev_name = evt.device.dev_name()
        if not evt.err:
            try:
                if dev_name in self._sc_subarray_fqdn:
                    if evt.attr_value.name.lower() == "state":
                        self._sc_subarray_state[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "healthstate":
                        self._sc_subarray_health_state[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "adminmode":
                        self._sc_subarray_admin_mode[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "obsstate":
                        self._sc_subarray_obs_state[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "obsmode":
                        self._sc_subarray_obs_mode[dev_name] = evt.attr_value.value
                    else:
                        log_msg = ("Attribute {} not still "
                                   "handled".format(evt.attr_name))
                        self.logger.warning(log_msg)
                else:
                    log_msg = ("Unexpected change event for"
                               " attribute: {}".format(str(evt.attr_name)))
                    self.logger.warning(log_msg)
                    return

                log_msg = "Received event on {}/{}: {}".format(dev_name, 
                                                          str(evt.attr_value.name),
                                                          str(evt.attr_value.value))
                self.logger.info(log_msg)
                # update CSP sub-array SCM
                #07-2020: with the new base classes, transitions are handled via actions.
                #if evt.attr_value.name.lower() in ["obsstate"]:
                #    self.update_subarray_state()
                if evt.attr_value.name.lower() in ["healthstate"]:
                    self._update_subarray_health_state()
            except tango.DevFailed as df:
                self.logger.error(str(df.args[0].desc))
            except Exception as except_occurred:
                self.logger.error(str(except_occurred))
        else:
            for item in evt.errors:
                # API_EventTimeout: if sub-element device not reachable it transitions
                # to UNKNOWN state.
                if item.reason == "API_EventTimeout":
                    self.logger.info("API_EventTimeout")
                    # only if the device is ONLINE/MAINTENANCE, its State is set to 
                    # UNKNOWN when there is a timeout on connection, otherwise its
                    # State should be reported always as DISABLE
                    if self._sc_subarray_admin_mode[dev_name] in [AdminMode.ONLINE,
                                                         AdminMode.MAINTENANCE]:
                        self._sc_subarray_state[dev_name] = tango.DevState.UNKNOWN
                        self._sc_subarray_health_state[dev_name] = HealthState.UNKNOWN
                        # TODO how report obsState? 
                        # adminMode can't be change otherwise the State and healthState
                        # are note updated
                    # 07-2020
                    # Note: with new base classes transition are handled via actions. But here we
                    # have to be careful....need to review this part
                    # update the State and healthState of the CSP sub-array
                    #self.update_subarray_state()
                log_msg = item.reason + ": on attribute " + str(evt.attr_name)
                self.logger.warning(log_msg)
    
    def _attributes_change_evt_cb(self, evt):
        """
        *Class callback function.*
        Retrieve the value of the sub-element xxxCommandProgress attribute
        subscribed for change event when a long-running command is issued
        on the sub-element device.

        :param evt: The event data

        :return: None
        """
        
        dev_name = evt.device.dev_name()
        if not evt.err:
            try:
                if "cmdprogress" == evt.attr_value.name.lower()[-11:]:
                    if dev_name in self._sc_subarray_fqdn:
                        cmd_name = evt.attr_value.name[:-15]
                        self._sc_subarray_cmd_progress[dev_name][cmd_name] = evt.attr_value.value
                elif "timeoutexpiredflag" == evt.attr_value.name.lower():
                    if dev_name in self._sc_subarray_fqdn:
                        self._timeout_expired = True
                else:
                    log_msg = ("Unexpected change event for"
                               " attribute: {}".format(str(evt.attr_name)))
                    self.logger.warning(log_msg)
                    return

                log_msg = "New value for {} is {}".format(str(evt.attr_name),
                                                          str(evt.attr_value.value))
                self.logger.info(log_msg)
            except tango.DevFailed as df:
                self.logger.error(str(df.args[0].desc))
            except Exception as except_occurred:
                self.logger.error(str(except_occurred))
        else:
            for item in evt.errors:
                log_msg = item.reason + ": on attribute " + str(evt.attr_name)
                self.logger.warning(log_msg)
              
    def _cmd_ended_cb(self, evt):
        """
        Callback function immediately executed when the asynchronous invoked
        command returns.

        :param evt: a CmdDoneEvent object. This class is used to pass data
            to the callback method in asynchronous callback model for command
            execution.
        :type: CmdDoneEvent object. It has the following members:

                - device     : (DeviceProxy) The DeviceProxy object on which the
                               call was executed.
                - cmd_name   : (str) The command name
                - argout_raw : (DeviceData) The command argout
                - argout     : The command argout
                - err        : (bool) A boolean flag set to true if the command
                               failed. False otherwise
                - errors     : (sequence<DevError>) The error stack
                - ext
        :return: none
        """
        # NOTE:if we try to access to evt.cmd_name or other paramters, sometime
        # the callback crashes withthis error:
        # terminate called after throwing an instance of 'boost::python::error_already_set'
        try:
            # Can happen evt empty??
            if evt:
                if not evt.err:
                    if evt.argout[0] == ResultCode.STARTED:
                        self.logger.info("Device {} is processing the command {}".format(evt.device.dev_name(),
                                                                                      evt.cmd_name))
                        if evt.cmd_name.lower() == 'configurescan':
                            self._reconfiguring = False
                    if evt.argout[0] == ResultCode.OK:
                        self.logger.info("Device {} successfully processed the command {}".format(evt.device.dev_name(),
                                                                                               evt.cmd_name))
                        if evt.cmd_name.lower() == 'configurescan':
                            self._reconfiguring = False
                    if evt.argout[0] == ResultCode.FAILED:
                        self.logger.info("Failure in Device {} while processing the command {}".format(evt.device.dev_name(),
                                                                                               evt.cmd_name))
                        self._sc_subarray_cmd_exec_state[evt.device.dev_name()][evt.cmd_name.lower()] = CmdExecState.FAILED
                        self.logger.info("sc_subarray_cmd_exec_state[{}][{}]:{}".format(evt.device.dev_name(), evt.cmd_name.lower(), 
                                                           self._sc_subarray_cmd_exec_state[evt.device.dev_name()][evt.cmd_name.lower()]))
                        self._failure_message[evt.cmd_name.lower()] += evt.argout[1]
                else:
                    msg = "Error!!Command {} ended on device {}.\n".format(evt.cmd_name,
                                                                           evt.device.dev_name())
                    msg += " Desc: {}".format(evt.errors[0].desc)
                    self.logger.info(msg)
                    self._sc_subarray_cmd_exec_state[evt.device.dev_name()][evt.cmd_name.lower()] = CmdExecState.FAILED
                    self._failure_message[evt.cmd_name.lower()] += msg
                    # obsState and obsMode values take on the CbfSubarray's values via
                    # the subscribe/publish mechanism
            else:
                self.logger.error("cmd_ended callback: evt is empty!!")
        except tango.DevFailed as df:
            msg = ("CommandCallback cmd_ended failure - desc: {}"
                   " reason: {}".format(df.args[0].desc, df.args[0].reason))
            self.logger.error(msg)
        except Exception as ex:
            msg = "CommandCallBack cmd_ended general exception: {}".format(str(ex))
            self.logger.error(msg)
           
    # Class protected methods
    # ---------------
    
    def update_subarray_state(self):
        """
        Class protected method.
        Retrieve the State attribute values of the CSP sub-elements and aggregate
        them to build up the CSP global state.
        This method should be called only when no command is running.

        :param: None
        :return: None
        """
        self.logger.info("update_subarray_state")
        self._update_subarray_health_state()
        # check if a long-running command is in execution
        for key, thread in self._command_thread.items():
            if thread.is_alive():
                self.logger.info("Tread {} is running".format(key))
                return
        target_obs_state = self.obs_state_evaluator()
        if target_obs_state != self._obs_state:
            self.set_csp_obs_state(target_obs_state)

    def _update_subarray_health_state(self):
        """
        Class protected method.
        Retrieve the healthState attribute of the CSP sub-elements and
        aggregate them to build up the CSP health state

        :param: None

        :return: None
        """
        # if the Subarray is OFF (no assigned resources) or DISABLE,
        # its health state is UNKNOWN.
        # Note: when the Subarray adminMode is set OFFLINE/RESERVED/NOT_FITTED
        # all its allocated resources are released and its State moves to DISABLE.
        # 
        if self.get_state() in [tango.DevState.OFF, tango.DevState.DISABLE]:
            self._health_state = HealthState.UNKNOWN
            
        # The whole CspSubarray HealthState is OK if is ON and all its assigned sub-components
        # (CBF and PSS subarrays as well PST Beams) are OK.
        # - CbfSubarray ON (receptors/stations assigned)
        # - PssSubarray ON
                
        # default value to DEGRADED
        self._health_state = HealthState.DEGRADED
        
        # build the list of all the Csp Subarray sub-components ONLINE/MAINTENANCE
        admin_fqdn = [fqdn for fqdn, admin_value in self._sc_subarray_admin_mode.items()
                      if admin_value in [AdminMode.ONLINE, AdminMode.MAINTENANCE]]
        # build the list of sub-elements with State ON
        state_fqdn =  [fqdn for fqdn in admin_fqdn if self._sc_subarray_state[fqdn] == tango.DevState.ON]
        # build the list with the healthState of ONLINE/MAINTENANCE devices 
        health_list = [self._sc_subarray_health_state[fqdn] for fqdn in state_fqdn]
        
        if self.CbfSubarray in admin_fqdn:
            if all(value == HealthState.OK for value in health_list):
                self._health_state = HealthState.OK
            elif self._sc_subarray_health_state[self.CbfSubarray] in [HealthState.FAILED,
                                                             HealthState.UNKNOWN,
                                                             HealthState.DEGRADED]:
                self._health_state = self._sc_subarray_health_state[self.CbfSubarray]
        else:
            # if CBF Subarray is not ONLINE/MAINTENANCE ....
            self._health_state = self._sc_subarray_health_state[self.CbfSubarray]
        return
    
    '''
    def _update_subarray_obs_state(self):
        """
        Class protected method.
        Retrieve the State attribute values of the CSP sub-elements and aggregate
        them to build up the CSP global state.

        :param: None

        :return: None
        """
        # update the CspSubarray ObsMode only when no command is running on sub-array
        # sub-components
        exec_state_list = []
        device_list = self._sc_subarray_fqdn
        if not any(self._sc_subarray_assigned_fqdn):
            for fqdn in self._sc_subarray_fqdn:
                if self._sc_subarray_obs_state[fqdn] == ObsState.READY:
                    self._sc_subarray_assigned_fqdn.append(fqdn)
                    device_list = self._sc_subarray_assigned_fqdn
        for fqdn in device_list:
            device_exec_state_list = [value for value in self._sc_subarray_cmd_exec_state[fqdn].values()]
            exec_state_list.extend(device_exec_state_list)
            # when command are not running, the CbfSubarray osbState reflects the
            # CSP Subarray obsState.
            # If CbfSubarray obsState is READY and PssSubarray IDLE, the CspSubarray
            # onsState is READY (it can performs IMAGING!)
            if all(value == CmdExecState.IDLE for value in exec_state_list) or (not any(exec_state_list)):
                self._obs_state = self._sc_subarray_obs_state[self.CbfSubarray]
                self.logger.info("All sub-array sub-component are IDLE."
                                  "Subarray obsState:{}".format( self._obs_state))
        self.logger.info("Subarray ObsState:{}".format( self._obs_state))
    '''
    def _open_connection(self, fqdn):
        device_proxy = DeviceProxy(fqdn)
        return device_proxy

    def _get_expected_delay(self, proxy,attr_name):
        try:
            attr_value = proxy.read_attribute(attr_name)
            return attr_value.value
        except AttributeError as attr_err:
            self.logger.info("No attribute {} on device {}".format(str(attr_err), proxy))
            tango.Except.throw_exception("Attribute Error", str(attr_err), "", tango.ErrSeverity.ERR)


    def connect_to_subarray_subcomponent(self, fqdn):
        """
        Class method.
        Establish a *stateless* connection with a CSP Subarray sub-component
        device (CBF Subarray, PSS Subarray , PST Beams).
        Exceptions are logged.

        :param fqdn:
                the CspSubarray sub-component FQDN
        :return: None
        """
        
        # check if the device has already been addedd
        if fqdn in self._sc_subarray_fqdn:
            return
        # read the sub-componet adminMode (memorized) attribute from
        # the CSP.LMC TANGO DB. 
        #attribute_properties = self._csp_tango_db.get_device_attribute_property(fqdn, 
        #                                                                     {'adminMode': ['__value']})
        #self.logger.debug("fqdn: {} attribute_properties: {}".format(fqdn, attribute_properties))
        #try:
        #    admin_mode_memorized = attribute_properties['adminMode']['__value']
        #    self._sc_subarray_admin_mode[fqdn] = int(admin_mode_memorized[0])
        #except KeyError as key_error:
        #    self.logger.warning("No key {} found".format(str(key_error)))    
        try:
            log_msg = "Trying connection to " + str(fqdn) + " device"
            self.logger.info(log_msg)
            device_proxy = self._open_connection(fqdn)
            self.logger.info("fqdn: {} device_proxy: {}".format(fqdn, device_proxy))
            # Note: The DeviceProxy is initialized even if the sub-component
            # device is not running (but defined into the TANGO DB! If not defined in the
            # TANGO DB a exception is throw). 
            # The connection with a sub-element is establish as soon as the corresponding
            # device starts. 
            # append the FQDN to the list and store the sub-element proxies
            self._sc_subarray_fqdn.append(fqdn)
            self._sc_subarray_proxies[fqdn] = device_proxy
            
            # subscription of SCM attributes (State, healthState and adminMode).
            # Note: subscription is performed also for devices not ONLINE or MAINTENANCE.
            # In this way the CspMaster is able to detect a change in the admin value.
               
            ev_id = device_proxy.subscribe_event("adminMode",
                                                 EventType.CHANGE_EVENT,
                                                 self._sc_scm_change_event_cb,
                                                 stateless=True)
            self._sc_subarray_event_id[fqdn]['adminMode'] = ev_id
                
            ev_id = device_proxy.subscribe_event("State",
                                                 EventType.CHANGE_EVENT,
                                                 self._sc_scm_change_event_cb,
                                                 stateless=True)
            self._sc_subarray_event_id[fqdn]['state'] = ev_id

            ev_id = device_proxy.subscribe_event("healthState",
                                                 EventType.CHANGE_EVENT,
                                                 self._sc_scm_change_event_cb,
                                                 stateless=True)
            self._sc_subarray_event_id[fqdn]['healthState'] = ev_id
            
            ev_id = device_proxy.subscribe_event("obsState",
                                                 EventType.CHANGE_EVENT,
                                                 self._sc_scm_change_event_cb,
                                                 stateless=True)
            self._sc_subarray_event_id[fqdn]['obsState'] = ev_id
                
            #ev_id = device_proxy.subscribe_event("obsMode",
            #                                     EventType.CHANGE_EVENT,
            #                                     self._sc_scm_change_event_cb,
            #                                     stateless=True)
            #self._sc_subarray_event_id[fqdn]['obsMode'] = ev_id
                
        except KeyError as key_err:
            log_msg = ("No key {} found".format(str(key_err)))
            self.logger.warning(log_msg)
        except tango.DevFailed as df:
            log_msg = ("Failure in connection to {}"
                       " device: {}".format(str(fqdn), str(df.args[0].desc)))
            self.logger.error(log_msg)

    def _is_sc_subarray_running (self, device_name):
        """
        *Class protected method.*

        Check if a sub-element is exported in the TANGO DB (i.e its TANGO 
        device server is running).
        If the device is not in the list of the connected sub-elements,
        a connection with the device is performed.

        :param subelement_name : the FQDN of the sub-element
        :type: `DevString`
        :return: True if the connection with the subarray is established, 
        False otherwise
        """
        try:
            proxy = self._sc_subarray_proxies[device_name]
            proxy.ping()
        except KeyError as key_err:
            # Raised when a mapping (dictionary) key is not found in the set
            # of existing keys.
            # no proxy registered for the subelement device
            msg = "Can't retrieve the information of key {}".format(key_err)
            self.logger.warning(msg)
            try: 
                proxy = DeviceProxy(device_name)
                # execute a ping to detect if the device is actually running
                proxy.ping()
                self._sc_subarray_proxies[device_name] = proxy
            except tango.DevFailed as df:  
                return False  
        except tango.DevFailed as df:
            msg = "Failure reason: {} Desc: {}".format(str(df.args[0].reason), str(df.args[0].desc))
            self.logger.warning(msg)
            return False
        return True
    
    def _is_subarray_composition_allowed(self):
        if self.get_state() in [tango.DevState.ON, tango.DevState.OFF]:
            return True
        return False
    
    def _is_subarray_configuring_allowed(self):
        if self.get_state() == tango.DevState.ON:
            return True
        return False
    
    def _push_event_on_obs_state(self,value):
        self._obs_state = value
        self.push_change_event("obsState", self._obs_state)
    
    # ----------------
    # Class private methods
    # ----------------

    def _init_state_model(self):
        """
        Sets up the state model for the device
        """
        self.state_model = CspSubarrayStateModel(
            logger=self.logger,
            op_state_callback=self._update_state,
            admin_mode_callback=self._update_admin_mode,
            obs_state_callback=self._update_obs_state,
        )

    def init_command_objects(self):
        """
        Sets up the command objects.
        The init_command_method is called after InitCommand in the
        SKABaseDevice class. 
        This means that the command object handler has to be defined into
        the InitCommandClass.
        """
        super().init_command_objects()

        args = (self, self.state_model, self.logger)
        self.gotoidle_cmd_obj = self.GoToIdleCommand(*args)
        self.abort_cmd_obj = self.AbortCommand(*args)
        self.register_command_object("GoToIdle", self.GoToIdleCommand(*args))
        self.register_command_object("Configure", self.ConfigureCommand(*args))
        self.register_command_object("Scan", self.ScanCommand(*args))
        self.register_command_object("ObsReset", self.ObsResetCommand(*args))
        self.register_command_object("Abort", self.AbortCommand(*args))
        self.register_command_object("Restart", self.RestartCommand(*args))


    # ----------------
    # Class Properties
    # ----------------

    PSTBeams = class_property(
        dtype='DevVarStringArray',
    )

    # -----------------
    # Device Properties
    # -----------------
    
    CspMaster = device_property(
        dtype='DevString', 
    )

    CbfSubarray = device_property(
        dtype='DevString',
    )

    PssSubarray = device_property(
        dtype='DevString',
    )

    SubarrayProcModeCorrelation = device_property(
        dtype='DevString',
    )

    SubarrayProcModePss = device_property(
        dtype='DevString',
    )

    SubarrayProcModePst = device_property(
        dtype='DevString',
    )

    SubarrayProcModeVlbi = device_property(
        dtype='DevString',
    )

    # ----------
    # Attributes
    # ----------
    
    scanID = attribute(
        dtype='DevULong64',
        access=AttrWriteType.READ_WRITE,
    )

    procModeCorrelationAddr = attribute(
        dtype='DevString',
        label="Correlation Inherent Capability Address",
        doc="The CSP sub-array Correlation Inherent Capability FQDN.",
    )

    procModePssAddr = attribute(
        dtype='DevString',
        label="PSS Inherent Capability address",
        doc="The CSP sub-array PSS Inherent Capability FQDN.",
    )

    procModePstAddr = attribute(
        dtype='DevString',
        label="PST Inherent Capability address",
        doc="The CSP sub-array PST Inherent Capability FQDN.",
    )

    procModeVlbiAddr = attribute(
        dtype='DevString',
        label="VLBI Inhernt Capabilityaddress",
        doc="The CSP sub-array VLBI Inherent Capability FQDN.",
    )

    cbfSubarrayState = attribute(
        dtype='DevState',
    )

    pssSubarrayState = attribute(
        dtype='DevState',
    )

    cbfSubarrayHealthState = attribute(
        dtype='DevEnum',
        label="CBF Subarray Health State",
        doc="CBF Subarray Health State",
        enum_labels=["OK", "DEGRADED", "FAILED", "UNKNOWN", ],
    )

    pssSubarrayHealthState = attribute(
        dtype='DevEnum',
        label="PSS Subarray Health State",
        doc="PSS Subarray Health State",
        enum_labels=["OK", "DEGRADED", "FAILED", "UNKNOWN", ],
    )

    cbfSubarrayAdminMode = attribute(
        dtype='DevEnum',
        label="CBF Subarray Admin Mode",
        doc="CBF Subarray Admin Mode",
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED",],
    )

    pssSubarrayAdminMode = attribute(
        dtype='DevEnum',
        label="PSS Subarray Admin Mode",
        doc="PSS Subarray Admin Mode",
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED",],
    )
    cbfSubarrayObsState = attribute(
        dtype='DevEnum',
        label="CBF Subarray Observing State",
        doc="The CBF subarray observing state.",
        enum_labels=["IDLE", "CONFIGURING", "READY", "SCANNING", "PAUSED", "ABORTED", "FAULT",],
    )

    pssSubarrayObsState = attribute(
        dtype='DevEnum',
        label="PSS Subarray Observing State",
        doc="The PSS subarray observing state.",
        enum_labels=["IDLE", "CONFIGURING", "READY", "SCANNING", "PAUSED", "ABORTED", "FAULT",],
    )

    pssSubarrayAddr = attribute(
        dtype='DevString',
        label="PSS sub-array address",
        doc="The PSS sub-element sub-array FQDN.",
    )

    cbfSubarrayAddr = attribute(
        dtype='DevString',
        label="CBF sub-array address",
        doc="The CBF sub-element sub-array FQDN.",
    )

    validScanConfiguration = attribute(
        dtype='DevString',
        label="Valid Scan Configuration",
        doc="Store the last valid scan configuration.",
    )

    addSearchBeamsDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="AddSearchBeams command duration expected",
        doc="The duration expected (in sec) for the AddSearchBeams command.",
    )

    remSearchBeamsDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="RemoveSearchBeams command duration expected",
        doc="The duration expected (in sec) for the RemoveSearchBeams command.",
    )

    addSearchBeamsDurationMeasured = attribute(
        dtype='DevUShort',
        label="AddSearchBeams command duration measured",
        doc="The duration measured (in sec) for the AddSearchBeams command.",
    )

    remSearchBeamsDurationMeasured = attribute(
        dtype='DevUShort',
        label="RemoveSearchBeams command duration measured",
        doc="The duration measured (in sec) for the RemoveSearchBeams command.",
    )


    addTimingBeamsDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="AddTimingBeams command duration expected",
        doc="The duration expected (in sec) for the AddTimingBeams command.",
    )

    remTimingBeamsDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="RemoveTimingBeams command duration expected",
        doc="The duration expected (in sec) for the RemoveTimingBeams command.",
    )

    addTimingBeamsDurationMeasured = attribute(
        dtype='DevUShort',
        label="AddTimingBeams command duration measured",
        doc="The duration measured (in sec) for the AddTimingBeams command.",
    )

    remTimingBeamsDurationMeasured = attribute(
        dtype='DevUShort',
        label="GoToIdle command duration measured",
        doc="The duration measured (in sec) for the RemoveTimingBeams command.",
    )

    goToIdleDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="GoToIdle command duration expected",
        doc="The duration expected (in sec) for the GoToIdle command.",
    )

    goToIdleDurationMeasured = attribute(
        dtype='DevUShort',
        label="GoToIdle command duration measured",
        doc="The duration measured (in sec) for the GoToIdle command.",
    )

    goToIdleCmdProgress = attribute(
        dtype='DevUShort',
        label="GoToIdle command progress percentage",
        polling_period=1500,
        abs_change=5,
        doc="The progress percentage for the GoToIdle command.",
    )

    endScanDurationExpected = attribute(
        dtype='DevUShort',
        label="EndScan command duration expected",
        doc="The duration expected (in sec) for the EndScan command.",
    )

    endScanDurationMeasured = attribute(
        dtype='DevUShort',
        label="EndScan command duration measured",
        doc="The duration measured (in sec) for the EndScan command.",
    )

    endScanCmdProgress = attribute(
        dtype='DevUShort',
        label="EndScan command progress percentage",
        polling_period=1500,
        abs_change=5,
        doc="The progress percentage for the EndScan command.",
    )

    
    addResourcesCmdProgress = attribute(
        dtype='DevUShort',
        label="Add resources command progress percentage",
        polling_period=1500,
        abs_change=5,
        doc="The progress percentage for the Add resources command.",
    )
    
    removeResourcesCmdProgress = attribute(
        dtype='DevUShort',
        label="Remove resources command progress percentage",
        polling_period=1500,
        abs_change=5,
        doc="The progress percentage for the Add/SearchBeams command.",
    )
    
    scanCmdProgress = attribute(
        dtype='DevUShort',
        label="Scan command progress percentage",
        polling_period=1500,
        abs_change=5,
        doc="The progress percentage for the Scan command.",
    )

    reservedSearchBeamNum = attribute(
        dtype='DevUShort',
        label="Number of reserved SearchBeam IDs",
        doc="Number of SearchBeam IDs reserved for the CSP sub-array",
    )

    numOfDevCompletedTask = attribute(
        dtype='DevUShort',
        label="Number of devices that completed the task",
        doc="Number of devices that completed the task",
    )

    timeoutExpiredFlag = attribute(
        dtype='DevBoolean',
        label="CspSubarray command execution timeout flag",
        polling_period=1000,
        doc="The timeout flag for a CspSubarray command.",
    )

    failureRaisedFlag = attribute(
        dtype='DevBoolean',
        label="CspSubarray failure flag",
        polling_period=1000,
        doc="The failure flag for a CspSubarray command.",
    )

    cmdFailureMessage = attribute(
        dtype='DevString',
        label="The failure message",
        doc="The failure message on command execution",
    )

    isCmdInProgress = attribute(
        dtype='DevBoolean',
    )

    configurationID = attribute(
        dtype='DevString',
        label="Configuration ID",
        doc="The configuration identifier (string)",
    )

    pstOutputLink = attribute(
        dtype='DevString',
        label="PST output link",
        doc="The output link for PST products.",
    )

    assignedSearchBeamIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="List of assigned Search Beams",
        doc="List of assigned Search Beams",
    )

    reservedSearchBeamIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="List of reserved SearchBeam IDs",
        doc="List of SearchBeam IDs reserved for the CSP sub-array",
    )

    assignedTimingBeamIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=16,
        label="List of assigned TimingBeam IDs",
        doc="List of TimingBeam IDs assigned to the  CSP sub-array",
    )

    assignedVlbiBeamIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=20,
        label="List of assigned VlbiBeam IDs",
        doc="List of VlbiBeam IDs assigned to the  CSP sub-array",
    )

    assignedSearchBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="Assigned SearchBeams State",
        doc="State of the assigned SearchBeams",
    )

    assignedTimingBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="Assigned TimingBeams State",
        doc="State of the assigned TimingBeams",
    )

    assignedVlbiBeamsState = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="Assigned VlbiBeams State",
        doc="State of the assigned VlbiBeams",
    )

    assignedSearchBeamsHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="Assigned SearchBeams HealthState",
        doc="HealthState of the assigned SearchBeams",
    )

    assignedTimingBeamsHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=16,
        label="Assigned TimingBeams HealthState",
        doc="HealthState of the assigned TimingBeams",
    )

    assignedVlbiBeamsHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=20,
        label="Assigned VlbiBeams HealthState",
        doc="HealthState of the assigned VlbiBeams",
    )

    assignedSearchBeamsObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="Assigned SearchBeams ObsState",
        doc="ObsState of the assigned SearchBeams",
    )

    assignedTimingBeamsObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=16,
        label="Assigned TimingBeams ObsState",
        doc="ObsState of the assigned TimingBeams",
    )

    assignedVlbiBeamsObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=20,
        label="Assigned VlbiBeams ObsState",
        doc="ObsState of the assigned VlbiBeams",
    )

    assignedSearchBeamsAdminMode = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="Assigned SearchBeams AdminMode",
        doc="AdminMode of the assigned SearchBeams",
    )

    assignedTimingBeamsAdminMode = attribute(
        dtype=('DevUShort',),
        max_dim_x=16,
        label="Assigned TimingBeams AdminMode",
        doc="AdminMode of the assigned TimingBeams",
    )

    assignedVlbiBeamsAdminMode = attribute(
        dtype=('DevUShort',),
        max_dim_x=20,
        label="Assigned VlbiBeams AdminMode",
        doc="AdminMode of the assigned VlbiBeams",
    )

    pstBeams = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        label="PSTBeams addresses",
        doc="PST sub-element PSTBeam TANGO device FQDNs.",
    )

    listOfDevCompletedTask = attribute(
        dtype=('DevString',),
        max_dim_x=100,
        label="List of devices that completed the task",
        doc="List of devices that completed the task",
    )

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarray.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarray.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarray.delete_device) ENABLED START #
        #release the allocated event resources
        # check for running threads and stop them
        for key, thread in self._command_thread.items():
            is_alive = thread.is_alive()
            if is_alive:
                self._stop_thread[key] = True
                thread.join()
        event_to_remove = {}
        for fqdn in self._sc_subarray_fqdn:
            try:
                event_to_remove[fqdn] = []
                for attr_name, event_id in self._sc_subarray_event_id[fqdn].items():
                    try:
                        # self._se_subarray_event_id[fqdn].remove(event_id)
                        # in Pyton3 can't remove the element from the list while looping on it.
                        # Store the unsubscribed events in a temporary list and remove them later.
                        if event_id:
                            self._sc_subarray_proxies[fqdn].unsubscribe_event(event_id)
                            event_to_remove[fqdn].append(attr_name)
                    except tango.DevFailed as df:
                        msg = ("Unsubscribe event failure.Reason: {}. "
                               "Desc: {}".format(df.args[0].reason, df.args[0].desc))
                        self.logger.error(msg)
                    except KeyError as key_err:
                        # NOTE: in PyTango unsubscription of a not-existing event id raises a
                        # KeyError exception not a DevFailed!!
                        msg = "Unsubscribe event failure. Reason: {}".format(str(key_err))
                        self.logger.error(msg)
                # remove the attribute entry from the fqdn dictionary
                for attr_name in event_to_remove[fqdn]:
                    del self._sc_subarray_event_id[fqdn][attr_name]
                # check if there are still some registered events. What to do in this case??
                if not self._sc_subarray_event_id[fqdn]:
                    # remove the dictionary element when the fqdn dictionary is
                    # empty
                    self._sc_subarray_event_id.pop(fqdn)
                else:
                    msg = "Still subscribed events: {} for device {}".format(self._sc_subarray_event_id[fqdn], fqdn)
                    self.logger.warning(msg)
            except KeyError as key_err:
                msg = " Can't retrieve the information of key {}".format(key_err)
                self.logger.err(msg)
        # clear the subarrays list and dictionary
        self._sc_subarray_fqdn.clear()
        self._sc_subarray_proxies.clear()

        # PROTECTED REGION END #    //  CspSubarray.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_scanID(self):
        # PROTECTED REGION ID(CspSubarray.scanID_read) ENABLED START #
        """Return the scanID attribute."""
        return self._scan_id 
        # PROTECTED REGION END #    //  CspSubarray.scanID_read

    def write_scanID(self, value):
        # PROTECTED REGION ID(CspSubarray.scanID_write) ENABLED START #
        """Set the scanID attribute."""
        self._scan_id = value
        # PROTECTED REGION END #    //  CspSubarray.scanID_write

    def read_procModeCorrelationAddr(self):
        # PROTECTED REGION ID(CspSubarray.procModeCorrelationAddr_read) ENABLED START #
        """Return the procModeCorrelationAddr attribute."""
        return self.SubarrayProcModeCorrelation
        # PROTECTED REGION END #    //  CspSubarray.corrInherentCapAddr_read

    def read_procModePssAddr(self):
        # PROTECTED REGION ID(CspSubarray.procModePssAddr_read) ENABLED START #
        """Return the procModePssAddr attribute."""
        return self.SubarrayProcModePss
        # PROTECTED REGION END #    //  CspSubarray.pssInherentCapAddr_read

    def read_procModePstAddr(self):
        # PROTECTED REGION ID(CspSubarray.procModePstAdd_read) ENABLED START #
        """Return the procModePstAddr( attribute."""
        return self.SubarrayProcModePst
        # PROTECTED REGION END #    //  CspSubarray.procModePsstdd_read

    def read_procModeVlbiAddr(self):
        # PROTECTED REGION ID(CspSubarray.procModeVlbiAdd_read) ENABLED START #
        """Return the procModeVlbiAddr attribute."""
        return self.SubarrayProcModeVlbi
        # PROTECTED REGION END #    //  CspSubarray.procModeVlbiAdd_read

    def read_cbfSubarrayState(self):
        # PROTECTED REGION ID(CspSubarray.cbfSubarrayState_read) ENABLED START #
        """Return the cbfSubarrayState attribute."""
        return self._sc_subarray_state[self.CbfSubarray]
        # PROTECTED REGION END #    //  CspSubarray.cbfSubarrayState_read

    def read_pssSubarrayState(self):
        # PROTECTED REGION ID(CspSubarray.pssSubarrayState_read) ENABLED START #
        """Return the pssSubarrayState attribute."""
        return self._sc_subarray_state[self.PssSubarray]
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayState_read

    def read_cbfSubarrayHealthState(self):
        # PROTECTED REGION ID(CspSubarray.cbfSubarrayHealthState_read) ENABLED START #
        """Return the cbfSubarrayHealthState attribute."""
        return self._sc_subarray_health_state[self.CbfSubarray]
        # PROTECTED REGION END #    //  CspSubarray.cbfSubarrayHealthState_read

    def read_pssSubarrayHealthState(self):
        # PROTECTED REGION ID(CspSubarray.pssSubarrayHealthState_read) ENABLED START #
        """Return the pssSubarrayHealthState attribute."""
        return self._sc_subarray_health_state[self.PssSubarray]
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayHealthState_read

    def read_cbfSubarrayAdminMode(self):
        # PROTECTED REGION ID(CspSubarray.cbfSubarrayHealthState_read) ENABLED START #
        """Return the cbfSubarrayHealthState attribute."""
        return self._sc_subarray_admin_mode[self.CbfSubarray]
        # PROTECTED REGION END #    //  CspSubarray.cbfSubarrayHealthState_read

    def read_pssSubarrayAdminMode(self):
        # PROTECTED REGION ID(CspSubarray.pssSubarrayHealthState_read) ENABLED START #
        """Return the pssSubarrayHealthState attribute."""
        return self._sc_subarray_admin_mode[self.PssSubarray]
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayHealthState_read
        
    def read_cbfSubarrayObsState(self):
        # PROTECTED REGION ID(CspSubarray.cbfSubarrayObsState_read) ENABLED START #
        """Return the cbfSubarrayObsState attribute."""
        return self._sc_subarray_obs_state[self.CbfSubarray]
        # PROTECTED REGION END #    //  CspSubarray.cbfSubarrayObsState_read

    def read_pssSubarrayObsState(self):
        # PROTECTED REGION ID(CspSubarray.pssSubarrayObsState_read) ENABLED START #
        """Return the pssSubarrayObsState attribute."""
        return self._sc_subarray_obs_state[self.PssSubarray]
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayObsState_read

    def read_pssSubarrayAddr(self):
        # PROTECTED REGION ID(CspSubarray.pssSubarrayAddr_read) ENABLED START #
        """Return the pssSubarrayAddr attribute."""
        return self.PssSubarray
        # PROTECTED REGION END #    //  CspSubarray.pssSubarrayAddr_read

    def read_cbfSubarrayAddr(self):
        # PROTECTED REGION ID(CspSubarray.cbfSubarrayAddr_read) ENABLED START #
        """Return the cbfSubarrayAddr attribute."""
        return self.CbfSubarray
        # PROTECTED REGION END #    //  CspSubarray.cbfSubarrayAddr_read

    def read_validScanConfiguration(self):
        # PROTECTED REGION ID(CspSubarray.validScanConfiguration_read) ENABLED START #
        """Return the validScanConfiguration attribute."""
        return self._valid_scan_configuration
        # PROTECTED REGION END #    //  CspSubarray.validScanConfiguration_read

    def read_addSearchBeamsDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.addSearchBeamsDurationExpected_read) ENABLED START #
        """Return the addSearchBeamsDurationExpected attribute."""
        return self._cmd_duration_expected['addsearchbeams']
        # PROTECTED REGION END #    //  CspSubarray.addSearchBeamsDurationExpected_read

    def write_addSearchBeamsDurationExpected(self, value):
        # PROTECTED REGION ID(CspSubarray.addSearchBeamsDurationExpected_write) ENABLED START #
        """Set the addSearchBeamsDurationExpected attribute."""
        self._cmd_duration_expected['addsearchbeams'] = value
        # PROTECTED REGION END #    //  CspSubarray.addSearchBeamDurationExpected_write

    def read_remSearchBeamsDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.remSearchBeamsDurationExpected_read) ENABLED START #
        """Return the remSearchBeamsDurationExpected attribute."""
        return self._cmd_duration_expected['removesearchbeams']
        # PROTECTED REGION END #    //  CspSubarray.remSearchBeamsDurationExpected_read

    def write_remSearchBeamsDurationExpected(self, value):
        # PROTECTED REGION ID(CspSubarray.remSearchBeamsDurationExpected_write) ENABLED START #
        """Set the remSearchBeamsDurationExpected attribute."""
        self._cmd_duration_expected['removesearchbeams'] = value
        # PROTECTED REGION END #    //  CspSubarray.remSearchBeamsDurationExpected_write

    def read_addSearchBeamsDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.addSearchBeamsDurationMeasured_read) ENABLED START #
        """Return the addSearchBeamsDurationMeasured attribute."""
        return self._cmd_duration_measured['addsearchbeams']
        # PROTECTED REGION END #    //  CspSubarray.addSearchBeamsDurationMeasured_read

    def read_remSearchBeamsDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.remSearchBeamsDurationMeasured_read) ENABLED START #
        """Return the remSearchBeamsDurationMeasured attribute."""
        return self._cmd_duration_measured['removesearchbeams']
        # PROTECTED REGION END #    //  CspSubarray.remSearchBeamsDurationMeasured_read

    def read_addTimingBeamsDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.addTimingBeamsDurationExpected_read) ENABLED START #
        """Return the addTimingBeamsDurationExpected attribute."""
        return self._cmd_duration_expected['addtimingbeams']
        # PROTECTED REGION END #    //  CspSubarray.addTimingBeamsDurationExpected_read

    def write_addTimingBeamsDurationExpected(self, value):
        # PROTECTED REGION ID(CspSubarray.addTimingBeamsDurationExpected_write) ENABLED START #
        """Set the addTimingBeamsDurationExpected attribute."""
        self._cmd_duration_expected['addtimingbeams'] = value
        # PROTECTED REGION END #    //  CspSubarray.addTimingBeamsDurationExpected_write

    def read_remTimingBeamsDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.remTimingBeamsDurationExpected_read) ENABLED START #
        """Return the remTimingBeamsDurationExpected attribute."""
        return self._cmd_duration_expected['removetimingbeams']
        # PROTECTED REGION END #    //  CspSubarray.remTimingBeamsDurationExpected_read

    def write_remTimingBeamsDurationExpected(self, value):
        # PROTECTED REGION ID(CspSubarray.remTimingBeamsDurationExpected_write) ENABLED START #
        """Set the remTimingBeamsDurationExpected attribute."""
        self._cmd_duration_expected['removetimingbeams'] = value
        # PROTECTED REGION END #    //  CspSubarray.remTimingBeamsDurationExpected_write

    def read_addTimingBeamsDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.addTimingBeamsDurationMeasured_read) ENABLED START #
        """Return the addTimingBeamDurationMeasured attribute."""
        return self._cmd_duration_measured['addtimingbeams']
        # PROTECTED REGION END #    //  CspSubarray.addTimingBeamsDurationMeasured_read

    def read_remTimingBeamsDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.remTimingBeamsDurationMeasured_read) ENABLED START #
        """Return the remTimingBeamsDurationMeasured attribute."""
        return self._cmd_duration_measured['removetimingbeams']
        # PROTECTED REGION END #    //  CspSubarray.remTimingBeamsDurationMeasured_read
    
    def read_addResourcesCmdProgress(self):
        # PROTECTED REGION ID(CspSubarray.searchBeamsCmdProgress_read) ENABLED START #
        """Return the assResourcesCmdProgress attribute."""
        return self._cmd_progress['addbeams']
        # PROTECTED REGION END #    //  CspSubarray.searchBeamsCmdProgress_read

    def read_goToIdleDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.goToIdleDurationExpected_read) ENABLED START #
        """Return the goToIdleDurationExpected attribute."""
        return self._cmd_duration_expected['gotoidle']
       
        # PROTECTED REGION END #    //  CspSubarray.goToIdleDurationExpected_read

    def write_goToIdleDurationExpected(self, value):
        # PROTECTED REGION ID(CspSubarray.goToIdleDurationExpected_write) ENABLED START #
        """Set the goToIdleDurationExpected attribute."""
        self._cmd_duration_expected['gotoidle'] = value
        # PROTECTED REGION END #    //  CspSubarray.endSBDurationExpected_write

    def read_goToIdleDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.goToIdleDurationMeasured_read) ENABLED START #
        """Return the goToIdleDurationMeasured attribute."""
        return self._cmd_duration_measured['gotoidle']
        # PROTECTED REGION END #    //  CspSubarray.goToIdleDurationMeasured_read

    def read_endScanDurationExpected(self):
        # PROTECTED REGION ID(CspSubarray.endScanDurationExpected_read) ENABLED START #
        """Return the endScanDurationExpected attribute."""
        return self._cmd_duration_expected['endscan']
        # PROTECTED REGION END #    //  CspSubarray.endScanDurationExpected_read

    def read_endScanDurationMeasured(self):
        # PROTECTED REGION ID(CspSubarray.endScanDurationMeasured_read) ENABLED START #
        """Return the endScanDurationMeasured attribute."""
        return self._cmd_duration_measured['endscan']
        # PROTECTED REGION END #    //  CspSubarray.endScanDurationMeasured_read

    def read_removeResourcesCmdProgress(self):
        # PROTECTED REGION ID(CspSubarray.removeResourcesCmdProgress_read) ENABLED START #
        """Return the removeResourcesCmdProgress attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSubarray.removeResourcesCmdProgress_read

    def read_goToIdleCmdProgress(self):
        # PROTECTED REGION ID(CspSubarray.goToIdleCmdProgress_read) ENABLED START #
        """Return the goToIdleCmdProgress attribute."""
        return self._cmd_progress['gotoidle']
        # PROTECTED REGION END #    //  CspSubarray.goToIdleCmdProgress_read

    def read_endScanCmdProgress(self):
        # PROTECTED REGION ID(CspSubarray.endScanCmdProgress_read) ENABLED START #
        """Return the endScanCmdProgress attribute."""
        return self._cmd_progress['endscan']
        # PROTECTED REGION END #    //  CspSubarray.endScanCmdProgress_read

    def read_reservedSearchBeamNum(self):
        # PROTECTED REGION ID(CspSubarray.reservedSearchBeamNum_read) ENABLED START #
        """Return the reservedSearchBeamNum attribute."""
        return 0
        # PROTECTED REGION END #    //  CspSubarray.reservedSearchBeamNum_read

    def read_numOfDevCompletedTask(self):
        # PROTECTED REGION ID(CspSubarray.numOfDevCompletedTask_read) ENABLED START #
        """Return the numOfDevCompletedTask attribute."""
        return self._num_dev_completed_task[self._last_executed_command]
        # PROTECTED REGION END #    //  CspSubarray.numOfDevCompletedTask_read

    def read_assignedSearchBeamIDs(self):
        # PROTECTED REGION ID(CspSubarray.assignedSearchBeamIDs_read) ENABLED START #
        """Return the assignedSearchBeamIDs attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedSearchBeamIDs_read

    def read_reservedSearchBeamIDs(self):
        # PROTECTED REGION ID(CspSubarray.reservedSearchBeamIDs_read) ENABLED START #
        """Return the reservedSearchBeamIDs attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.reservedSearchBeamIDs_read

    def read_assignedTimingBeamIDs(self):
        # PROTECTED REGION ID(CspSubarray.assignedTimingBeamIDs_read) ENABLED START #
        """Return the assignedTimingBeamIDs attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedTimingBeamIDs_read

    def read_assignedVlbiBeamIDs(self):
        # PROTECTED REGION ID(CspSubarray.assignedVlbiBeamIDs_read) ENABLED START #
        """Return the assignedVlbiBeamIDs attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedVlbiBeamIDs_read

    def read_assignedSearchBeamsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedSearchBeamsState_read) ENABLED START #
        """Return the assignedSearchBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarray.assignedSearchBeamsState_read

    def read_assignedTimingBeamsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedTimingBeamsState_read) ENABLED START #
        """Return the assignedTimingBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarray.assignedTimingBeamsState_read

    def read_assignedVlbiBeamsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedVlbiBeamsState_read) ENABLED START #
        """Return the assignedVlbiBeamsState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  CspSubarray.assignedVlbiBeamsState_read

    def read_assignedSearchBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarray.assignedSearchBeamsHealthState_read) ENABLED START #
        """Return the assignedSearchBeamsHealthState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedSearchBeamsHealthState_read

    def read_assignedTimingBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarray.assignedTimingBeamsHealthState_read) ENABLED START #
        """Return the assignedTimingBeamsHealthState attribute."""
        
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedTimingBeamsHealthState_read

    def read_assignedVlbiBeamsHealthState(self):
        # PROTECTED REGION ID(CspSubarray.assignedVlbiBeamsHealthState_read) ENABLED START #
        """Return the assignedVlbiBeamsHealthState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedVlbiBeamsHealthState_read

    def read_assignedSearchBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedSearchBeamsObsState_read) ENABLED START #
        """Return the assignedSearchBeamsObsState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedSearchBeamsObsState_read

    def read_assignedTimingBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedTimingBeamsObsState_read) ENABLED START #
        """Return the assignedTimingBeamsObsState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedTimingBeamsObsState_read

    def read_assignedVlbiBeamsObsState(self):
        # PROTECTED REGION ID(CspSubarray.assignedVlbiBeamsObsState_read) ENABLED START #
        """Return the assignedVlbiBeamsObsState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedVlbiBeamsObsState_read

    def read_assignedSearchBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarray.assignedSearchBeamsAdminMode_read) ENABLED START #
        """Return the assignedSearchBeamsAdminMode attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedSearchBeamsAdminMode_read

    def read_assignedTimingBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarray.assignedTimingBeamsAdminMode_read) ENABLED START #
        """Return the assignedTimingBeamsAdminMode attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedTimingBeamsAdminMode_read

    def read_assignedVlbiBeamsAdminMode(self):
        # PROTECTED REGION ID(CspSubarray.assignedVlbiBeamsAdminMode_read) ENABLED START #
        """Return the assignedVlbiBeamsAdminMode attribute."""
        return (0,)
        # PROTECTED REGION END #    //  CspSubarray.assignedVlbiBeamsAdminMode_read

    def read_pstBeams(self):
        # PROTECTED REGION ID(CspSubarray.pstBeams_read) ENABLED START #
        """Return the pstBeams attribute."""
        return self.PSTBeams
        # PROTECTED REGION END #    //  CspSubarray.pstBeams_read
        
    def read_scanCmdProgress(self):
        # PROTECTED REGION ID(CspSubarray.scanCmdProgress_read) ENABLED START #
        """Return the scanCmdProgress attribute."""
        return self._cmd_progress['scan']
        # PROTECTED REGION END #    //  CspSubarray.scanCmdProgress_read

    def read_timeoutExpiredFlag(self):
        # PROTECTED REGION ID(CspSubarray.timeoutExpiredFlag_read) ENABLED START #
        """Return the timeoutExpiredFlag attribute."""
        return self._timeout_expired
        # PROTECTED REGION END #    //  CspSubarray.cmdTimeoutExpired_read

    def read_failureRaisedFlag(self):
        # PROTECTED REGION ID(CspSubarray.failureRaisedFlag_read) ENABLED START #
        """Return the failureRaisedFlag attribute."""
        return self._failure_raised
        # PROTECTED REGION END #    //  CspSubarray.cmdAlarmRaised_read

    def read_isCmdInProgress(self):
        # PROTECTED REGION ID(CspSubarray.isCmdInProgress_read) ENABLED START #
        """Return the isCmdInProgress attribute."""
        if all(value == CmdExecState.IDLE for value in self._cmd_execution_state.values()):
            return False
        return True
        # PROTECTED REGION END #    //  CspSubarray.isCmdInProgress_read

    def read_cmdFailureMessage(self):
        # PROTECTED REGION ID(CspSubarray.cmdFailureMessage_read) ENABLED START #
        """Return the cmdFailureMessage attribute."""
        return self._failure_message[self._last_executed_command]
        # PROTECTED REGION END #    //  CspSubarray.cmdFailureMessage_read

    def read_pstOutputLink(self):
        # PROTECTED REGION ID(CspSubarray.pstOutputLink_read) ENABLED START #
        """Return the pstOutputLink attribute."""
        return ''
        # PROTECTED REGION END #    //  CspSubarray.pstOutputLink_read

    def read_listOfDevCompletedTask(self):
        # PROTECTED REGION ID(CspSubarray.listOfDevCompletedTask_read) ENABLED START #
        """Return the listOfDevCompletedTask attribute."""
        self._list_dev_completed_task[self._last_executed_command]
        # PROTECTED REGION END #    //  CspSubarray.listOfDevCompletedTask_read

    def read_configurationID(self):
        # PROTECTED REGION ID(CspSubarray.configurationID_read) ENABLED START #
        """Return the configurationID attribute."""
        return self._configuration_id
        # PROTECTED REGION END #    //  CspSubarray.configurationID_read


    # --------
    # Commands
    # --------

    @command(
        dtype_in='DevString',
        dtype_out='DevVarLongStringArray',
    )
    @DebugIt()
    def Scan(self, argin):
        # PROTECTED REGION ID(CspSubarray.Scan) ENABLED START #
        """
        Starts the scan.

        :param argin: the scanID number.
        :type: 'DevVarStringArray'

        :return: 'DevVarLongStringArray'
        """
        self.logger.info("Call Scan command")
        handler = self.get_command_object("Scan")
        (result_code, message) = handler(argin)
        return [[result_code], [message]] 
        # PROTECTED REGION END #    //  CspSubarray.Scan    
        
        
    @command(
        dtype_in='DevString',
        doc_in="A Json-encoded string with the scan configuration.",
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Configure(self, argin):
        """
        *TANGO Class method*

        Configure a scan for the subarray.\n
        The command can be execuced when the CspSubarray State is *ON* and the \
        ObsState is *IDLE* or *READY*.\n
        If the configuration for the scan is not correct (invalid parameters or invalid JSON)\
        the configuration is not applied and the ObsState of the CspSubarray remains IDLE.

        :param argin: a JSON-encoded string with the parameters to configure a scan.
        :return: None
        :raises:
            tango.DevFailed exception if the CspSubarray ObsState is not valid or if an exception\
            is caught during command execution.
        """
        self.logger.info("Call Configure command")
        handler = self.get_command_object("Configure")
        (result_code, message) = handler(argin)
        return [[result_code], [message]] 

    @command(
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def GoToIdle(self):
        # PROTECTED REGION ID(CspSubarray.GoToIdle) ENABLED START #
        """
        Set the subarray obsState to IDLE.

        :return: None
        """
        self.logger.info("Call GoToIdle command")
        handler = self.get_command_object("GoToIdle")
        (result_code, message) = handler()
        return [[result_code], [message]] 
        # PROTECTED REGION END #    //  CspSubarray.GoToIdle

    @command(
        dtype_out='DevVarLongStringArray',
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Abort(self):
        # PROTECTED REGION ID(CspSubarray.Abort) ENABLED START #
        """
        Change obsState to ABORTED.

        :return:'DevVarLongStringArray'
        """
        self.logger.info("CALL ABORT at time {}".format(time.time()))
        handler = self.get_command_object("Abort")
        (result_code, message) = handler()
        return [[result_code], [message]] 

        # PROTECTED REGION END #    //  CspSubarray.Abort

    '''    
    @command(
        dtype_out='DevVarLongStringArray',
    )
    @DebugIt()
    def EndScan(self):
        # PROTECTED REGION ID(CspSubarray.EndScan) ENABLED START #
        """
        *Class method*
        End the execution of a running scan. After successful execution, the CspSubarray \
        *ObsState* is  IDLE.

        :raises: *tango.DevFailed* if the subarray *obsState* is not SCANNING or if an exception
            is caught during the command execution.
        """
        # PROTECTED REGION END #    //  CspSubarray.EndScan

    @command(
        dtype_in='DevUShort',
        doc_in="The number of SearchBeams Capabilities to assign to the subarray.",
    )
    @DebugIt()
    @ObsStateCheck('addresources')
    @SubarrayRejectCmd('RemoveSearchBeams',
                       'RemoveNumOfSearchBeams',
                       'Configure')
    def AddNumOfSearchBeams(self, argin):
        # PROTECTED REGION ID(CspSubarray.AddNumOfSearchBeams) ENABLED START #
        self._cmd_execution_state['addsearchbeams'] = CmdExecState.RUNNING
        # connect to CspMaster and check if enough beams are available
        # forward the command to PSS
        # wait for PSS number of assigned pipelines == number of required beams
        # get the PSS beams IDs
        # if map exists, retrieve the SearchBeam IDs
        # get the corresponding SearchBeams FQDNs
        # open proxies to devices
        # subscribe SearchBeams attributes
        self._cmd_execution_state['addsearchbeams'] = CmdExecState.RUNNING
        # PROTECTED REGION END #    //  CspSubarray.AddNumOfSearchBeams
        pass
    
    @AdminModeCheck('RemoveNumOfSearchBeams')
    def is_RemoveNumOfSearchBeams_allowed(self):
        return self._is_subarray_composition_allowed()
        
    @command(
        dtype_in='DevUShort',
        doc_in="The number of SearchBeam Capabilities to remove from the \nCSP sub-array.\nAll the assigned SearcBbeams are removed from the CSP sub-array if the\ninput number is equal to the max number of SearchBeam \nCapabilities for the specified Csp sub-array instance (1500 for MID,\n500 for LOW)",
    )
    @DebugIt()
    @ObsStateCheck('removeresources')
    @SubarrayRejectCmd('AddSearchBeams', 'Configure')
    def RemoveNumOfSearchBeams(self, argin):
        # PROTECTED REGION ID(CspSubarray.RemoveNumOfSearchBeams) ENABLED START #
        """
        Remove the specified number of SearchBeam Capabilities from the subarray.

        :param argin: The number of SearchBeam Capabilities to remove from the \
            CSP sub-array. All the assigned SearcBbeams are removed from the CSP \
            sub-array if the input number is equal to the max number of SearchBeam \
            Capabilities for the specified Csp sub-array instance (1500 for MID, 500 for LOW)
        
        :type: 'DevUShort'
        
        :return: None
        """
        self._cmd_execution_state['removesearchbeams'] = CmdExecState.RUNNING
        # PROTECTED REGION END #    //  CspSubarray.RemoveNumOfSearchBeams
    
    @AdminModeCheck('AddTimingBeams')
    def is_AddTimingBeams_allowed(self):
        return self._is_subarray_composition_allowed()
    
    @command(
        dtype_in='DevVarUShortArray',
        doc_in="The list of TimingBeam Capability IDs to assign to the CSP sub-array.",
    )
    @DebugIt()
    @ObsStateCheck('addresources')
    @SubarrayRejectCmd('RemoveTimingBeams', 'Configure')
    def AddTimingBeams(self, argin):
        # PROTECTED REGION ID(CspSubarray.AddTimingBeams) ENABLED START #
        """
        Add the specified TimingBeam Capability IDs to the CSP sub-array.

        :param argin: the list of TimingBeam Capability IDs to assign to the CSP sub-array.
        :type: 'DevVarUShortArray'
        :return: None
        """
        # read the  CSP Master list of CSP TimingBeams addresses
        # get the CSP TimingBeams addresses from IDs
        # map timing beams ids to PST Beams ids
        # write the CSP TimingBeams membership
        # get the PSTBeams addresses
        self._cmd_execution_state['addtimingbeams'] = CmdExecState.RUNNING
        pass
        # PROTECTED REGION END #    //  CspSubarray.AddTimingBeams

    @AdminModeCheck('AddSearchBeams')
    def is_AddSearchBeams_allowed(self):
        return self._is_subarray_composition_allowed()
    
    @command(
        dtype_in='DevVarUShortArray',
        doc_in="The list of SearchBeam Capability IDs to assign to the CSP sub-array.",
    )
    @DebugIt()
    @ObsStateCheck('addresources')
    @SubarrayRejectCmd('RemoveSearchBeams', 'Configure')
    def AddSearchBeams(self, argin):
        # PROTECTED REGION ID(CspSubarray.AddSearchBeams) ENABLED START #
        """
        Add the specified SeachBeam Capability IDs o the CSP sub-array.

        :param argin: the list of SearchBeam Capability IDs to assign to the\
            CSP sub-array.
        :param argintype: 'DevVarUShortArray'
        :return: None
        """
        # read the  CSP Master list of CSP SearchBeams addresses
        # get the CSP SearchBeams addresses from IDs
        # map search beams ids to PSS beams IDs
        # issue the AddSearchBeams command on PSS sub-array
        # wait until PSS list of beams is equal to the sent one
        self._cmd_execution_state['addsearchbeams'] = CmdExecState.RUNNING
        pass
        # PROTECTED REGION END #    //  CspSubarray.AddSearchBeams

    @AdminModeCheck('RemoveSearchBeams')
    def is_RemoveSearchBeams_allowed(self):
        return self._is_subarray_composition_allowed()
    
    @command(
        dtype_in='DevVarUShortArray',
        doc_in="The list of SearchBeam Capability IDs to remove from the CSP sub-array.",
    )
    @DebugIt()
    @ObsStateCheck('removeresources')
    @SubarrayRejectCmd('AddSearchBeams', 'Configure')
    def RemoveSearchBeamsID(self, argin):
        # PROTECTED REGION ID(CspSubarray.RemoveSearchBeamsID) ENABLED START #
        """
        Remove the specified Search Beam Capability IDs from the CSP sub-array.

        :param argin: the list of SearchBeam Capability IDs to remove from the\
            CSP sub-array.
        :type: 'DevVarUShortArray'
        :return:None
        """
        self._cmd_execution_state['removesearchbeams'] = CmdExecState.RUNNING
        pass
        # PROTECTED REGION END #    //  CspSubarray.RemoveSearchBeamsID
        
    @AdminModeCheck('RemoveTimingBeams')
    def is_RemoveTimingBeams_allowed(self):
        return self._is_subarray_composition_allowed()
    
    @command(
        dtype_in='DevVarUShortArray',
        doc_in="The list of Timing Beams IDs to remove from the sub-array.",
    )
    @DebugIt()
    @ObsStateCheck('removeresources')
    @SubarrayRejectCmd('AddTimingBeams', 'Configure')
    def RemoveTimingBeams(self, argin):
        # PROTECTED REGION ID(CspSubarray.RemoveTimingBeams) ENABLED START #
        """
        Remove the specified Timing Beams from the sub-array.

        :param argin: the list of Timing Beams IDs to remove from the sub-array.
        :type: 'DevVarUShortArray'
        :return: None
        """
        self._cmd_execution_state['removetimingbeams'] = CmdExecState.RUNNING
        # PROTECTED REGION END #    //  CspSubarray.RemoveTimingBeams
        
        '''
    
# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarray.main) ENABLED START #
    return run((CspSubarray,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarray.main

if __name__ == '__main__':
    main()
