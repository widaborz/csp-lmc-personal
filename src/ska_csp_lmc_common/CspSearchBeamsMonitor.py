# -*- coding: utf-8 -*-
#
# This file is part of the CspSearchBeamsMonitor project
#
# INAF.SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC Capability monitor for SearchBeams Capabilty devices

CSP.LMC Capability monitor for SearchBeams Capabilty devices
"""
# PROTECTED REGION ID (CspSearchBeamsMonitor.standardlibray_import) ENABLED START #
# Python standard library
# PROTECTED REGION END# //CspSearchBeamsMonitor.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSearchBeamsMonitor.additionnal_import) ENABLED START #
from ska.base.control_model import HealthState, AdminMode, ObsState, ObsMode
from .CspCapabilityMonitor import CspCapabilityMonitor
# PROTECTED REGION END #    //  CspSearchBeamsMonitor.additionnal_import

__all__ = ["CspSearchBeamsMonitor", "main"]


class CspSearchBeamsMonitor(CspCapabilityMonitor):
    """
    CSP.LMC Capability monitor for SearchBeams Capabilty devices

    **Properties:**

    - Device Property
    """
    # PROTECTED REGION ID(CspSearchBeamsMonitor.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSearchBeamsMonitor.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------

    numOfReservedIDs = attribute(
        dtype='DevUShort',
        label="Number of reserved IDs",
        doc="The number of all sub-arrays reserved IDs.",
    )

    reservedIDs = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        label="Number of reserved IDs",
        doc="The number of SearchBeam IDs reserved to each sub-array",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSearchBeamsMonitor."""
        CspCapabilityMonitor.init_device(self)
        # PROTECTED REGION ID(CspSearchBeamsMonitor.init_device) ENABLED START #
        # self._reserved_ids: the number of reserved searchBeams for each subarray
        # dictionary:
        # keys: subarray ID
        # value list of SearchBeams IDs
        self._reserved_ids = {}
        self._num_of_reserved_ids = 0
        # PROTECTED REGION END #    //  CspSearchBeamsMonitor.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSearchBeamsMonitor.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSearchBeamsMonitor.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSearchBeamsMonitor.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSearchBeamsMonitor.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_numOfReservedIDs(self):
        # PROTECTED REGION ID(CspSearchBeamsMonitor.numOfReservedIDs_read) ENABLED START #
        """Return the numOfReservedIDs attribute."""
        return self._num_of_reserved_ids
        # PROTECTED REGION END #    //  CspSearchBeamsMonitor.numOfReservedIDs_read

    def read_reservedIDs(self):
        # PROTECTED REGION ID(CspSearchBeamsMonitor.reservedIDs_read) ENABLED START #
        """Return the reservedIDs attribute."""
        return ('',)
        # PROTECTED REGION END #    //  CspSearchBeamsMonitor.reservedIDs_read


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSearchBeamsMonitor.main) ENABLED START #
    return run((CspSearchBeamsMonitor,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSearchBeamsMonitor.main

if __name__ == '__main__':
    main()
