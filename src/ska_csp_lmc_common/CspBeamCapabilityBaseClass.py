# -*- coding: utf-8 -*-
#
# This file is part of the CspBeamCapabilityBaseClass project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Beam Capability Base Class

Parent class of the CSP Beams Capability
"""
# PROTECTED REGION ID (CspBeamCapabilityBase.standardlibray_import) ENABLED START #
# Python standard library
# PROTECTED REGION END# //CspBeamCapabilityBase.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from ska.base import SKACapability
# Additional import
# PROTECTED REGION ID(CspBeamCapabilityBaseClass.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.additionnal_import

__all__ = ["CspBeamCapabilityBaseClass", "main"]


class CspBeamCapabilityBaseClass(SKACapability):
    """
    Parent class of the CSP Beams Capability

    **Properties:**

    - Device Property
    """
    # PROTECTED REGION ID(CspBeamCapabilityBaseClass.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.class_variable

    # -----------------
    # Device Properties
    # -----------------
  # ----------
    # Attributes
    # ----------

    subarrayMembership = attribute(
        dtype='DevUShort',
        label="The Beam Capability subarray affiliation.",
        doc="The subarray ID the CSP Beam Capability belongs to.",
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspBeamCapabilityBaseClass."""
        SKACapability.init_device(self)
        # PROTECTED REGION ID(CspBeamCapabilityBaseClass.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspBeamCapabilityBaseClass.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspBeamCapabilityBaseClass.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_subarrayMembership(self):
        # PROTECTED REGION ID(CspBeamCapabilityBaseClass.subarrayMembership_read) ENABLED START #
        """Return the subarrayMembership attribute."""
        return 0
        # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.subarrayMembership_read




# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspBeamCapabilityBaseClass.main) ENABLED START #
    return run((CspBeamCapabilityBaseClass,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspBeamCapabilityBaseClass.main

if __name__ == '__main__':
    main()
