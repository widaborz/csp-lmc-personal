# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarrayProcModeVlbi project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Subarray VLBI Capability

The class exposes parameters and commands required
for monitor and control of the Procesing Mode VLBI in a
single subarray.
It is used as base for developement of LOW and MID
specific capabilities.
"""
# PROTECTED REGION ID (CspSubarrayProcModeVlbi.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os
# PROTECTED REGION END# //CspSubarrayIProcModeVlbi.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSubarrayProcModeVlbi.additionnal_import) ENABLED START #
from .CspSubarrayInherentCapability import CspSubarrayInherentCapability
# PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.additionnal_import

__all__ = ["CspSubarrayProcModeVlbi", "main"]


class CspSubarrayProcModeVlbi(CspSubarrayInherentCapability):
    """
    The class exposes parameters and commands required
    for monitor and control of the Procesing Mode VLBI in a
    single subarray.
    It is used as base for developement of LOW and MID
    specific capabilities.

    **Properties:**

    - Device Property
     """
    # PROTECTED REGION ID(CspSubarrayProcModeVlbi.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------

















    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSubarrayProcModeVlbi."""
        CspSubarrayInherentCapability.init_device(self)
        # PROTECTED REGION ID(CspSubarrayProcModeVlbi.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarrayProcModeVlbi.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarrayProcModeVlbi.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarrayProcModeVlbi.main) ENABLED START #
    return run((CspSubarrayProcModeVlbi,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarrayProcModeVlbi.main

if __name__ == '__main__':
    main()
