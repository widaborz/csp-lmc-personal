# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarrayProcModeCorrelation project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Subarray Processing Mode Correlation Capability

The class exposes parameters nd commands required for monitor and
control of the correlator functions for a single subarray.
It is used as a base for the development of LOW and MID specific
capabilities.
"""
# PROTECTED REGION ID (CspSubarrayProcModeCorrelation.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os
# PROTECTED REGION END# //CspSubarrayIProcModeCorrelation.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(CspSubarrayProcModeCorrelation.additionnal_import) ENABLED START #
from .CspSubarrayInherentCapability import CspSubarrayInherentCapability
# PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.additionnal_import

__all__ = ["CspSubarrayProcModeCorrelation", "main"]


class CspSubarrayProcModeCorrelation(CspSubarrayInherentCapability):
    """
    The class exposes parameters nd commands required for monitor and
    control of the correlator functions for a single subarray.
    It is used as a base for the development of LOW and MID specific
    capabilities.

    **Properties:**

    - Device Property
    """

    # PROTECTED REGION ID(CspSubarrayProcModeCorrelation.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------


    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSubarrayProcModeCorrelation."""
        CspSubarrayInherentCapability.init_device(self)
        # PROTECTED REGION ID(CspSubarrayProcModeCorrelation.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarrayProcModeCorrelation.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarrayProcModeCorrelation.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarrayProcModeCorrelation.main) ENABLED START #
    return run((CspSubarrayProcModeCorrelation,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarrayProcModeCorrelation.main

if __name__ == '__main__':
    main()
