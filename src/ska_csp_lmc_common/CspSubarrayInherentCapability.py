# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarrayInherentCapability project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP Subarray Inherent Capability Base Class

Parent class for the CSP Subarray Processing Mode Capability devices.
Used as a base for developement of CSP Inherent Capabilities.
"""
# PROTECTED REGION ID (CspSubarrayInherentCapability.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os
# PROTECTED REGION END# //CspSubarrayInherentCapability.standardlibray_import
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
#from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType 
# Additional import
# PROTECTED REGION ID(CspSubarrayInherentCapability.additionnal_import) ENABLED START #
#
from ska.base import SKACapability
from ska.base import utils

#
# PROTECTED REGION END #    //  CspSubarrayInherentCapability.additionnal_import

__all__ = ["CspSubarrayInherentCapability", "main"]


class CspSubarrayInherentCapability(SKACapability):
    """
    Parent class for the CSP Subarray Processing Mode Capability devices.
    Used as a base for developement of CSP Inherent Capabilities.

    **Properties:**

    - Device Property 
    """
    # PROTECTED REGION ID(CspSubarrayInherentCapability.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  CspSubarrayInherentCapability.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------


    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspSubarrayInherentCapability."""
        SKACapability.init_device(self)
        # PROTECTED REGION ID(CspSubarrayInherentCapability.init_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayInherentCapability.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspSubarrayInherentCapability.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayInherentCapability.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspSubarrayInherentCapability.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  CspSubarrayInherentCapability.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------
    @command(
    )
    @DebugIt()
    def On(self):
        # PROTECTED REGION ID(CspSubarrayInherentCapability.On) ENABLED START #
        """
        
            Enable the CSP Subarray inherent Capability setting its State to ON.
            State at initialization is OFF

        :return:None
        """
        pass
        # PROTECTED REGION END #    //  CspSubarrayInherentCapability.On

    @command(
    )
    @DebugIt()
    def Off(self):
        # PROTECTED REGION ID(CspSubarrayInherentCapability.Off) ENABLED START #
        """
        
            Disable the CspSubarray inherent Capability
            setting the State to OFF.

        :return:None
        """
        pass
        # PROTECTED REGION END #    //  CspSubarrayInherentCapability.Off

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspSubarrayInherentCapability.main) ENABLED START #
    return run((CspSubarrayInherentCapability,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspSubarrayInherentCapability.main

if __name__ == '__main__':
    main()
