# -*- coding: utf-8 -*-
#
# This file is part of the CspMaster project
#
# INAF - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" CSP.LMC Common CspMaster

CSP.LMC Common Class for the CSPMaster TANGO Device.
"""

# PROTECTED REGION ID (CspMaster.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os
import time
import threading
from collections import defaultdict
# PROTECTED REGION END# //CspMaster.standardlibray_import

# tango imports 
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, EventType, DevState
from tango import AttrWriteType, DeviceProxy
# Additional import
# PROTECTED REGION ID(CspMaster.additional_import) ENABLED START #
#
from ska.base import SKAMaster
from ska.base import utils
from ska.base.commands import ResultCode
from ska.base.control_model import HealthState, AdminMode, ObsState, LoggingLevel
from .utils.decorators import AdminModeCheck, CmdInputArgsCheck
from .utils.cspcommons import CmdExecState
from . import release
# PROTECTED REGION END# //CspMaster.additional_import

__all__ = ["CspMaster", "main"]

class CspMaster(SKAMaster):
    """
    CSP.LMC Common Class for the CSPMaster TANGO Device.

    **Properties:**

    - Device Property
    
        CspCbf
            - FQDN of the  CBF Sub-element Master
            - Type:'DevString'
    
        CspPss
            - TANGO FQDN of the  PSS Sub-element Master
            - Type:'DevString'
    
        CspPst
            - TANGO FQDN of the  PST Sub-element Master
            - Type:'DevString'
    
        CspSubarrays
            - TANGO FQDN of the  CSP.LMC Subarrays
            - Type:'DevVarStringArray'
    
        SearchBeamsMonitor
            - TANGO Device to monitor the CSP SearchBeams Capability devices.
            - Type:'DevString'
    
        TimingBeamsMonitor
            - TANGO Device to monitor the CSP TimingBeams Capability devices.
            - Type:'DevString'
    
        VlbiBeamsMonitor
            - TANGO Device to monitor the CSP VlbiBeams Capability devices.
            - Type:'DevString'
    
    """
    # PROTECTED REGION ID(CspMaster.class_variable) ENABLED START #
    
    # PROTECTED REGION END #    //  CspMaster.class_variable
    # PROTECTED REGION ID(CspMaster.class_protected_methods) ENABLED START #
    
    # !! NOTE !!: 
    # In methods and attributes of the class:
    # 'se' prefix stands for 'sub-element
    # 'cb' suffix stands for 'callback'
    #----------------
    # Event Callback functions
    # ---------------
    def _se_scm_change_event_cb(self, evt):
        """
        Class callback function.
        Retrieve the values of the sub-element SCM attributes subscribed
        for change event at device initialization.

        :param evt: The event data

        :return: None
        """
        dev_name = evt.device.dev_name()
        if not evt.err:
            try:
                if dev_name in self._se_fqdn:
                    if evt.attr_value.name.lower() == "state":
                        self.logger.debug("{}: received event on {} value {}".format(dev_name,
                                                                         evt.attr_value.name,
                                                                         evt.attr_value.value))
                        self._se_state[dev_name] = evt.attr_value.value
                        
                    elif evt.attr_value.name.lower() == "healthstate":
                        self.logger.debug("{}: received event on {} value {}".format(dev_name,
                                                                         evt.attr_value.name,
                                                                         evt.attr_value.value))
                        self._se_health_state[dev_name] = evt.attr_value.value
                    elif evt.attr_value.name.lower() == "adminmode":
                        self.logger.debug("device: {} adminMode value {}".format(dev_name,evt.attr_value.value ))
                        self._se_admin_mode[dev_name] = evt.attr_value.value
                    else:
                        log_msg = ("Attribute {} not still "
                                   "handled".format(evt.attr_name))
                        self.logger.warning(log_msg)
                else:
                    log_msg = ("Unexpected change event for"
                               " attribute: {}".format(str(evt.attr_name)))
                    self.logger.warning(log_msg)
                    return

                log_msg = "New value for {} is {}".format(str(evt.attr_name),
                                                          str(evt.attr_value.value))
                self.logger.info(log_msg)
                # update CSP global state
                # healthState and State are updated accordingly to the updated values of
                # sub-elements healthState, State and adminMode
                if evt.attr_value.name.lower() in ["state", "healthstate", "adminmode"]:
                    self._update_csp_state()
            except tango.DevFailed as df:
                self.logger.error(str(df.args[0].desc))
            except Exception as except_occurred:
                self.logger.error(str(except_occurred))
        else:
            for item in evt.errors:
                # API_EventTimeout: if sub-element device not reachable it transitions
                # to UNKNOWN state.
                if item.reason == "API_EventTimeout":
                    # only if the device is ONLINE/MAINTENANCE, its State is set to 
                    # UNKNOWN when there is a timeout on connection, otherwise its
                    # State should be reported always as DISABLE
                    if self._se_admin_mode[dev_name] in [AdminMode.ONLINE,
                                                         AdminMode.MAINTENANCE]:
                        self._se_state[dev_name] = tango.DevState.UNKNOWN
                        self._se_health_state[dev_name] = HealthState.UNKNOWN
                    # if the device is executing the shutdown the state is set to
                    # OFF
                    if self._se_cmd_execution_state[dev_name]['off'] == CmdExecState.RUNNING:
                        self._se_state[dev_name] = tango.DevState.OFF
                    # update the State and healthState of the CSP Element
                    self._update_csp_state()
                log_msg = item.reason + ": on attribute " + str(evt.attr_name)
                self.logger.warning(log_msg)
    
    def _attributes_change_evt_cb(self, evt):
        """
        Class callback function.
        Retrieve the value of the sub-element xxxCommandProgress and xxxcmdTimeoutExpired
        attributes subscribed for change event when a long-running command is issued
        on the sub-element device.

        :param evt: The event data

        :return: None
        """
        dev_name = evt.device.dev_name()
        if not evt.err:
            try:
                if "commandprogress" == evt.attr_value.name.lower()[-15:]:
                    if dev_name in self._se_fqdn:
                        # get the command name (on/off/standby) from the attribute name 
                        # (onCommandProgress/offCommandProgress/standbyCommandProgress))
                        # removing the last 15 chars
                        cmd_name = evt.attr_value.name[:-15]
                        self._se_cmd_progress[dev_name][cmd_name] = evt.attr_value.value
                elif evt.attr_value.name.lower() == "oncmdtimeoutexpired":
                    self._se_timeout_expired[dev_name]['on'] = True
                elif evt.attr_value.name.lower() == "offcmdtimeoutexpired":
                    self._se_timeout_expired[dev_name]['off'] = True
                elif evt.attr_value.name.lower() == "standbycmdtimeoutexpired":
                    self._se_timeout_expired[dev_name]['standby'] = True
                else:
                    log_msg = ("Unexpected change event for"
                               " attribute: {}".format(str(evt.attr_name)))
                    self.logger.warning(log_msg)
                    return

                log_msg = "New value for {} is {}".format(str(evt.attr_name),
                                                          str(evt.attr_value.value))
                self.logger.info(log_msg)
            except tango.DevFailed as df:
                self.logger.error(str(df.args[0].desc))
            except Exception as except_occurred:
                self.logger.error(str(except_occurred))
        else:
            for item in evt.errors:
                log_msg = item.reason + ": on attribute " + str(evt.attr_name)
                self.logger.warning(log_msg)
              
    def _cmd_ended_cb(self, evt):
        """
        Callback function immediately executed when the asynchronous invoked
        command returns.

        :param evt: a CmdDoneEvent object. This class is used to pass data
            to the callback method in asynchronous callback model for command
            execution.
        :type: CmdDoneEvent object
            It has the following members:
                - device     : (DeviceProxy) The DeviceProxy object on which the
                               call was executed.
                - cmd_name   : (str) The command name
                - argout_raw : (DeviceData) The command argout
                - argout     : The command argout
                - err        : (bool) A boolean flag set to true if the command
                               failed. False otherwise
                - errors     : (sequence<DevError>) The error stack
                - ext
        :return: none
        """
        # NOTE:if we try to access to evt.cmd_name or other paramters, sometime
        # the callback crashes with this error:
        # terminate called after throwing an instance of 'boost::python::error_already_set'
        try:
            # Can happen evt empty??
            if evt:
                if not evt.err:
                    msg = "Device {} is processing command {}".format(evt.device,
                                                                      evt.cmd_name)
                    self.logger.info(msg)
                else:
                    msg = "Error!!Command {} ended on device {}.\n".format(evt.cmd_name,
                                                                           evt.device.dev_name())
                    msg += " Desc: {}".format(evt.errors[0].desc)
                    self.logger.error(msg)
                    self._se_cmd_execution_state[evt.device.dev_name()][evt.cmd_name.lower()] = CmdExecState.FAILED
                    self._failure_message[evt.cmd_name] = msg
                    self.logger.info("_cmd_ended_cb _cmd_execution_state:{}".format(self._cmd_execution_state))
                    # obsState and obsMode values take on the CbfSubarray's values via
                    # the subscribe/publish mechanism
            else:
                self.logger.error("cmd_ended callback: evt is empty!!")
        except tango.DevFailed as df:
            msg = ("CommandCallback cmd_ended failure - desc: {}"
                   " reason: {}".format(df.args[0].desc, df.args[0].reason))
            self.logger.error(msg)
        except Exception as ex:
            msg = "CommandCallBack cmd_ended general exception: {}".format(str(ex))
            self.logger.error(msg)
    
    # ---------------
    # Class methods
    # ---------------
    
    def _update_csp_state(self):
        """
        Class method.
        Retrieve the *State* attribute value of the CBF sub-element and build up
        the CSP global state: only if CBF sub-element is present CSP can work.
        The *State* of of PSS and PST sub-elements (if ONLINE/MAINTENANCE) only
        contributes to determine the CSP *healthState*

        :param: None

        :return: None
        """
        self._update_csp_health_state()
        if all(value == CmdExecState.IDLE for value in self._cmd_execution_state.values()) or (not any(self._cmd_execution_state)):
            self.logger.debug("_cmd_execution_state:{}".format(self._cmd_execution_state.items()))
            self.set_state(self._se_state[self.CspCbf])
            self.set_change_event('State', self._se_state[self.CspCbf])
            self.set_archive_event('State', self._se_state[self.CspCbf])
        if self._admin_mode in [AdminMode.OFFLINE, AdminMode.NOT_FITTED, AdminMode.RESERVED]:
            self.set_state(tango.DevState.DISABLE)
        self.logger.debug("CspState: {}".format(self.get_state()))

    def _update_csp_health_state(self):
        """
        Class method.
        Retrieve the *healthState* and *adminMode* attributes of the CSP
        sub-elements and aggregate them to build up the CSP *healthState*.
        
        :param: None

        :return: None
        """

        # The whole CSP HealthState is OK only if:
        # - all sub-elements with adminMode ON-LINE or MAINTENACE are ON AND
        # - each sub-element HealthState is OK
        
        # default value to DEGRADED
        self._health_state = HealthState.DEGRADED
        # build the list of all the sub-elements ONLINE/MAINTENANCE
        admin_fqdn = [fqdn for fqdn, admin_value in self._se_admin_mode.items()
                      if admin_value in [AdminMode.ONLINE, AdminMode.MAINTENANCE]]
        # build the list of sub-elements with State ON/STANDBY and ONLINE/MAINTENANCE
        state_fqdn =  [fqdn for fqdn in admin_fqdn if self._se_state[fqdn] in [tango.DevState.ON,
                                                                               tango.DevState.STANDBY]]
        # build the list with the healthState of ONLINE/MAINTENANCE devices 
        health_list = [self._se_health_state[fqdn] for fqdn in state_fqdn]
        
        if self.CspCbf in admin_fqdn:
            if all(value == HealthState.OK for value in health_list):
                self._health_state = HealthState.OK
            elif self._se_health_state[self.CspCbf] in [HealthState.FAILED,
                                                      HealthState.UNKNOWN,
                                                      HealthState.DEGRADED]:
                self._health_state = self._se_health_state[self.CspCbf]
        else:
            # if CBF is not ONLINE/MAINTENANCE ....
            self._health_state = self._se_health_state[self.CspCbf]
        return
    
    def _connect_to_subelements (self):
        """
        Class method.
        Establish a *stateless* connection with each CSP sub-element and
        subscribe for the sub-element SCM attributes.
        
        :return: None
        """
        for fqdn in self._se_fqdn:
            try:
                # DeviceProxy to sub-elements
                log_msg = "Trying connection to" + str(fqdn) + " device"
                self.logger.info(log_msg)
                device_proxy = DeviceProxy(fqdn)
                #device_proxy.ping()
                # Note: ping() method is not called. The DeviceProxy is initialized even if the
                # sub-element is not running (but defined into the TANGO DB!). 
                # The connection with a sub-element is establish as soon as the corresponding
                # Master device starts. 
                
                # store the sub-element proxies
                self._se_proxies[fqdn] = device_proxy
                # subscription of SCM attributes (State, healthState and adminMode).
                # Note: subscription is performed also for devices not ONLINE/MAINTENANCE.
                # In this way the CspMaster is able to detect a change in the admin value.
                
                
                ev_id = device_proxy.subscribe_event("adminMode",
                                                     EventType.CHANGE_EVENT,
                                                     self._se_scm_change_event_cb,
                                                     stateless=True)
                self._se_event_id[fqdn]['adminMode'] = ev_id
                
                ev_id = device_proxy.subscribe_event("State",
                                                     EventType.CHANGE_EVENT,
                                                     self._se_scm_change_event_cb,
                                                     stateless=True)
                self._se_event_id[fqdn]['state'] = ev_id

                ev_id = device_proxy.subscribe_event("healthState",
                                                     EventType.CHANGE_EVENT,
                                                     self._se_scm_change_event_cb,
                                                     stateless=True)
                self._se_event_id[fqdn]['healthState'] = ev_id
            except KeyError as key_err:
                log_msg = ("No key {} found".format(str(key_err)))
                self.logger.warning(log_msg)
            except tango.DevFailed as df:
                #for item in df.args:
                log_msg = ("Failure in connection to {}"
                           " device: {}".format(str(fqdn), str(df.args[0].desc)))
                self.logger.error(log_msg)
                
    def _is_device_running(self, device_fqdn, proxy_list=None):
        """
        *Class method.*

        Check if a TANGO device is exported in the TANGO DB (i.e its TANGO 
        device server is running).
        
        :param: device_fqdn : the FQDN of the sub-element
        :type: `DevString`
        :param proxy_list: the list with the proxies for the device
        :type: list of TANGO proxies
        :return: True if the connection with the subarray is established,
                False otherwise
        """
        if not proxy_list:
            return False
        try:
            proxy = proxy_list[device_fqdn]
            # ping the device to control if is alive
            proxy.ping()
            return True
        except KeyError as key_err:
            # Raised when a mapping (dictionary) key is not found in the set
            # of existing keys.
            # no proxy registered for the subelement device
            msg = "Can't retrieve proxy for device {}".format(key_err)
            self.logger.error(msg)
            return False
        except tango.DevFailed:
            return False
         
    def _issue_power_command(self, device_list, **args_dict):
        """
        Target function called when the power command threads start.
        The *On*, *Standby* and *Off* methods issue the command on the sub-element
        devices in a separate thread.
        The target function accepts as input arguments the command to execute and
        the list of devices to command.
        
        :param  device_list: tuple with the FQDN of the sub-element devices
                args_dict: dictionary with information about the command to execute.
                           The dictionary keys are:
                            - cmd_name : the TANGO command name to execute
                            - attr_name: the corresponding command progress attribute to subscribe
                            - dev_state: the expected end state for the device transition
        """
        #TODO: order the list alphabetically so that the CBF is always the first element to start
        # the TANGO command to execute
        tango_cmd_name = 0
        # the sub-element device state after successful transition
        dev_successful_state = 0
        try:
            tango_cmd_name = args_dict['cmd_name']
            dev_successful_state = args_dict['dev_state']
        except KeyError as key_err:
            self.logger.warning("No key: {}".format(str(key_err)))
            # reset the CSP and CSP sub-elements command execution
            # state flags
            self._se_cmd_execution_state.clear()
            self._cmd_execution_state.clear()
            return
        # tango_cmd_name: is the TANGO command name with the capital letter
        # In the dictionary keys, is generally used the command name in lower letters
        cmd_name = tango_cmd_name.lower()
        self.logger.info("cmd_name: {} dev_state: {}".format(cmd_name,
                                                  dev_successful_state))
        num_of_failed_device = 0
        self._num_dev_completed_task[cmd_name] = 0
        self._list_dev_completed_task[cmd_name] = []
        self._cmd_progress[cmd_name] = 0
        self._cmd_duration_measured[cmd_name] = 0
        self._failure_message[cmd_name] = ''
        # sub-element command execution measured time
        se_cmd_duration_measured = defaultdict(lambda:defaultdict(lambda:0))
        # loop on the devices and power-on them sequentially
        for device in device_list:
            se_cmd_duration_measured[device][cmd_name] = 0
            self._se_cmd_progress[device][cmd_name] = 0
            
            device_proxy = self._se_proxies[device] 
            self.logger.debug("Issue asynch command {} on device {}:".format(cmd_name, device))
               
            # Note: when the command ends on the sub-element, the _cmd_ended_cb callback
            # is called. This callback sets the sub-element execution state to FAULT if
            # an exception is caught during the execution on the sub-element.
            #
            # !!Note!!: 
            # If the command is issued while the same command is already running on a
            # sub-element, the sub-element should return without throwing any exception
            # (see "SKA System Control Guidelines").
            # In this case the current method enters the while loop and the execution of the
            # sub-element command is tracked in the right way.
            try:
                # 04-11-2020: removed the registration of the cmd_ended_cb callback. It has
                # been observed a wrong behavior when the callback is thrown after the end of the
                # thread. This happens when the same command is executed twice on the CbfMaster.
                # In this case the CbfMaster throws an exception to signal that the device is
                # already in that state, the check on the device state passes with success,
                # the self._cmd_execution_state is set to 
                # IDLE and the thread exit. After it is received the callback message with the
                # error message generated by the CbfMaster but the 
                # self._cmd_execution_state results RUNNING and the device stucks 
                if device_proxy.state() == dev_successful_state:
                    continue
                device_proxy.command_inout_asynch(tango_cmd_name, self._cmd_ended_cb)
            except tango.DevFailed as df:
                # It should not happen! Verify
                msg = "Failure reason: {} Desc: {}".format(str(df.args[0].reason), str(df.args[0].desc))
                self.logger.warning(msg)
                self._se_cmd_execution_state[device][cmd_name] = CmdExecState.FAILED
                self._failure_message[cmd_name] += msg
                num_of_failed_device += 1
                # skip to next device
                continue
            # set the sub-element command execution flag
            self._se_cmd_execution_state[device][cmd_name] = CmdExecState.RUNNING
            # register the starting time for the command
            self._se_cmd_starting_time[device] = time.time() 
            # loop on the device until the State changes to ON or a timeout or
            # a failure detection occurred
            self.logger.debug("Device {} State {} expected value {}".format(device, self._se_state[device], dev_successful_state))
            command_progress = self._cmd_progress[cmd_name]
            while True:
                if self._se_state[device] == dev_successful_state:
                    self.logger.info("Command {} ended with success on device {}.".format(cmd_name,
                                                                                          device))
                    # update the list and number of device that completed the task
                    self._num_dev_completed_task[cmd_name]  += 1
                    self._list_dev_completed_task[cmd_name].append(device)
                    # reset the value of the attribute reporting the execution state of
                    # the command
                    self._se_cmd_execution_state[device][cmd_name] = CmdExecState.IDLE
                    
                    self._se_cmd_progress[device][cmd_name] = 100
                    # command success: exit from the wait loop and issue the command
                    # on the next device in the list
                    break
                # check for other sub-element FAULT values
                if self._se_state[device] == tango.DevState.FAULT:
                    self._se_cmd_execution_state[device][cmd_name] = CmdExecState.FAILED
                    self._failure_message[cmd_name] += ("Device {} is {}".format(device, self.get_status()))
                    self.logger.warning(self._failure_message[cmd_name]) 
                    num_of_failed_device += 1
                    break
                # check if sub-element command ended throwing an exception: in this case the
                # 'cmd_ended_cb' callback is invoked. The callback log the exception and
                # sets the sub-element execution state to FAILED.
                # NOTE: as per the "SKA Control Guidelines", a sub-element shall not throw an
                # exception if the sub-element is already in the requested final state or if the
                # command is already running.
                # A different implementation could cause a wrong behavior of the current function.
                if self._se_cmd_execution_state[device][cmd_name] == CmdExecState.FAILED:
                    # execution ended for this sub-element, skip to the next one
                    num_of_failed_device += 1
                    # skip to next device
                    break
                # check for timeout event. A timeout event can be detected in two ways:
                # 1- the sub-element implements the 'onTimeoutExpired' attribute configured
                #    for change event
                # 2- the CspMaster periodically checks the time elapsed from the start
                #    of the command: if the value is greater than the sub-element expected time
                #    for command execution, the sub-element command execution state is set
                #    to TIMEOUT
                # Note: the second check, can be useful if the timeout event is not received
                # (for example for a temporary connection timeout)
                elapsed_time = time.time() - self._se_cmd_starting_time[device]
                if (elapsed_time > self._se_cmd_duration_expected[device][cmd_name] or
                    self._se_cmd_execution_state[device][cmd_name] == CmdExecState.TIMEOUT):
                    msg = ("Timeout executing {} command  on device {}".format(cmd_name, device))
                    self.logger.warning(msg)
                    self._se_cmd_execution_state[device][cmd_name] = CmdExecState.TIMEOUT       
                    num_of_failed_device += 1
                    # if the CBF command timeout expires, the CSP power-on is stopped
                    # TODO: verify if this behavior conflicts with ICD
                    self.logger.debug("elapsed_time:{} device {}".format(elapsed_time, device))
                    if device == self.CspCbf:
                        self.logger.error("CBF Timeout during command {} "
                                          "execution!!! Exit".format(cmd_name))
                        self._timeout_expired[cmd_name] = True
                        self._se_cmd_execution_state[device][cmd_name] = CmdExecState.IDLE
                        with self._cmd_exec_state_lock:
                            self._cmd_execution_state[cmd_name] = CmdExecState.IDLE
                        return
                    # timeout on the sub-element, skip to the next device
                    break
                time.sleep(0.1)
                # update the progress counter inside the loop taking into account the number of devices
                # executing the command
                self._cmd_progress[cmd_name] = command_progress + self._se_cmd_progress[device][cmd_name]/len(device_list)
            # end of the while loop
            # calculate the real execution time for the command
            se_cmd_duration_measured[device][cmd_name] = (time.time() 
                                                                  - self._se_cmd_starting_time[device])
            self.logger.info("measured duration:{}".format(se_cmd_duration_measured[device][cmd_name]))
            self._cmd_duration_measured[cmd_name] += se_cmd_duration_measured[device][cmd_name]
            # update the progress counter at the end of the loop 
            self._cmd_progress[cmd_name] = command_progress + (self._se_cmd_progress[device][cmd_name]/len(device_list))
            if len(device_list) ==  self._num_dev_completed_task[cmd_name] + num_of_failed_device:
                self.logger.info("All devices have been handled!")
                # end of the command: the command has been issued on all the sub-element devices
                # reset the execution flag for the CSP
                break   
        # out of the for loop
        self._last_executed_command = cmd_name
        # if one or more sub-elements goes in timeout or failure, set the CSP
        # corresponding attribute
        for device in device_list:
            # reset the CSP sub-element command execution flag
            if self._se_cmd_execution_state[device][cmd_name] == CmdExecState.TIMEOUT:
                # set the CSP timeout flag
                self._timeout_expired[cmd_name] = True
            if self._se_cmd_execution_state[device][cmd_name] == CmdExecState.FAILED:
                # set the CSP timeout flag
                self._failure_raised[cmd_name] = True
            # reset the CSP sub-element command execution flag
            self._se_cmd_execution_state[device][cmd_name] = CmdExecState.IDLE
        # reset the CSP command execution flag
        with self._cmd_exec_state_lock:
            self._cmd_execution_state[cmd_name] = CmdExecState.IDLE

    def _switch_subarrays(self, tango_cmd_name):
        """
        Helper method to execute the On[Off] command on the CSP subarrays.

        :param tangp_cmd_name: The command to execute
        :type tamgo_cmd_name: string
        :return: None
        """
        subarray_action = tango_cmd_name.lower()
        if subarray_action not in ['on', 'off']:
            self.logger.warning(f"Invalid command {subarray_action} to issue on CSP subarray")
            return
        try:
            subarray_group = tango.Group("CSPSubarray")
            for subarray in self.CspSubarrays:
                subarray_group.add(subarray)
            self.logger.info("Going to switch-{} the subarrays".format(subarray_action))
            answers = subarray_group.command_inout(subarray_action)
            for reply in answers:
                if reply.has_failed():
                    self.logger.info("stack: {}".format(len(reply.get_err_stack())))
                    for err in reply.get_err_stack():
                        self.logger.info("err {} reason {}".format(err.desc, err.reason) )
                    self.logger.warning("Subarray {} failed executing command {}".format(reply.dev_name(),
                                                                                        subarray_action))
                else:
                    (result_code, msg) = reply.get_data()
                    if result_code == ResultCode.FAILED:
                        self.logger.error(f"Subarray {reply.dev_name()}: {msg[0]}")
                    elif result_code == ResultCode.OK:
                        self.logger.info(f"Subarray {reply.dev_name()}: {msg[0]}")
        except tango.DevFailed as df: 
            log_msg = (f"Failure in command {subarray_action} "
                         "for device {str(fqdn)}: {str(df.args[0].reason)}")
            self.logger.error(log_msg)
        except Exception:
            self.logger.error("command {} failed on subarray".format(subarray_action))
    
    def _enable_subarrays(self):
        return self._switch_subarrays('On')

    def _disable_subarrays(self):
        return self._switch_subarrays('Off')

    def _se_write_adminMode(self, value, device_fqdn):
        """
        *Class method.*

        Set the administrative mode of the specified device.
        :param: subelement_name : the FQDN of the sub-element
        :type: `DevString`
        :return: True if the connection with the subarray is established,
                False otherwise
        """
        # check if the device is exported in the TANGO DB (that is the server is running)

        # 03-19-20 
        # NOTE: this check can be removed because the check on valid values
        # for attributes of DevEnum type is done by TANGO
        if value not in [AdminMode.ONLINE, AdminMode.MAINTENANCE, AdminMode.OFFLINE,
                         AdminMode.NOT_FITTED, AdminMode.RESERVED]:
            msg = "Invalid {} value for adminMode attribute".format(value)
            tango.Except.throw_exception("Command failed",
                                             msg,
                                             "write_adminMode",
                                             tango.ErrSeverity.ERR)
        # check if the sub-element administrative mode has already the requested value
        if value == self._se_admin_mode[device_fqdn]:
            return   
        if self._is_device_running(device_fqdn, self._se_proxies):
            try:
                proxy = self._se_proxies[device_fqdn]
                proxy.write_attribute_asynch('adminMode', value)
                # TODO: add checks for timeout/errors
            except KeyError as key_err:
                msg = "Can't retrieve the information of key {}".format(key_err)
                self.logger.error(msg)
                tango.Except.throw_exception("DevFailed excpetion", msg,
                                             "write adminMode", tango.ErrSeverity.ERR)
    def _connect_capabilities_monitor(self):
        """
        Protected Class method.

        Establish connecton with the TANGO devices performing the monitor of the CSP
        Element Capabilities.
        """
        # build the list with the Capability monitor devices
        for fqdn in self._capability_monitor_fqdn:
            try:
                self._capability_monitor_proxy[fqdn] = DeviceProxy(fqdn)
            except tango.DevFailed as tango_err:
                self.logger.warning(tango_err.args[0].desc)
            
                
# PROTECTED REGION END #    //  CspMaster.class_protected_methods
    # -----------------
    # Device Properties
    # -----------------

    CspCbf = device_property(
        dtype='DevString',
    )

    CspPss = device_property(
        dtype='DevString',
    )

    CspPst = device_property(
        dtype='DevString',
    )

    CspSubarrays = device_property(
        dtype='DevVarStringArray',
    )

    SearchBeamsMonitor = device_property(
        dtype='DevString',
    )

    TimingBeamsMonitor = device_property(
        dtype='DevString',
    )

    VlbiBeamsMonitor = device_property(
        dtype='DevString',
    )

    # ----------
    # Attributes
    # ----------

    adminMode = attribute(
        dtype='DevEnum',
        access=AttrWriteType.READ_WRITE,
        polling_period=1000,
        memorized=True,
        doc= ("The admin mode reported for this device. It may interpret the" 
              "current device condition and condition of all managed devices"
              "to set this. Most possibly an aggregate attribute."),
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED", ],

    )

    onCommandProgress = attribute(
        dtype='DevUShort',
        label="Progress percentage for the On command",
        polling_period=3000,
        abs_change=10,
        max_value=100,
        min_value=0,
        doc=("Percentage progress implemented for commands that  result in state/mode"
             "transitions for a large \nnumber of components and/or are executed in "
             "stages (e.g power up, power down)"),
    )

    offCommandProgress = attribute(
        dtype='DevUShort',
        label="Progress percentage for the Off command",
        polling_period=3000,
        abs_change=10,
        max_value=100,
        min_value=0,
        doc=("Percentage progress implemented for commands that  result in state/mode"
             " transitions for a large \nnumber of components and/or are executed in "
             "stages (e.g power up, power down)"),
    )

    standbyCommandProgress = attribute(
        dtype='DevUShort',
        label="Progress percentage for the Standby command",
        polling_period=3000,
        abs_change=10,
        max_value=100,
        min_value=0,
        doc=("Percentage progress implemented for commands that  result in state/mode "
            "transitions for a large \nnumber of components and/or are executed in "
            "stages (e.g power up, power down)"),
    )

    onCmdDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="Expected duration (sec) of the On command execution",
        abs_change=0,
        max_value=100,
        min_value=0,
        memorized=True,
        doc=("Set/Report the duration expected (in sec.) for the On command execution"),
    )

    offCmdDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="Expected duration (sec) of the Off command",
        abs_change=0,
        max_value=100,
        min_value=0,
        memorized=True,
        doc=("Set/Report the duration expected (in sec.) for the Off command execution"),
    )

    standbyCmdDurationExpected = attribute(
        dtype='DevUShort',
        access=AttrWriteType.READ_WRITE,
        label="Expected duration (sec) of the Standby command",
        abs_change=0,
        max_value=100,
        min_value=0,
        memorized=True,
        doc=("Set/Report the duration expected (in sec.) for the Standby command"),
    )

    onCmdDurationMeasured = attribute(
        dtype='DevUShort',
        label="Measured duration (sec) of the On command execution",
        abs_change=0,
        max_value=100,
        min_value=0,
        doc=("Report the measured duration (in sec.) of the On command execution"),
    )

    offCmdDurationMeasured = attribute(
        dtype='DevUShort',
        label="Measured duration (sec) of the Off command",
        abs_change=0,
        max_value=100,
        min_value=0,
        doc=("Report the measured duration (in sec.) of the Off command execution"),
    )

    standbyCmdDurationMeasured = attribute(
        dtype='DevUShort',
        label="Measured duration (sec) of the Standby command",
        abs_change=0,
        max_value=100,
        min_value=0,
        doc=("Report the measured duration (in sec.) of the Standby command"),
    )

    onCmdTimeoutExpired = attribute(
        dtype='DevBoolean',
        label="CBF command timeout flag",
        polling_period=2000,
        doc=("Signal the occurence of a timeout during the execution of commands on"
             " CBF Sub-element"),
    )

    offCmdTimeoutExpired = attribute(
        dtype='DevBoolean',
        label="PSS command timeout flag",
        polling_period=2000,
        doc=("Signal the occurence of a timeout during the execution of commands on "
             "PSS Sub-element"),
    )

    standbyCmdTimeoutExpired = attribute(
        dtype='DevBoolean',
        label="PST command timeout flag",
        polling_period=2000,
        doc=("Signal the occurence of a timeout during the execution of commands on"
             " PST Sub-element"),
    )

    cspCbfState = attribute(
        dtype='DevState',
        label="CBF status",
        polling_period=3000,
        doc=("The CBF sub-element Device State. Allowed values are ON, STANDBY, OFF, "
             "DISABLE, ALARM, FAULT, UNKNOWN"),
    )

    cspPssState = attribute(
        dtype='DevState',
        label="PSS status",
        polling_period=3000,
        doc=("The PSS sub-element Device State. Allowed values are ON, STANDBY, OFF,"
             " DISABLE, ALARM, FAULT, UNKNOWN"),
    )

    cspPstState = attribute(
        dtype='DevState',
        label="PST status",
        polling_period=3000,
        doc=("The PST sub-element Device State. Allowed values are ON, STANDBY,OFF,"
             " DISABLE, ALARM, FAULT, UNKNOWN"),
    )

    cspCbfHealthState = attribute(
        dtype='DevEnum',
        label="CBF Health status",
        polling_period=3000,
        abs_change=1,
        doc="The CBF sub-element healthState.",
        enum_labels=["OK", "DEGRADED", "FAILED", "UNKNOWN", ],
    )

    cspPssHealthState = attribute(
        dtype='DevEnum',
        label="PSS Health status",
        polling_period=3000,
        abs_change=1,
        doc="The PSS sub-element healthState",
        enum_labels=["OK", "DEGRADED", "FAILED", "UNKNOWN", ],
    )

    cspPstHealthState = attribute(
        dtype='DevEnum',
        label="PST health status",
        polling_period=3000,
        abs_change=1,
        doc="The PST sub-element healthState.",
        enum_labels=["OK", "DEGRADED", "FAILED", "UNKNOWN", ],
    )

    cbfMasterAddress = attribute(
        dtype='DevString',
        doc="TANGO FQDN of the CBF sub-element Master",
    )

    pssMasterAddress = attribute(
        dtype='DevString',
        doc="TANGO FQDN of the PSS sub-element Master",
    )

    pstMasterAddress = attribute(
        dtype='DevString',
        doc="TANGO FQDN of the PST sub-element Master",
    )

    cspCbfAdminMode = attribute(
        dtype='DevEnum',
        access=AttrWriteType.READ_WRITE,
        label="CBF administrative Mode",
        polling_period=3000,
        abs_change=1,
        doc=("The CBF sub-lement adminMode. Allowed values are ON-LINE, "
             "MAINTENANCE, OFF-LINE, NOT-FITTED, RESERVED"),
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED", ],
    )

    cspPssAdminMode = attribute(
        dtype='DevEnum',
        access=AttrWriteType.READ_WRITE,
        label="PSS administrative mode",
        polling_period=3000,
        abs_change=1,
        doc=("The PSS sub-lement adminMode. Allowed values are ON-LINE, "
             "MAINTENANCE, OFF-LINE, NOT-FITTED, RESERVED"),
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED", ],
    )

    cspPstAdminMode = attribute(
        dtype='DevEnum',
        access=AttrWriteType.READ_WRITE,
        label="PST administrative mode",
        polling_period=3000,
        abs_change=1,
        doc=("The PST sub-lement adminMode. Allowed values are ON-LINE, "
             "MAINTENANCE, OFF-LINE, NOT-FITTED, RESERVED"),
        enum_labels=["ON-LINE", "OFF-LINE", "MAINTENANCE", "NOT-FITTED", "RESERVED", ],
    )

    numOfDevCompletedTask = attribute(
        dtype='DevUShort',
        label="Number of devices that completed the task",
        doc="Number of devices that completed the task",
    )

    onCmdFailure = attribute(
        dtype='DevBoolean',
        label="CBF command failure flag",
        polling_period=1000,
        doc="Alarm flag set when the On command fails with error(s).",
    )

    onFailureMessage = attribute(
        dtype='DevString',
        label="On execution failure message",
        doc="Alarm message when the On command fails with error(s).",
    )

    offCmdFailure = attribute(
        dtype='DevBoolean',
        label="Off execution failure flag",
        polling_period=1000,
        doc="Alarm flag set when the Off command fails with error(s).",
    )

    offFailureMessage = attribute(
        dtype='DevString',
        label="Off execution failure message",
        doc="Alarm message when the Off command fails with error(s).",
    )

    standbyCmdFailure = attribute(
        dtype='DevBoolean',
        label="Standby execution failure message",
        polling_period=1000,
        doc="Alarm flag set when the Standby command fails with error(s).",
    )

    standbyFailureMessage = attribute(
        dtype='DevString',
        label="Standby execution failure message",
        doc="Alarm message when the Standby command fails with error(s).",
    )

    searchBeamsAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=1500,
        label="Search Beam Capabilities Addresses",
        doc="The SearchBeam Capabilityies FQDNs",
    )

    timingBeamsAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        label="Timing Beams Capability Addresses",
        doc="The list of Timing Beam Capabilities FQDNs",
    )

    vlbiBeamsAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=20,
        label="Vlbi Beam Capabilities Addresses",
        doc="The list of VlbiBeam Capabilities FQDNs",
    )


    reportSearchBeamState = attribute(
        dtype=('DevState',),
        max_dim_x=1500,
        label="SearchBeam Capabilities State",
        doc="Report the state of the SearchBeam Capabilities as an array of DevState.",
    )

    reportSearchBeamHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=1500,
        label="SearchBeam Capabilities healthState",
        abs_change=1,
        doc=("Report the health state of the SearchBeam Capabilities as an array"
             " of unsigned short. For ex`: [0,0,...,1..]`"),
    )

    reportSearchBeamAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=1500,
        label="SearchBeam Capabilities adminMode",
        abs_change=1,
        doc=("Report the administrative mode of the SearchBeam Capabilities as an"
             " array of unsigned short. For ex `:[0,0,0,...2..]`"),
    )

    reportTimingBeamState = attribute(
        dtype=('DevState',),
        max_dim_x=16,
        label="TimingBeam Capabilities State",
        doc="Report the state of the TimingBeam Capabilities as an array of DevState.",
    )

    reportTimingBeamHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=16,
        label="TimingBeam Capabilities healthState",
        abs_change=1,
        doc=("Report the health state of the TimingBeam Capabilities as an array of"
             " unsigned short. For ex [0,0,...,1..]"),
    )

    reportTimingBeamAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=16,
        label="TimingBeam Capabilities adminMode",
        abs_change=1,
        doc=("Report the administrativw mode of the TimingBeam Capabilities as an"
             " array of unsigned short. For ex [0,0,0,...2..]"),
    )

    reportVlbiBeamState = attribute(
        dtype=('DevState',),
        max_dim_x=20,
        label="VlbiBeam Capabilities State",
        doc="Report the state of the VLBIBeam Capabilities as an array of DevState.",
    )

    reportVlbiBeamHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=20,
        label="VlbiBeam Capabilities healthState",
        abs_change=1,
        doc=("Report the health state of the VlbiBeam Capabilities as an array of"
             " unsigned short. For ex [0,0,...,1..]"),
    )

    reportVlbiBeamAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=20,
        label="VlbiBeam Capabilities adminMode",
        abs_change=1,
        doc=("Report the administrative mode of the VlbiBeam Capabilities as an"
             " array of unsigned short. For ex -[0,0,0,...2..]"),
    )

    cspSubarrayAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        doc="CSPSubarrays FQDN",
    )
    
    listOfDevCompletedTask = attribute(
        dtype=('DevString',),
        max_dim_x=100,
        label="List of devices that completed the task",
        doc="List of devices that completed the task",
    )
    
    reservedSearchBeamIDs = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        label="SearchBeams reserved IDS",
        doc="For each sub-array idm reports the list of the reserved SearchBeam IDs",
    )

    reportSearchBeamState = attribute(name="reportSearchBeamState",
        label="SearchBeam Capabilities State",
        forwarded=True
    )
    reportSearchBeamHealthState = attribute(name="reportSearchBeamHealthState",
        label="SearchBeam Capabilities healthState",
        forwarded=True
    )
    reportSearchBeamObsState = attribute(name="reportSearchBeamObsState",
        label="SearchBeam Capabilities obsState",
        forwarded=True
    )
    reportSearchBeamAdminMode = attribute(name="reportSearchBeamAdminMode",
        label="SearchBeam Capabilities adminMode",
        forwarded=True
    )
    reportTimingBeamState = attribute(name="reportTimingBeamState",
        label="TimingBeam Capabilities State",
        forwarded=True
    )
    reportTimingBeamHealthState = attribute(name="reportTimingBeamHealthState",
        label="TimingBeam Capabilities healthState",
        forwarded=True
    )
    reportTimingBeamObsState = attribute(name="reportTimingBeamObsState",
        label="TimingBeam Capabilities obsState",
        forwarded=True
    )
    reportTimingBeamAdminMode = attribute(name="reportTimingBeamAdminMode",
        label="TimingBeam Capabilities adminMode",
        forwarded=True
    )
    reportVlbiBeamState = attribute(name="reportVlbiBeamState",
        label="VlbiBeam Capabilities State",
        forwarded=True
    )
    reportVlbiBeamHealthState = attribute(name="reportVlbiBeamHealthState",
        label="VlbiBeam Capabilities healthState",
        forwarded=True
    )
    reportVlbiBeamObsState = attribute(name="reportVlbiBeamObsState",
        label="VlbiBeam Capabilities obsState",
        forwarded=True
    )
    reportVlbiBeamAdminMode = attribute(name="reportVlbiBeamAdminMode",
        label="VlbiBeam Capabilities adminMode",
        forwarded=True
    )
    
    unassignedVlbiBeamIDs = attribute(name="unassignedVlbiBeamIDs",
        label="Unassigned VlbiBeam Capabilities IDs",
        forwarded=True
    )
    unassignedTimingBeamIDs = attribute(name="unassignedTimingBeamIDs",
        label="Unassigned TimingBeam Capabilities IDs",
        forwarded=True
    )
    unassignedSearchBeamIDs = attribute(name="unassignedSearchBeamIDs",
        label="Unassigned SeachBeam Capabilities IDs",
        forwarded=True
    )
    numOfUnassignedVlbiBeams = attribute(name="numOfUnassignedVlbiBeams",
        label="Num of onassigned VlbiBeam Capabilities IDs",
        forwarded=True
    )
    numOfUnassignedTimingBeams = attribute(name="numOfUnassignedTimingBeams",
        label="Num of unassigned TimingBeam Capabilities IDs",
        forwarded=True
    )
    numOfUnassignedSearchBeams = attribute(name="numOfUnassignedSearchBeams",
        label="Num of unassigned SeachBeam Capabilities IDs",
        forwarded=True
    )
    #numOfReservedSearchBeams = attribute(name="numOfReservedSearchBeams",
    #    label="Number of reserved SeachBeam Capabilities",
    #    forwarded=True
    #)
    searchBeamMembership = attribute(name="searchBeamMembership",
        label="SearchBeam Membership",
        forwarded=True
    )
    timingBeamMembership = attribute(name="timingBeamMembership",
        label="TimingBeam Membership",
        forwarded=True
    )
    vlbiBeamMembership = attribute(name="vlbiBeamMembership",
        label="VlbiBeam Membership",
        forwarded=True
    )
    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the CspMaster."""
        SKAMaster.init_device(self)      
        # PROTECTED REGION ID(CspMaster.init_device) ENABLED START #
        self._build_state = '{}, {}, {}'.format(release.name, release.version, release.description)
        self._version_id = release.version
        # connect to TANGO DB
        #csp_tango_db = tango.Database()
        # read the CSP memorized attributes from the TANGO DB. 
        # Note: a memorized attribute has defined the attribute
        # property '__value'
        #attribute_properties = csp_tango_db.get_device_attribute_property(self.get_name(),
        #                                                                  {'adminMode': ['__value']})
        self.set_change_event("state", True, True)
        self.set_archive_event("state", True, True)
        # set init values for the CSP Element and Sub-element SCM states
        self.set_state(tango.DevState.INIT)
        self._health_state = HealthState.OK
        self._admin_mode = AdminMode.ONLINE
        # use defaultdict to initialize the sub-element State,healthState
        # and adminMode. The dictionary uses as keys the sub-element
        # fqdn, for example
        # self._se_state[self.CspCbf]
        # return the State value of the Mid Cbf sub-element.
        self._se_state         = defaultdict(lambda: tango.DevState.DISABLE)
        self._se_health_state  = defaultdict(lambda: HealthState.UNKNOWN)
        self._se_admin_mode    = defaultdict(lambda: AdminMode.NOT_FITTED)
        
        # build a dictionary with the (attr_name, value) of the memorized attributes
        # use memorized atrtibute if present, otherwise the default one
        #memorized_attr_dict = {attr_name : int(value[0]) for attr_name, db_key in attribute_properties.items() 
        #                                                 for key, value in db_key.items() 
        #                                                 if key == '__value'}
        #try:
        #    self._admin_mode = memorized_attr_dict['adminMode']
        #    if self._admin_mode not in [AdminMode.ONLINE, AdminMode.MAINTENANCE]:
        #        self._health_state = HealthState.UNKNOWN
        #        self.set_state(tango.DevState.DISABLE)
        #except KeyError as key_err:
        #    self.logger.info("Key {} not found".format(key_err))
        
        # initialize list with CSP sub-element FQDNs
        self._se_fqdn = []
        # NOTE:
        # The normal behavior when a Device Property is created is:
        # - a self.Property attribute is created in the Dev_Impl object
        # - it takes a value from the Database if it has been defined.
        # - if not, it takes the default value assigned in Pogo.
        # - if no value is specified nowhere, the attribute is created
        #   with [] value.
        self._se_fqdn.append(self.CspCbf)
        self._se_fqdn.append(self.CspPss)
        self._se_fqdn.append(self.CspPst)
        
        # read the sub-elements adminMode (memorized) attributes from
        # the TANGO DB. 
        # Note: a memorized attribute has defined the attribute property '__value'
        #for fqdn in  self._se_fqdn:
        #    attribute_properties = csp_tango_db.get_device_attribute_property(fqdn,
        #                                                                      {'adminMode': ['__value']})
        #    self.logger.info("fqdn: {} attribute_properties: {}".format(fqdn, attribute_properties))
        #    try:
        #        admin_mode_memorized = attribute_properties['adminMode']['__value']
        #        self._se_admin_mode[fqdn] = int(admin_mode_memorized[0])
        #    except KeyError as key_err:
        #        msg = ("No key {} found for sub-element {}"
        #                " adminMode attribute".format(key_err, fqdn))
        #        self.logger.info(msg)
                                                                                      
        # _se_proxies: the sub-element proxies
        # implemented as dictionary:
        # keys: sub-element FQDN
        # values: device proxy
        self._se_proxies = {}
        
        # Nested default dictionary with list of event ids/sub-element. Need to
        # store the event ids for each sub-element and attribute to un-subscribe
        # them at sub-element disconnection.
        # keys: sub-element FQDN
        # values: dictionary (keys: attribute name, values: event id)
        self._se_event_id = defaultdict(lambda: defaultdict(lambda: 0))
        
        # _se_cmd_execution_state: implement the execution state of a long-running
        # command for each sub-element.
        # implemented as a nested default dictionary:
        # keys: sub-element FQDN
        # values: defaut dictionary (keys: command name, values: command state)
        self._se_cmd_execution_state = defaultdict(lambda: defaultdict(lambda: CmdExecState.IDLE))
        
        # _cmd_execution_state: implement the execution state of a long-running
        # command for the whole CSP.  Setting this attribute prevent the execution
        # of the same command while it is already running.
        # implemented as a default dictionary:
        # keys: command name
        # values:command state
        self._cmd_execution_state = defaultdict(lambda: CmdExecState.IDLE)
        
        # _se_cmd_starting_time: for each sub-element report the long-running command 
        # starting time
        # Implemented as dictionary:
        # keys: the sub-element FQDN
        # values: starting time
        self._se_cmd_starting_time = defaultdict(lambda: 0.0)
        
        # _command_thread: thread for the command execution
        # keys: the command name('on, 'off'...)
        # values: thread instance
        self._command_thread = {}

        # _cmd_exec_state_lock: lock to share the _cmd_execution_state
        # attribute among thread in a safe way
        self._cmd_exec_state_lock = threading.Lock()

        # _se_cmd_progress: for each sub-element report the execution progress 
        # of a long-running command
        # implemented as a default nested dictionary:
        # keys: sub-element FQDN
        # values: default dictionary (keys: command name, values: the execution percentage)
        self._se_cmd_progress = defaultdict(lambda: defaultdict(lambda: 0))
        
        # _cmd_progress: report the execution progress of a long-running command
        # implemented as a dictionary:
        # keys: command name ('on', 'off'..)
        # values: the percentage
        self._cmd_progress = defaultdict(lambda: 0)
        
        # _se_cmd_duration_expected: for each sub-element, store the duration (in sec.) 
        # configured for a long-running command 
        # Implemented as a nested default dictionary
        # keys: FQDN
        # values: default dictionary (keys: command name, values: the duration (in sec))
        self._se_cmd_duration_expected = defaultdict(lambda: defaultdict(lambda: 20))
        
        # _cmd_duration_expected: store the duration (in sec.) configured for
        # a long-running command 
        # Implemented asdefault dictionary
        # keys: command name ('on', 'off',..)
        # values: the duration (in sec)
        self._cmd_duration_expected = defaultdict(lambda: 30)
        
        # _cmd_duration_measured: report the measured duration (in sec.) for
        # a long-running command 
        # Implemented as default dictionary
        # keys: command name ('on', 'off',..)
        # values: the duration (in sec)
        self._cmd_duration_measured = defaultdict(lambda: 0)
        
        # _timeout_expired: report the timeout flag 
        # Implemented as a dictionary
        # keys: command name ('on', 'off', 'standby'..)
        # values: True/False
        self._timeout_expired = defaultdict(lambda: False)
        
        # _failure_raised: report the failure flag 
        # Implemented as a dictionary
        # keys: command name ('on', 'off', 'standby'..)
        # values: True/False
        self._failure_raised = defaultdict(lambda: False)
        
        # _failure_message: report the failure message
        # Implemented as a dictionary
        # keys: command name ('on', 'off', 'standby'..)
        # values: the message
        self._failure_message = defaultdict(lambda: '')
        
        # _list_dev_completed_task: for each long-running command report the list of subordinate
        # components that completed the task
        # Implemented as a dictionary
        # keys: the command name ('on', 'off',...)
        # values: the list of components
        self._list_dev_completed_task = defaultdict(lambda: [])
        # the last executed command 
        self._last_executed_command = ""
         
        # _num_dev_completed_task: for each long-running command report the number
        #  of subordinate components that completed the task
        # Implemented as a dictionary
        # keys: the command name ('on', 'off',...)
        # values: the number of components
        self._num_dev_completed_task = defaultdict(lambda:0)
        
        # _capability_proxy: dictionary 
        # keys: the Capability Monitor devices FQDNs
        # values: DeviceProxy proxies
        self._capability_monitor_proxy = {}

        # _capability_fqdn: list of CapabilityMonitor FQDN devices
        self._capability_monitor_fqdn = []

        # Try connection with sub-elements
        self._connect_to_subelements()

        # to use the push model in command_inout_asynch (the one with the callback parameter),
        # change the global TANGO model to PUSH_CALLBACK.
        apiutil = tango.ApiUtil.instance()
        apiutil.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)
        # PROTECTED REGION END #    //  CspMaster.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(CspMaster.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  CspMaster.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(CspMaster.delete_device) ENABLED START #
        event_to_remove = {}
        for fqdn in self._se_fqdn:
            try:
                event_to_remove[fqdn] = []
                for attr_name, event_id in self._se_event_id[fqdn].items():
                    try:
                        # need to check the event_id value because with the use of defaultdict
                        # it may happen that a dictionary entry is = 0 (default ditionary value)
                        if event_id:
                            self._se_proxies[fqdn].unsubscribe_event(event_id)
                            event_to_remove[fqdn].append(attr_name)
                        # NOTE: in tango unsubscription of not-existing event
                        # id raises a KeyError exception not a DevFailed !!
                    except KeyError as key_err:
                        msg = ("Failure unsubscribing event {} "
                               "on device {}. Reason: {}".format(event_id,
                                                                 fqdn,
                                                                 key_err))
                        self.logger.error(msg)
                # remove the attribute entry from the fqdn dictionary
                for attr_name in event_to_remove[fqdn]:
                    del self._se_event_id[fqdn][attr_name]
                # check if there are still some registered events.
                if not self._se_event_id[fqdn]:
                    # remove the dictionary element when the fqdn dictionary is
                    # empty
                    self._se_event_id.pop(fqdn)
                else:
                    # What to do in this case??. Only log (for the moment)
                    msg = "Still subscribed events: {}".format(self._se_event_id)
                    self.logger.warning(msg)
            except KeyError as key_err:
                msg = " Can't retrieve the information of key {}".format(key_err)
                self.logger.error(msg)
                continue
        # clear any list and dict
        self._se_fqdn.clear()
        self._se_proxies.clear()
        # PROTECTED REGION END #    //  CspMaster.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_adminMode(self):
        # PROTECTED REGION ID(CspMaster.adminMode_read) ENABLED START #
        """Return the adminMode attribute."""
        return self._admin_mode
        # PROTECTED REGION END #    //  CspMaster.adminMode_read

    def write_adminMode(self, value):
        # PROTECTED REGION ID(CspMaster.adminMode_write) ENABLED START #
        """
        Write attribute method.

        Set the administration mode for the whole CSP element.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, MAINTENANCE, NOT-FITTED, RESERVED).
        :type: DevEnum
        :return: None
        """
        # PROTECTED REGION ID(CspMaster.adminMode_write) ENABLED START #
        for fqdn in self._se_fqdn:
            try:
                self._se_write_adminMode(value, fqdn)
            except tango.DevFailed as df:
                
                log_msg = (("Failure in setting adminMode "
                            "for device {}: {}".format(str(fqdn), 
                            str(df.args[0].reason))))
                self.logger.error(log_msg)
        #TODO: what happens if one sub-element fails?
        # add check on timeout command execution
        self._admin_mode = value
        if self._admin_mode in [AdminMode.OFFLINE, AdminMode.NOT_FITTED, AdminMode.RESERVED]:
            self.set_state(tango.DevState.DISABLE)
        else:
            if self.get_state() == tango.DevState.DISABLE:
                #STANDBY or ON how can be determined??
                self.set_state(tango.DevState.STANDBY)
        # PROTECTED REGION END #    //  CspMaster.adminMode_write

    def read_onCommandProgress(self):
        # PROTECTED REGION ID(CspMaster.onCommandProgress_read) ENABLED START #
        """Return the onCommandProgress attribute."""
        return int(self._cmd_progress['on'])
        # PROTECTED REGION END #    //  CspMaster.onCommandProgress_read

    def read_offCommandProgress(self):
        # PROTECTED REGION ID(CspMaster.offCommandProgress_read) ENABLED START #
        """Return the offCommandProgress attribute."""
        return int(self._cmd_progress['off'])
        # PROTECTED REGION END #    //  CspMaster.offCommandProgress_read

    def read_standbyCommandProgress(self):
        # PROTECTED REGION ID(CspMaster.standbyCommandProgress_read) ENABLED START #
        """Return the standbyCommandProgress attribute."""
        return int(self._cmd_progress['standby'])
        # PROTECTED REGION END #    //  CspMaster.standbyCommandProgress_read

    def read_onCmdDurationExpected(self):
        # PROTECTED REGION ID(CspMaster.onCmdDurationExpected_read) ENABLED START #
        """Return the onCmdDurationExpected attribute."""
        return self._cmd_duration_expected['on']
        # PROTECTED REGION END #    //  CspMaster.onCmdDurationExpected_read

    def write_onCmdDurationExpected(self, value):
        # PROTECTED REGION ID(CspMaster.onCmdDurationExpected_write) ENABLED START #
        """Set the onCmdDurationExpected attribute."""
        self._cmd_duration_expected['on'] = value
        # PROTECTED REGION END #    //  CspMaster.onCmdDurationExpected_write

    def read_offCmdDurationExpected(self):
        # PROTECTED REGION ID(CspMaster.offCmdDurationExpected_read) ENABLED START #
        """Return the offCmdDurationExpected attribute."""
        return self._cmd_duration_expected['off']
        # PROTECTED REGION END #    //  CspMaster.offCmdDurationExpected_read

    def write_offCmdDurationExpected(self, value):
        # PROTECTED REGION ID(CspMaster.offCmdDurationExpected_write) ENABLED START #
        """Set the offCmdDurationExpected attribute."""
        self._cmd_duration_expected['off'] = value
        # PROTECTED REGION END #    //  CspMaster.offCmdDurationExpected_write

    def read_standbyCmdDurationExpected(self):
        # PROTECTED REGION ID(CspMaster.standbyCmdDurationExpected_read) ENABLED START #
        """Return the standbyCmdDurationExpected attribute."""
        return self._cmd_duration_expected['standby'] 
        # PROTECTED REGION END #    //  CspMaster.standbyCmdDurationExpected_read

    def write_standbyCmdDurationExpected(self, value):
        # PROTECTED REGION ID(CspMaster.standbyCmdDurationExpected_write) ENABLED START #
        """Set the standbyCmdDurationExpected attribute."""
        self._cmd_duration_expected['standby'] = value
        # PROTECTED REGION END #    //  CspMaster.standbyCmdDurationExpected_write

    def read_onCmdDurationMeasured(self):
        # PROTECTED REGION ID(CspMaster.onCmdDurationMeasured_read) ENABLED START #
        """Return the onCmdDurationMeasured attribute."""
        return int(self._cmd_duration_measured['on'])
        # PROTECTED REGION END #    //  CspMaster.onCmdDurationMeasured_read

    def read_offCmdDurationMeasured(self):
        # PROTECTED REGION ID(CspMaster.offCmdDurationMeasured_read) ENABLED START #
        """Return the offCmdDurationMeasured attribute."""
        return int(self._cmd_duration_measured['off'])
        # PROTECTED REGION END #    //  CspMaster.offCmdDurationMeasured_read

    def read_standbyCmdDurationMeasured(self):
        # PROTECTED REGION ID(CspMaster.standbyCmdDurationMeasured_read) ENABLED START #
        """Return the standbyCmdDurationMeasured attribute."""
        return int(self._cmd_duration_measured['standby'])
        # PROTECTED REGION END #    //  CspMaster.standbyCmdDurationMeasured_read

    def read_onCmdTimeoutExpired(self):
        # PROTECTED REGION ID(CspMaster.onCmdTimeoutExpired_read) ENABLED START #
        """Return the onCmdTimeoutExpired attribute."""
        return self._timeout_expired['on']
        # PROTECTED REGION END #    //  CspMaster.cbfOnCmdTimeoutExpired_read

    def read_offCmdTimeoutExpired(self):
        # PROTECTED REGION ID(CspMaster.pssOnCmdTimeoutExpired_read) ENABLED START #
        """Return the offCmdTimeoutExpired attribute."""
        return self._timeout_expired['off']
        # PROTECTED REGION END #    //  CspMaster.pssOnCmdTimeoutExpired_read

    def read_standbyCmdTimeoutExpired(self):
        # PROTECTED REGION ID(CspMaster.standbyCmdTimeoutExpired_read) ENABLED START #
        """Return the standbyCmdTimeoutExpired attribute."""
        return self._timeout_expired['standby']
        # PROTECTED REGION END #    //  CspMaster.standbyCmdTimeoutExpired_read

    def read_cspCbfState(self):
        # PROTECTED REGION ID(CspMaster.cspCbfState_read) ENABLED START #
        """Return the cspCbfState attribute."""
        return self._se_state[self.CspCbf]
        # PROTECTED REGION END #    //  CspMaster.cspCbfState_read

    def read_cspPssState(self):
        # PROTECTED REGION ID(CspMaster.cspPssState_read) ENABLED START #
        """Return the cspPssState attribute."""
        return self._se_state[self.CspPss]
        # PROTECTED REGION END #    //  CspMaster.cspPssState_read

    def read_cspPstState(self):
        # PROTECTED REGION ID(CspMaster.cspPstState_read) ENABLED START #
        """Return the cspPstState attribute."""
        return self._se_state[self.CspPst]
        # PROTECTED REGION END #    //  CspMaster.cspPstState_read

    def read_cspCbfHealthState(self):
        # PROTECTED REGION ID(CspMaster.cspCbfHealthState_read) ENABLED START #
        """Return the cspCbfHealthState attribute."""
        return self._se_health_state[self.CspCbf]
        # PROTECTED REGION END #    //  CspMaster.cspCbfHealthState_read

    def read_cspPssHealthState(self):
        # PROTECTED REGION ID(CspMaster.cspPssHealthState_read) ENABLED START #
        """Return the cspPssHealthState attribute."""
        return self._se_health_state[self.CspPss]
        # PROTECTED REGION END #    //  CspMaster.cspPssHealthState_read

    def read_cspPstHealthState(self):
        # PROTECTED REGION ID(CspMaster.cspPstHealthState_read) ENABLED START #
        """Return the cspPstHealthState attribute."""
        return self._se_health_state[self.CspPst]
        # PROTECTED REGION END #    //  CspMaster.cspPstHealthState_read

    def read_cbfMasterAddress(self):
        # PROTECTED REGION ID(CspMaster.cbfMasterAddress_read) ENABLED START #
        """Return the cbfMasterAddress attribute."""
        return self.CspCbf
        # PROTECTED REGION END #    //  CspMaster.cbfMasterAddress_read

    def read_pssMasterAddress(self):
        # PROTECTED REGION ID(CspMaster.pssMasterAddress_read) ENABLED START #
        """Return the pssMasterAddress attribute."""
        return self.CspPss
        # PROTECTED REGION END #    //  CspMaster.pssMasterAddress_read

    def read_pstMasterAddress(self):
        # PROTECTED REGION ID(CspMaster.pstMasterAddress_read) ENABLED START #
        """Return the pstMasterAddress attribute."""
        return self.CspPst
        # PROTECTED REGION END #    //  CspMaster.pstMasterAddress_read

    def read_cspCbfAdminMode(self):
        # PROTECTED REGION ID(CspMaster.cspCbfAdminMode_read) ENABLED START #
        """Return the cspCbfAdminMode attribute."""
        return self._se_admin_mode[self.CspCbf]
        # PROTECTED REGION END #    //  CspMaster.cspCbfAdminMode_read

    def write_cspCbfAdminMode(self, value):
        # PROTECTED REGION ID(CspMaster.cspCbfAdminMode_write) ENABLED START #
        """
        Write attribute method

        Set the CBF sub-element *adminMode* attribute value.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, MAINTENANCE, NOT-FITTED, RESERVED).
        :return: None
        :raises: tango.DevFailed when there is no DeviceProxy providing interface \
            to the CBF sub-element Master, or an exception is caught in command execution.
        """
        self._se_write_adminMode(value, self.CspCbf)
        # PROTECTED REGION END #    //  CspMaster.cspCbfAdminMode_write

    def read_cspPssAdminMode(self):
        # PROTECTED REGION ID(CspMaster.cspPssAdminMode_read) ENABLED START #
        """Return the cspPssAdminMode attribute."""
        return self._se_admin_mode[self.CspPss]
        # PROTECTED REGION END #    //  CspMaster.cspPssAdminMode_read

    def write_cspPssAdminMode(self, value):
        # PROTECTED REGION ID(CspMaster.cspPssAdminMode_write) ENABLED START #
        """Set the cspPssAdminMode attribute."""
        self._se_write_adminMode(value, self.CspPss)
        # PROTECTED REGION END #    //  CspMaster.cspPssAdminMode_write

    def read_cspPstAdminMode(self):
        # PROTECTED REGION ID(CspMaster.cspPstAdminMode_read) ENABLED START #
        """Return the cspPstAdminMode attribute."""
        return self._se_admin_mode[self.CspPst]
        # PROTECTED REGION END #    //  CspMaster.cspPstAdminMode_read

    def write_cspPstAdminMode(self, value):
        # PROTECTED REGION ID(CspMaster.cspPstAdminMode_write) ENABLED START #
        """Set the cspPstAdminMode attribute."""
        self._se_write_adminMode(value, self.CspPst)
        # PROTECTED REGION END #    //  CspMaster.cspPstAdminMode_write

    def read_numOfDevCompletedTask(self):
        # PROTECTED REGION ID(CspMaster.numOfDevCompletedTask_read) ENABLED START #
        """Return the numOfDevCompletedTask attribute."""
        if not self._last_executed_command:
            return 0
        return self._num_dev_completed_task[self._last_executed_command]
        
        # PROTECTED REGION END #    //  CspMaster.numOfDevCompletedTask_read
    def read_onCmdFailure(self):
        # PROTECTED REGION ID(CspMaster.onCmdFailure_read) ENABLED START #
        """Return the onCmdFailure attribute."""
        return self._failure_raised['on']
        # PROTECTED REGION END #    //  CspMaster.onCmdFailure_read

    def read_onFailureMessage(self):
        # PROTECTED REGION ID(CspMaster.onFailureMessage_read) ENABLED START #
        """Return the onFailureMessage attribute."""
        return self._failure_message['on']
        # PROTECTED REGION END #    //  CspMaster.onFailureMessage_read

    def read_offCmdFailure(self):
        # PROTECTED REGION ID(CspMaster.offCmdFailure_read) ENABLED START #
        """Return the offCmdFailure attribute."""
        return self._failure_raised['off']
        # PROTECTED REGION END #    //  CspMaster.offCmdFailure_read

    def read_offFailureMessage(self):
        # PROTECTED REGION ID(CspMaster.offFailureMessage_read) ENABLED START #
        """Return the offFailureMessage attribute."""
        return self._failure_message['off']
        # PROTECTED REGION END #    //  CspMaster.offFailureMessage_read

    def read_standbyCmdFailure(self):
        # PROTECTED REGION ID(CspMaster.standbyCmdFailure_read) ENABLED START #
        """Return the standbyCmdFailure attribute."""
        return self._failure_raised['standby']
        # PROTECTED REGION END #    //  CspMaster.standbyCmdFailure_read

    def read_standbyFailureMessage(self):
        # PROTECTED REGION ID(CspMaster.standbyFailureMessage_read) ENABLED START #
        """Return the standbyFailureMessage attribute."""
        return self._failure_message['standby']
        # PROTECTED REGION END #    //  CspMaster.standbyFailureMessage_read

    def read_cspSubarrayAddresses(self):
        # PROTECTED REGION ID(CspMaster.cspSubarrayAddresses_read) ENABLED START #
        """Return the cspSubarrayAddresses attribute."""
        return self.CspSubarrays
        # PROTECTED REGION END #    //  CspMaster.cspSubarrayAddresses_read
        
    def read_listOfDevCompletedTask(self):
        # PROTECTED REGION ID(CspMaster.listOfDevCompletedTask_read) ENABLED START #
        """Return the listOfDevCompletedTask attribute."""
        #TO IMPLEMENT
        # report the list of the devices that completed the last executed task
        if not self._last_executed_command:
            return ('',)
        return self._list_dev_completed_task[self._last_executed_command]
        # PROTECTED REGION END #    //  CspMaster.listOfDevCompletedTask_read
    
    def read_reservedSearchBeamIDs(self):
        # PROTECTED REGION ID(CspMaster.reservedSearchBeamIDs_read) ENABLED START #
        """Return the reservedSearchBeamIDs attribute."""
        return ('',)
        # PROTECTED REGION END #    //  CspMaster.reservedSearchBeamIDs_read

    def read_searchBeamsAddresses(self):
        # PROTECTED REGION ID(CspMaster.searchBeamsAddresses_read) ENABLED START #
        """Return the searchBeamsAddresses attribute.
           This attribute stores the list of the SearchBeams Capabilities TANGO
           addresses (FQDNs)
        """
        self.logger.info(self._capability_monitor_fqdn)
        if not self._is_device_running(self.SearchBeamsMonitor, self._capability_monitor_proxy):
            return ('',)
        try:
            proxy = self._capability_monitor_proxy[self.SearchBeamsMonitor]
            return proxy.capabilityAddresses
        except tango.DevFailed as tango_err:
            msg = "Attribute reading failure: {}".format(tango_err.args[0].desc)
            self.logger.error(msg)
            tango.Except.throw_exception("Attribute reading failure", msg,
                                         "read_searchBeamsAddresses", tango.ErrSeverity.ERR)
        # PROTECTED REGION END #    //  CspMaster.searchBeamsAddresses_read

    def read_timingBeamsAddresses(self):
        # PROTECTED REGION ID(CspMaster.timingBeamsAddresses_read) ENABLED START #
        """Return the timingBeamsAddresses attribute.
           This attribute stores the list of the TimingBeams Capabilities TANGO
           addresses (FQDNs)
        """
        if not self._is_device_running(self.TimingBeamsMonitor, self._capability_monitor_proxy):
            return ('',)
        try:
            proxy = self._capability_monitor_proxy[self.TimingBeamsMonitor]
            return proxy.capabilityAddresses
        except tango.DevFailed as tango_err:
            msg = "Attribute reading failure: {}".format(tango_err.args[0].desc)
            self.logger.error(msg)
            tango.Except.throw_exception("Attribute reading failure", msg,
                                         "read_timingBeamsAddresses", tango.ErrSeverity.ERR)
        # PROTECTED REGION END #    //  CspMaster.timingBeamsAddresses_read

    def read_vlbiBeamsAddresses(self):
        # PROTECTED REGION ID(CspMaster.vlbiBeamsAddresses_read) ENABLED START #
        """Return the vlbiBeamsAddresses attribute.
           This attribute stores the list of the VlbiBeams Capabilities TANGO
           addresses (FQDNs)
        """
        if not self._is_device_running(self.VlbiBeamsMonitor, self._capability_monitor_proxy):
            return ('',)
        try:
            proxy = self._capability_monitor_proxy[self.VlbiBeamsMonitor]
            return proxy.capabilityAddresses
        except tango.DevFailed as tango_err:
            msg = "Attribute reading failure: {}".format(tango_err.args[0].desc)
            self.logger.error(msg)
            tango.Except.throw_exception("Attribute reading failure", msg,
                                         "read_vlbiBeamsAddresses", tango.ErrSeverity.ERR)
        # PROTECTED REGION END #    //  CspMaster.vlbiBeamsAddresses_read

    # --------
    # Commands
    # --------

    @AdminModeCheck('On')
    def is_On_allowed(self):
        """
        *TANGO is_allowed method*

        Command *On* is allowed when the device *State* is STANDBY.

        :return: True if the method is allowed, otherwise False.
        """
        # PROTECTED REGION ID(CspMaster.is_On_allowed) ENABLED START #
        # Note: as per SKA Guidelines, the command is allowed when 
        if self.get_state() not in [tango.DevState.STANDBY, tango.DevState.ON]:
            return False
        return True
    
    @command(
        dtype_in='DevVarStringArray',
        doc_in=("If the array length is 0, the command applies to the whole CSP Element."
                " If the array length is > 1, each array element specifies the FQDN of "
                "the CSP SubElement to switch ON."),
    )
    @DebugIt()  
    @CmdInputArgsCheck("onCommandProgress", "onCmdTimeoutExpired", cmd_name = "on")
    def On(self, argin):
        # PROTECTED REGION ID(CspMaster.On) ENABLED START #
        """
        *Class TANGO method*
        
        Switch-on the CSP sub-elements specified by the input argument. If no argument is
        specified, the command is issued on all the CSP sub-elements.
        The command is executed if the *AdminMode* is ONLINE or *MAINTENANCE*.
        If the AdminMode is *OFFLINE*, *NOT-FITTED* or *RESERVED*, the method throws an 
        exception.
        The CSP.LMC commands sub-element transition from STANDBY to ON sequentially. It
        waits for the first sub-element (CBF) to complete the transition, then issues the
        command to the second sub-element (e.g. PST), waits for PST to complete and then 
        issues command for PSS.
        Command is forwarded to the sub-element devices specified in the input list,
        only if their adminMode is ONLINE or MAINTENANCE.

        :param argin: 
                    The list of sub-element FQDNs to switch-on or an empty list to switch-on the whole 
                    CSP Element.
                    
                    If the array length is 0, the command applies to the whole CSP Element. If the 
                    array length is > 1, each array element specifies the FQDN of the
                    CSP SubElement to switch ON.
        :type: 'DevVarStringArray'                    
        :return: None
        :raises: *tango.DevFailed* exception when:
        
                - the CSP adminMode is not correct
                - one or more sub-element devices are already running a power command
                - there is no DeviceProxy providing interface to the CBF sub-element
        """
        # the input list after decorator examination
        device_list = argin
        # invoke the constructor for the command thread. 
        # The target thread function is common to all the invoked commands. Specifc information
        # are passed as arguments of the function
        # args: the list of sub-element FQDNS
        # args_dict: dictionary with the specific command information
        args_dict = {'cmd_name':'On', 'dev_state': tango.DevState.ON}
        self._command_thread['on'] = threading.Thread(target=self._issue_power_command, name="Thread-On",
                                               args=(device_list,),
                                               kwargs=args_dict)
        try:
            # start the thread
            # set the  CSP command execution running flag
            self.logger.info("self._cmd_execution_state: {}".format(self._cmd_execution_state.items()))
            with self._cmd_exec_state_lock:
                self._cmd_execution_state['on'] = CmdExecState.RUNNING
            self._command_thread['on'].start()
            self.logger.debug("self._cmd_execution_state: {}".format(self._cmd_execution_state.items()))
            # To temporarily solve SKB-26 -> join the thread to synchronize the command
            self._command_thread['on'].join()
            self._enable_subarrays()
            self._update_csp_state()
            self.logger.info("CSP State:{}".format(self.get_state()))
        except Exception as e:
            # reset the sub-element command exec state
            self.logger.error(f"Received error {e}")
            self._se_cmd_execution_state.clear()
            self._cmd_execution_state['on'] = CmdExecState.IDLE
            tango.Except.throw_exception("Command failed",
                                         "Thread non started while executing On command",
                                         "On",
                                         tango.ErrSeverity.ERR)
        # PROTECTED REGION END #    //  CspMaster.On

    @AdminModeCheck('Off')
    def is_Off_allowed(self):
        """
        *TANGO is_allowed method*

        Command *Off* is allowed when the device *State* is STANDBY.

        :return: True if the method is allowed, otherwise False.
        """
        # PROTECTED REGION ID(CspMaster.is_On_allowed) ENABLED START #
        # Note: as per SKA Guidelines, the command is allowed when 
        if self.get_state() not in [tango.DevState.STANDBY, tango.DevState.OFF]:
            return False
        return True

    @command(
        dtype_in='DevVarStringArray',
        doc_in=("If the array length is 0, the command applies to the whole CSP Element."
                "If the array length is > 1, each array element specifies the FQDN of the"
                " CSP SubElement to switch OFF."),
    )
    @DebugIt()
    @CmdInputArgsCheck("offCommandProgress", "offCmdTimeoutExpired", cmd_name = "off")
    def Off(self, argin):
        # PROTECTED REGION ID(CspMaster.Off) ENABLED START #
        """
        *Class TANGO method*
        
        Power-down the CSP sub-elements specified by the input argument. If no argument is
        specified, the command is issued on all the CSP sub-elements.
        The command is executed if the *AdminMode* is ONLINE or *MAINTENANCE*.
        If the AdminMode is *OFFLINE*, *NOT-FITTED* or *RESERVED*, the method throws an 
        exception.
        The CSP.LMC commands sub-element transition from STANDBY to OFF sequentially.It
        waits for the first sub-element (CBF) to complete the transition, then issues the
        command to the second sub-element (e.g. PST), waits for PST to complete and then 
        issues command for PSS.
        Command is forwarded to the sub-element devices specified in the input list,
        only if their adminMode is ONLINE or MAINTENANCE.

        :param argin: 
                    The list of sub-element FQDNs to power-down or an empty list to
                    power-off the whole CSP Element.
                    If the array length is 0, the command applies to the whole CSP
                    Element. If the array length is > 1, each array element specifies
                    the FQDN of the CSP SubElement to power-off.
        :type: 'DevVarStringArray'                    
        :return: None
        :raises: *tango.DevFailed* exception when:
        
                - the CSP adminMode is not correct
                - one or more sub-element devices are already running a power command
                - there is no DeviceProxy providing interface to the CBF sub-element
        """
        # the input list after decorator examination
        device_list = argin
        # invoke the constructor for the command thread. 
        # The target thread function is common to all the invoked commands. Specifc information
        # are passed as arguments of the function
        # args: the list of sub-element FQDNS
        # args_dict: dictionary with the specific command information
        args_dict = {'cmd_name':'Off',
                     'dev_state': tango.DevState.OFF}
       
        self._command_thread['off'] = threading.Thread(target=self._issue_power_command, name="Thread-Off",
                                                args=(device_list,),
                                                kwargs=args_dict)
        try:
            # set the  CSP command execution running flag
            with self._cmd_exec_state_lock:
                self._cmd_execution_state['off'] = CmdExecState.RUNNING
            # start the thread
            self._command_thread['off'].start()
            self._command_thread['off'].join()
            self._update_csp_state()
            self.logger.info("CSP State:{}".format(self.get_state()))
        except Exception:
            # reset the sub-element command exec state
            self._se_cmd_execution_state.clear()
            self._cmd_execution_state['off'] = CmdExecState.IDLE
            tango.Except.throw_exception("Command failed",
                                         "Thread non started while executing Off command",
                                         "Off",
                                         tango.ErrSeverity.ERR)
        
        # PROTECTED REGION END #    //  CspMaster.Off

    @AdminModeCheck('Standby')
    def is_Standby_allowed(self):
        """
        *TANGO is_allowed method*

        Command *Standby* is allowed when the device *State* is ON.

        :return: True if the method is allowed, otherwise False.
        """
        # PROTECTED REGION ID(CspMaster.is_On_allowed) ENABLED START #
        # Note: as per SKA Guidelines, the command is allowed when 
        if self.get_state() not in [tango.DevState.STANDBY, tango.DevState.ON]:
            return False
        return True
    
    @command(
        dtype_in='DevVarStringArray',
        doc_in=("If the array length is 0, the command applies to the whole CSP Element. "
                "If the array length is > 1, each array element specifies the FQDN of the"
                "CSP SubElement to put in STANDBY mode."),
    )
    @DebugIt()
    @CmdInputArgsCheck("standbyCommandProgress", 
                  "standbyCmdTimeoutExpired", cmd_name = "standby")
    def Standby(self, argin):
        # PROTECTED REGION ID(CspMaster.Standby) ENABLED START #
        """
        Transit CSP or one or more CSP SubElements from ON to *STANDBY*.
        The command is executed only if the *AdminMode* is ONLINE or *MAINTENANCE*.
        If the AdminMode is *OFFLINE*, *NOT-FITTED* or *RESERVED*, the method throws an 
        exception.
        The CSP.LMC commands sub-element transition from ON to STANDBY sequentially. It
        waits for the first sub-element (CBF) to complete the transition, then issues the
        command to the second sub-element (e.g. PST), waits for PST to complete and then 
        issues command for PSS.
        Command is forwarded to the sub-element devices specified in the input list,
        only if their adminMode is ONLINE or MAINTENANCE.

        :param argin: A list with the FQDN of the sub-elements to set in low-power.
                      If the array length is 0, the command applies to the whole
                      CSP Element. If the array length is > 1, each array element 
                      specifies the FQDN of the CSP SubElement to put in STANDBY mode.
        :type: 'DevVarStringArray' 
        :return: None
        :raises: *tango.DevFailed* exception when:
        
                - the CSP adminMode is not correct
                - one or more sub-element devices are already running a power command
                - there is no DeviceProxy providing interface to the CBF sub-element
        """
        device_list = argin
        # build the dictionary to pass as argument of the thread target function
        # The dictionary keys are:
        # cmd_name : the TANGO command name to execute
        # attr_name: the corresponding command progress attribute to subscribe
        # dev_state: the expected finale state for the device transition
        args_dict = {'cmd_name':'Standby',
                     'dev_state':tango.DevState.STANDBY}
        # invoke the constructor for the command thread
        self._command_thread['standby'] = threading.Thread(target=self._issue_power_command,
                                                           name="Thread-Standby",
                                                           args=(device_list,),
                                                           kwargs=args_dict)
        try:
            # set the  CSP command execution running flag
            self.logger.debug("self._cmd_execution_state: {}".format(self._cmd_execution_state.items()))
            with self._cmd_exec_state_lock:
                self._cmd_execution_state['standby'] = CmdExecState.RUNNING
            # start the thread
            self._command_thread['standby'].start()
            self.logger.info("self._cmd_execution_state: {}".format(self._cmd_execution_state.items()))
            # To temprarily solve the SKB-26 -> join the thread to synchronize the command
            self._command_thread['standby'].join()
            self._update_csp_state()
            self.logger.info("CSP State:{}".format(self.get_state()))
            self._disable_subarrays()
        except Exception:
            # reset the sub-element command exec state
            self._se_cmd_execution_state.clear()
            self._cmd_execution_state['standby'] = CmdExecState.IDLE
            tango.Except.throw_exception("Command failed",
                                         "Thread not started while executing Standby command",
                                         "Standby",
                                         tango.ErrSeverity.ERR)
        # PROTECTED REGION END #    //  CspMaster.Standby

    @command(
    )
    @DebugIt()
    def Upgrade(self):
        # PROTECTED REGION ID(CspMaster.Upgrade) ENABLED START #
        """

        :return: None
        """
        pass
        # PROTECTED REGION END #    //  CspMaster.Upgrade

# ----------
# Run server
# ----------

def main(args=None, **kwargs):
    # PROTECTED REGION ID(CspMaster.main) ENABLED START #
    return run((CspMaster,), args=args, **kwargs)
    # PROTECTED REGION END #    //  CspMaster.main

if __name__ == '__main__':
    main()
