###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

0.8.1
-----
- Fix global timeout problem on configuration (YANDA support)

0.8.0
-----
- created new repository on Gitlab (ska-csp-lmc-common) to separate the project from mid-csp and low-csp
- updated pipeline for the new repository
- updated documentation for the new repository

0.7.5
-----
- CspMaster: On/Standby commands: update the master state after switching on/off the subarrays
- CspMaster: use push_change_event from code to update the master State attribute.
- CspSubarray: work to resolve bug skb-49

0.7.4
-----
- OFF command when sub-element is ON-EMPTY
- Added transaction id to the common section of ADR-18/22 json config file.

0.7.3
-----
- Solved dead-lock on Abort Command during Configuration
- Restart Command check the if subelements are EMPTY
- Check failures on GoToIdle execution

0.7.2
-----
- Initialization of CSP Subarray align its state ans obsstate to sub-elements.

0.7.1
-----
- Off command in Subarray drives each sub-element component to the final OFF/EMPTY state.
- CspMaster On/Standby commands are executed synchronously

0.7.0
-----
- Align CSP.LMC to be compliant with the SKA Base Classes API: add of 
  Assign/ReleaseResources commands.
- Add transaction ID for the new commands

0.6.12
-----
- support to ADR18/22

0.6.11
-----
- fix bug in setup.py

0.6.10
-----
- use of ska-log-transaction via transaction_id decorator.

0.6.9
-----
- use lmcbaseclasses 0.6.5

0.6.8
-----
- Removed the CspSubarrayStateModel: no more used. This state model was not correct becuase it
  did not register the callback to push events on obsState attribute 

0.6.7
-----
- Fixed a bug in re-configuration process.

0.6.6
-----
- Added Restart and Reset

0.6.5
-----
- Implemented Abort command. 
- Use mutex to avoid Abort invkoking while the configuring thread is
  checking the global CSP observing state.

0.6.4
-----
- CSP Subarrays are enabled when the  On command is invoked on the CspMaster
- fix EndScan: command invoked synchronously
- GoToIdle now invoked synchronously

0.6.3
-----
- use ska-python-buildenv and runtime 9.3.2
- use lmcbaseclasses ver 0.6.3
- Add support for Off and ObsReset commands
- Fix type_in argument for Scan (now it accepts a DevString instead of a DevStringVarArray)
- EndScan to discuss actual implementation

0.6.2
-----
- Finalized support for Configure command.
- Added GoToIdle command subclassing the ActionCommand class.

0.6.1
-----
- adding support for Configure command

0.6.0
-----
- start implementing lmcbaseclasses 0.6.0

0.5.12
------
- push event on obsState
- set subarray to CONFIGURING at Configure command reception
- modified __configure_scan thread to catch the CbfSubarray CONFIGURING
  obsState 

0.5.11
------
- call to DeviceProxy instead of tango.DeviceProxy to have the deviceemocking works
  properly
- removed the cbfOutputLinks forwarded attribute and updated the configuration
  files for the TANGO DB.
- set the lmcbaseclasses version < 0.6.0

0.5.10
------
- fix a minor bug

0.5.9
-----
- implemented the ADR-4 and ADR-10 decisions:

  * the json file contains the unique configuration ID
    and the list of the outputlinks, channelAverageMap and
    received addresses for CBF
  * the Scan command specifies as argument the scan Id
- added the isCmdInProgress attribute to get information
  about the execution state of a command

0.5.8
-----
- use ska-logging 0.3.0 and lmcbasclasses 0.5.2

0.5.7
-----
- removed the problem with forwarded attributes of array of strings.
  When the root attribute of a forwarded attribute is an array of 
  strings the devices crashes.
  Moved forwarded attributes to TANGO attributes.
- modify install_requires package in setup.py: use of 
  ska-logging version < 0.3.0 (0.3.0 has changed 
  importing and current lmcbaseclasses package is not
  updated, yet.).

0.5.6
-----
- set the exec flag before the thread start
- re-enable the cmd_ended_cb callback registration with
  the asynchronous commands
- configure thread lock to access commad execution flag
- removed some typos
- more logs

0.5.5
-----
- reduced the sleep time inside monitoring threads in CspMaster.py and CspSubarray.py
- the Csp State attribute is updated in the event callback only when the device is
  not running any power command.
- don't register the cmd_ended_cb callback with the asynchronous commands issued to power
  the CSP Element. When the CbfMaster is already in the requested State, the exception thrown
  by it is caught after the end of the thread and the class attribute _cmd_execution_state 
  (even if reset to IDLE inside the thread) is still equal RUNNING.  This causes the device
  failure if a new power command is issued on it. Maybe this issue is related to the PyTango
  issue with threading and I/O.


0.5.3
-----
- Use lmcbaseclasses = 0.5.0
- Moved ConfigureScan command to Configure.
- Moved EndSB() method to GoToIdle().
- CspMaster: moved attribute with "alarm" string in the name to "failure".
- Removed the default polling for xxCmdDurationExpected/Measured attributes.
- Removed a bug in the Off() method.
- CspSubarray: removed severe errors from the delete_device() method.
- Commented out the line with connection to PssSubarray.
- Removed a bug in passing arguments to the SubarrayRejectDecorator.
- Updated the csp-lmc-common TANGO DB configuration to resolve
  forwarded attributes not addressed.
- Install in editable mode the csp-lmc-common package to install also
  lmcbaseclasses package and its dependencies.
- Modified .gitlab-ci.yml file to execute linting and generate metrics.
- Still not resolved the issue to combine coverage outputs of 
  the different repository projects (csp-lmc-common and csp-lmc-mid).

0.4.0
-----
- Use lmcbaseclasses = 0.4.1
- ska-python-buildenv 9.3.1
- ska-python-runtime 9.3.1

0.3.0
-----
- Use lmcbaseclasses version >=0.2.0
